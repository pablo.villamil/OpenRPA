import os

#Add chrome webdriver to PATH system enviroment (need to selenium)
os.environ["PATH"]=os.environ["PATH"]+";C:\\Abs\\Archive\\scopeSrcUL\\OpenRPA\\Resources\\SeleniumWebDrivers\\Chrome\\chromedriver_win32 v76.0.3809.126\\"

#Import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

driver = webdriver.Chrome()
driver.get("http://www.python.org")
assert "Python" in driver.title
elem = driver.find_element_by_name("q")
elem.send_keys("pycon")
elem.send_keys(Keys.RETURN)
assert "No results found." not in driver.page_source
driver.close()