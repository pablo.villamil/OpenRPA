import psutil, datetime, logging, os, sys, getpass # stdout from logging

# Config settings
lPyOpenRPASourceFolderPathStr = r"..\Sources" # Path for test pyOpenRPA package

# Operations
if lPyOpenRPASourceFolderPathStr != "": sys.path.insert(0,os.path.abspath(os.path.join(lPyOpenRPASourceFolderPathStr))) # Path for test pyOpenRPA package

from pyOpenRPA.Agent import __Agent__
from pyOpenRPA.Tools import CrossOS

if CrossOS.IS_WINDOWS_BOOL: lPortInt = 1024
if CrossOS.IS_LINUX_BOOL: lPortInt = 1024
gUserNameUpperStr = getpass.getuser().upper()
if __name__ == "__main__": # New init way
    gSettings = {
        "OrchestratorDict":{
            "IsHTTPSBool": False, # True - if server is secured HTTPS, False - if server is not secured HTTP
            "HostStr":"127.0.0.1",
            "PortInt":lPortInt,
            "SuperTokenStr":"1992-04-03-0643-ru-b4ff-openrpa52zzz", # Access token to Orchestrator
        },
        "O2ADict":{
            "IsOnlineBool": True, # Parameter can be changed when program executes
            "ConnectionTimeoutSecFloat": 600.0, # Time to wait response from requests. If exceed - raise timeout exception
            "RetryTimeoutSecFloat": 10.0, # Retry interval if some error when connect

        },
        "A2ODict": {
            "ConnectionTimeoutSecFloat": 10.0, # Time to wait response from requests. If exceed - raise timeout exception
            "RetryTimeoutSecFloat": 10.0,  # Retry interval if some error when connect
        },
        "Logger":logging.getLogger("Agent"),
        "AgentDict": { # Will be filled automatically
            "HostNameUpperStr":None, # Machine hostname
            "UserUpperStr": None, # Username string
        },
        "ProcessorDict": {  # Has been changed. New general processor (one threaded) v.1.2.0
            "ActivityList": [  # List of the activities
                # {
                #    "Def":"DefAliasTest", # def link or def alias (look gSettings["Processor"]["AliasDefDict"])
                #    "ArgList":[1,2,3], # Args list
                #    "ArgDict":{"ttt":1,"222":2,"dsd":3} # Args dictionary
                #    "ArgGSettings": # Name of GSettings attribute: str (ArgDict) or index (for ArgList)
                #    "ArgLogger": None # Name of GSettings attribute: str (ArgDict) or index (for ArgList)
                # },
            ],
            "AliasDefDict": {},  # Storage for def with Str alias. To use it see pyOpenRPA.Orchestrator.ControlPanel
            "CheckIntervalSecFloat": 1.0,  # Interval for check gSettings in ProcessorDict > ActivityList
            "ExecuteBool": True,  # Flag to execute thread processor
            "ThreadIdInt": None  # Technical field - will be setup when processor init
        },
    }

    if not os.path.exists("Logs"):
        os.makedirs("Logs")
    ##########################
    # Подготовка логгера Robot
    #########################
    lL = gSettings["Logger"]
    lL.setLevel(logging.INFO)
    # create the logging file handler
    mRobotLoggerFH = logging.FileHandler(
        CrossOS.PathStr("Logs\\" + gUserNameUpperStr + "_" + datetime.datetime.now().strftime("%Y_%m_%d") + ".log"))
    mRobotLoggerFormatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    mRobotLoggerFH.setFormatter(mRobotLoggerFormatter)
    # add handler to logger object
    lL.addHandler(mRobotLoggerFH)
    ####################Add console output
    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(mRobotLoggerFormatter)
    lL.addHandler(handler)
    ############################################
    # Call the orchestrator def
    __Agent__.Agent(inGSettings=gSettings)

