import datetime
def SettingsUpdate(inDict):
    ##################################################
    #""/"SuperToken" MethodMatchURLList
    l__SuperToken_RuleMethodMatchURLBeforeList={
        ("","SUPERTOKEN"): { #!!!!!only in upper case!!!!
            "MethodMatchURLBeforeList": [
                {
                    "Method":"GET",
                    "MatchType":"Beginwith",
                    "URL":"/",
                    #"FlagAccessDefRequestGlobalAuthenticate": TestDef
                    "FlagAccess": True
                },
                {
                    "Method":"POST",
                    "MatchType":"Beginwith",
                    "URL":"/",
                    #"FlagAccessDefRequestGlobalAuthenticate": TestDef
                    "FlagAccess": True
                }
            ]
        }
    }
    #Append to global list
    inDict["Server"]["AccessUsers"]["RuleDomainUserDict"].update(l__SuperToken_RuleMethodMatchURLBeforeList)
    #"<AuthToken>":{"User":"", "Domain":"", "TokenDatetime":<Datetime>}
    #!!!!!!!!!!!!!!!!!!!!!!!
    #Attention: default supertoken is 1992-04-03-0643-ru-b4ff-openrpa52zzz - please change it when you will customize OpenRPA in your company
    #!!!!!!!!!!!!!!!!!!!!!!!
    inDict["Server"]["AccessUsers"]["AuthTokensDict"].update(
        {"1992-04-03-0643-ru-b4ff-openrpa52zzz":{"User":"SuperToken", "Domain":"", "TokenDatetime":  datetime.datetime.now(), "FlagDoNotExpire":True}}
    )
    #Return current dict
    return inDict