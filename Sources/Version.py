import os
import re
#Just call Version.Get("..")
def Get(inFolderPath):
    lREPatternString="^v[0-9]*.[0-9]*.[0-9]*$"
    lResultList = [f for f in os.listdir(inFolderPath) if re.match(lREPatternString,f)]
    lResult=None
    if len(lResultList) == 0:
        lResult = None
    else:
        lResult = lResultList[0]
    return lResult
#Update version in pyOpenRPA package
def pyOpenRPAVersionUpdate(inVarsionFolderPath,inInitFile):
    #Check version is not None
    lVersion = Get(inVarsionFolderPath)
    if lVersion:
        lInitPyFile = open(inInitFile,"r",encoding="utf8")
        lInitPyFyleString = lInitPyFile.read()
        lInitPyFile = open(inInitFile,"w",encoding="utf8")
        lInitPyFyleVersionIndexStart = lInitPyFyleString.find("__version__")
        lInitPyFyleVersionIndexEnd = lInitPyFyleString.find("\n",lInitPyFyleVersionIndexStart)
        lInitPyFyleString = lInitPyFyleString[:lInitPyFyleVersionIndexStart] + f"__version__ = '{lVersion}'" + lInitPyFyleString[lInitPyFyleVersionIndexEnd:]
        #print(lInitPyFyleString)
        lInitPyFile.write(lInitPyFyleString)
        lInitPyFile.close()