.. _module.orchestrator.gsettings:

#############################################
3. Настройки GSettings (шаблон)
#############################################

******************************
Общее
******************************
Ниже представлена структура единого глобального словаря настроек GSettings

******************************
Структура
******************************

.. include:: ../../pyOpenRPA/Orchestrator/SettingsTemplate.py
   :literal:


******************************
Быстрая навигация
******************************

- `Сообщество pyOpenRPA (telegram) <https://t.me/pyOpenRPA>`_
- `Сообщество pyOpenRPA (tenchat) <https://tenchat.ru/iMaslov?utm_source=19f2a84f-3268-437f-950c-d987ae42af24>`_
- `Сообщество pyOpenRPA (вконтакте) <https://vk.com/pyopenrpa>`_
- `Презентация pyOpenRPA <https://pyopenrpa.ru/Index/pyOpenRPA_product_service.pdf>`_
- `Портал pyOpenRPA <https://pyopenrpa.ru>`_
- `Репозиторий pyOpenRPA <https://gitlab.com/UnicodeLabs/OpenRPA>`_