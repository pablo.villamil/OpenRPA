# 2. Функции UIDesktop

## Общее

Здесь представлено описание всех функций, которые используются для взаимодействия с UI интерфейсами локальных приложений.

**Функции в модуле UIDesktop именуются по следующему принципу:**
<Входящий аргумент>_<действие>_<исходящий аргумент - если присутствует>

**Термины и определения:**


* **UIO:** Объект пользовательского интерфейса


* **UIOSelector:** Селектор (адрес) одного и/или более UIO объектов. Селектор представлен в формате списка (list) словарей (dict) атрибутивных критериев.

## Описание функций

Описание каждой функции начинается с обозначения L-,W+, что означает, что функция не поддерживается в ОС Linux (L), но поддерживается в Windows (W)

**Functions:**

| `BackendStr_GetTopLevelList_UIOInfo`([inBackend])

 | L-,W+: Получить список UIOInfo словарей - процессы, которые запущены в рабочей сессии и готовы для взаимодействия с роботом через backend inBackend

 |
| `Get_OSBitnessInt`()

                                | L-,W+: Определить разрядность робота, в котором запускается данная функция

                                                                                                                              |
| `PWASpecification_Get_PWAApplication`(…)

            | L-,W+: Получить значение атрибута backend по PWA (pywinauto) селектору.

                                                                                                                                 |
| `PWASpecification_Get_UIO`(…)

                       | L-,W+: Получить UIO объект по PWA (pywinauto) селектору.

                                                                                                                                                |
| `UIOEI_Convert_UIOInfo`(inElementInfo)

              | L-,W+: Техническая функция: Дообогащение словаря с параметрами UIO объекта по заданному UIO.element_info

                                                                                                |
| `UIOSelectorSecs_WaitAppear_Bool`(…)

                | L-,W+: Ожидать появление 1-го UIO объекта по заданному UIO селектору

                                                                                                                                    |
| `UIOSelectorSecs_WaitDisappear_Bool`(…)

             | L-,W+: Ожидать исчезновение 1-го UIO объекта по заданному UIO селектору

                                                                                                                                 |
| `UIOSelectorUIOActivity_Run_Dict`(…[, …])

           | L-,W+: Выполнить активность inActionName над UIO объектом, полученным с помощью UIO селектора inUIOSelector.

                                                                                            |
| `UIOSelector_Exist_Bool`(inUIOSelector)

             | L-,W+: Проверить существование хотя бы 1-го UIO объекта по заданному UIO селектору

                                                                                                                      |
| `UIOSelector_FocusHighlight`(inUIOSelector)

         | L-,W+: Установить фокус и подсветить на несколько секунд на экране зеленой рамкой UIO объект, который соответствует входящему UIO селектору inUIOSelector

                                               |
| `UIOSelector_GetChildList_UIOList`([…])

             | L-,W+: Получить список дочерних UIO объектов по входящему UIO селектору inUIOSelector.

                                                                                                                  |
| `UIOSelector_Get_BitnessInt`(inSpecificationList)

   | L-,W+: Определить разрядность приложения по UIO селектору.

                                                                                                                                              |
| `UIOSelector_Get_BitnessStr`(inSpecificationList)

   | L-,W+: Определить разрядность приложения по UIO селектору.

                                                                                                                                              |
| `UIOSelector_Get_UIO`(inSpecificationList[, …])

     | L-,W+: Получить список UIO объект по UIO селектору.

                                                                                                                                                     |
| `UIOSelector_Get_UIOActivityList`(inUIOSelector)

    | L-,W+: Получить список доступных действий/функций по UIO селектору inUIOSelector.

                                                                                                                       |
| `UIOSelector_Get_UIOInfo`(inUIOSelector)

            | L-,W+: Получить свойства UIO объекта (element_info), по заданному UIO селектору.

                                                                                                                        |
| `UIOSelector_Get_UIOInfoList`(inUIOSelector[, …])

   | L-,W+: Техническая функция: Получить список параметров последних уровней UIO селектора по UIO объектам, которые удовлетворяют входящим inUIOSelector, поиск по которым будет производится от уровня inElement.

 |
| `UIOSelector_Get_UIOList`(inSpecificationList)

      | L-,W+: Получить список UIO объектов по UIO селектору

                                                                                                                                                           |
| `UIOSelector_Highlight`(inUIOSelector)

              | L-,W+: Подсветить на несколько секунд на экране зеленой рамкой UIO объект, который соответствует входящему UIO селектору inUIOSelector

                                                                         |
| `UIOSelector_SafeOtherGet_Process`(inUIOSelector)

   | L-,W+: Получить процесс робота другой разрядности (если приложение UIO объекта выполняется в другой разрядности).

                                                                                              |
| `UIOSelector_SearchChildByMouse_UIO`(…)

             | L-,W+: Инициировать визуальный поиск UIO объекта с помощью указателя мыши.

                                                                                                                                     |
| `UIOSelector_SearchChildByMouse_UIOTree`(…)

         | L-,W+: Получить список уровней UIO объекта с указнием всех имеющихся атрибутов по входящему UIO селектору.

                                                                                                     |
| `UIOSelector_SearchProcessNormalize_UIOSelector`(…)

 | L-,W+: Нормализовать UIO селектор для дальнейшего использования в функциях поиска процесса, в котором находится искомый UIO объект.

                                                                            |
| `UIOSelector_SearchUIONormalize_UIOSelector`(…)

     | L-,W+: Нормализовать UIO селектор для дальнейшего использования в функциях поиск UIO объекта.

                                                                                                                  |
| `UIOSelector_TryRestore_Dict`(inSpecificationList)

  | L-,W+: Восстановить окно приложения на экране по UIO селектору inSpecificationList, если оно было свернуто.

                                                                                                    |
| `UIOSelectorsSecs_WaitAppear_List`(…[, …])

          | L-,W+: Ожидать появление хотя бы 1-го / всех UIO объектов по заданным UIO селекторам

                                                                                                                           |
| `UIOSelectorsSecs_WaitDisappear_List`(…[, …])

       | L-,W+:  Ожидать исчезновение хотя бы 1-го / всех UIO объектов по заданным UIO селекторам

                                                                                                                       |
| `UIOXY_SearchChild_ListDict`(inRootElement, …)

      | L-,W+: Техническая функция: Получить иерархию вложенности UIO объекта по заданным корневому UIO объекту, координатам X и Y.

                                                                                    |
| `UIO_FocusHighlight`(lWrapperObject[, colour, …])

   | L-,W+: Установить фокус и выполнить подсветку UIO объекта на экране

                                                                                                                                            |
| `UIO_GetCtrlIndex_Int`(inElement)

                   | L-,W+: Получить индекс UIO объекта inElement в списке родительского UIO объекта.

                                                                                                                               |
| `UIO_Highlight`(lWrapperObject[, colour, …])

        | L-,W+: Выполнить подсветку UIO объекта на экране

                                                                                                                                                               |

### pyOpenRPA.Robot.UIDesktop.BackendStr_GetTopLevelList_UIOInfo(inBackend='win32')
L-,W+: Получить список UIOInfo словарей - процессы, которые запущены в рабочей сессии и готовы для взаимодействия с роботом через backend inBackend

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
lAppList = UIDesktop.BackendStr_GetTopLevelList_UIOInfo() # Очистить UIO селектор от недопустимых ключей для дальнейшего использования
```


* **Параметры**

    **inBackend** (*list**, **обязательный*) – вид backend, который планируется использовать для взаимодействия с UIO объектами



* **Результат**

    список UIOInfo словарей



### pyOpenRPA.Robot.UIDesktop.Get_OSBitnessInt()
L-,W+: Определить разрядность робота, в котором запускается данная функция

```
from pyOpenRPA.Robot import UIDesktop
lRobotBitInt = UIDesktop.Get_OSBitnessInt() # Определить разрядность робота, в котором была вызвана это функция
```


* **Результат**

    64 (int) - разрядность приложения равна 64 битам; 32 (int) - разрядность приложения равна 32 битам



### pyOpenRPA.Robot.UIDesktop.PWASpecification_Get_PWAApplication(inControlSpecificationArray)
L-,W+: Получить значение атрибута backend по PWA (pywinauto) селектору. Мы рекомендуем использовать метод UIOSelector_UIO_Get, так как UIO селектор обладает большей функциональностью.

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"}]            
lBackendStr = UIDesktop.PWASpecification_Get_PWAApplication(lDemoBaseUIOSelector) # Получить backend по PWA селектору
```


* **Параметры**

    **inControlSpecificationArray** (*list**, **обязательный*) – PWA селектор, который определяет критерии поиска UIO объекта
    Допустимые ключи PWA селектора:


    * class_name содержимое атрибута class UIO объекта


    * class_name_re содержимое атрибута class UIO объекта, которое удовлетворяет установленному рег. выражению


    * process идентификатор процесса, в котором находится UIO объект


    * title содержимое атрибута title UIO объекта


    * title_re содержимое атрибута title UIO объекта, которое удовлетворяет установленному рег. выражению


    * top_level_only признак поиска только на верхнем уровне приложения. По умолчанию True


    * visible_only признак поиска только среди видимых UIO объектов. По умолчанию True


    * enabled_only признак поиска только среди разблокированных UIO объектов. По умолчанию False


    * best_match содержимое атрибута title UIO объекта максимально приближено к заданному


    * handle идентификатор handle искомого UIO объекта


    * ctrl_index индекс UIO объекта среди всех дочерних объектов в списке родительского


    * found_index индекс UIO объекта среди всех обнаруженных


    * predicate_func пользовательская функция проверки соответсвия UIO элемента


    * active_only признак поиска только среди активных UIO объектов. По умолчанию False


    * control_id идентификатор control_id искомого UIO объекта


    * control_type тип элемента (применимо, если backend == «uia»)


    * auto_id идентификатор auto_id искомого UIO объекта (применимо, если backend == «uia»)


    * framework_id идентификатор framework_id искомого UIO объекта (применимо, если backend == «uia»)


    * backend вид технологии подключения к поиску UIO объекта («uia» или «win32»)




* **Результат**

    «win32» или «uia»



### pyOpenRPA.Robot.UIDesktop.PWASpecification_Get_UIO(inControlSpecificationArray)
L-,W+: Получить UIO объект по PWA (pywinauto) селектору. ([https://pywinauto.readthedocs.io/en/latest/code/pywinauto.findwindows.html](https://pywinauto.readthedocs.io/en/latest/code/pywinauto.findwindows.html)). Мы рекомендуем использовать метод UIOSelector_UIO_Get, так как UIO селектор обладает большей функциональностью.

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"}]            
lUIOObject = UIDesktop.PWASpecification_Get_UIO(lDemoBaseUIOSelector) # Получить UIO объект по PWA селектору
```


* **Параметры**

    **inControlSpecificationArray** (*list**, **обязательный*) – PWA селектор, который определяет критерии поиска UIO объекта
    Допустимые ключи PWA селектора:


    * class_name содержимое атрибута class UIO объекта


    * class_name_re содержимое атрибута class UIO объекта, которое удовлетворяет установленному рег. выражению


    * process идентификатор процесса, в котором находится UIO объект


    * title содержимое атрибута title UIO объекта


    * title_re содержимое атрибута title UIO объекта, которое удовлетворяет установленному рег. выражению


    * top_level_only признак поиска только на верхнем уровне приложения. По умолчанию True


    * visible_only признак поиска только среди видимых UIO объектов. По умолчанию True


    * enabled_only признак поиска только среди разблокированных UIO объектов. По умолчанию False


    * best_match содержимое атрибута title UIO объекта максимально приближено к заданному


    * handle идентификатор handle искомого UIO объекта


    * ctrl_index индекс UIO объекта среди всех дочерних объектов в списке родительского


    * found_index индекс UIO объекта среди всех обнаруженных


    * predicate_func пользовательская функция проверки соответсвия UIO элемента


    * active_only признак поиска только среди активных UIO объектов. По умолчанию False


    * control_id идентификатор control_id искомого UIO объекта


    * control_type тип элемента (применимо, если backend == «uia»)


    * auto_id идентификатор auto_id искомого UIO объекта (применимо, если backend == «uia»)


    * framework_id идентификатор framework_id искомого UIO объекта (применимо, если backend == «uia»)


    * backend вид технологии подключения к поиску UIO объекта («uia» или «win32»)




* **Результат**

    UIO объект



### pyOpenRPA.Robot.UIDesktop.UIOEI_Convert_UIOInfo(inElementInfo)
L-,W+: Техническая функция: Дообогащение словаря с параметрами UIO объекта по заданному UIO.element_info

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"}]            
lUIO = UIDesktop.UIOSelector_Get_UIO(lDemoBaseUIOSelector) # Получить UIO объект по UIO селектору.
lUIOProcessInfoDict = UIDesktop.UIOEI_Convert_UIOInfo(lUIO.element_info)
```


* **Параметры**

    **inElementInfo** (*object**, **обязательный*) – экземпляр класса UIO.element_info, для которого требуется дообогатить словарь с параметрами (в дальнейшем можно использовать как элемент UIO селектора).



* **Результат**

    dict, пример: {«title»:None,»rich_text»:None,»process_id»:None,»process»:None,»handle»:None,»class_name»:None,»control_type»:None,»control_id»:None,»rectangle»:{«left»:None,»top»:None,»right»:None,»bottom»:None}, „runtime_id“:None}



### pyOpenRPA.Robot.UIDesktop.UIOSelectorSecs_WaitAppear_Bool(inSpecificationList, inWaitSecs)
L-,W+: Ожидать появление 1-го UIO объекта по заданному UIO селектору

!ВНИМАНИЕ! ДАННАЯ ФУНКЦИОНАЛЬНОСТЬ В АВТОМАТИЧЕСКОМ РЕЖИМЕ ПОДДЕРЖИВАЕТ ВСЕ РАЗРЯДНОСТИ ПРИЛОЖЕНИЙ (32|64), КОТОРЫЕ ЗАПУЩЕНЫ В СЕСИИ. PYTHON x64 ИМЕЕТ ВОЗМОЖНОСТЬ ВЗЗАИМОДЕЙСТВИЯ С x32 UIO ОБЪЕКТАМИ, НО МЫ РЕКОМЕНДУЕМ ДОПОЛНИТЕЛЬНО ИСПОЛЬЗОВАТЬ ИНТЕРПРЕТАТОР PYTHON x32 (ПОДРОБНЕЕ СМ. ФУНКЦИЮ Configure())

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"},{"title":"DEMO", "depth_start": 5, "depth_end": 5}]         
lDemoBaseUIOExistBool = UIDesktop.UIOSelectorSecs_WaitAppear_Bool(lDemoBaseUIOSelector) # Ожидать появление UIO объекта
```


* **Параметры**

    
    * **inSpecificationList** (*list**, **обязательный*) – UIO селектор, который определяет критерии поиска UIO объекта


    * **inWaitSecs** (*float**, **необязательный*) – Количество секунд, которые отвести на ожидание UIO объекта. По умолчанию 24 часа (86400 секунд)



* **Результат**

    True - UIO объект был обнаружен. False - обратная ситуациая



### pyOpenRPA.Robot.UIDesktop.UIOSelectorSecs_WaitDisappear_Bool(inSpecificationList, inWaitSecs)
L-,W+: Ожидать исчезновение 1-го UIO объекта по заданному UIO селектору

!ВНИМАНИЕ! ДАННАЯ ФУНКЦИОНАЛЬНОСТЬ В АВТОМАТИЧЕСКОМ РЕЖИМЕ ПОДДЕРЖИВАЕТ ВСЕ РАЗРЯДНОСТИ ПРИЛОЖЕНИЙ (32|64), КОТОРЫЕ ЗАПУЩЕНЫ В СЕСИИ. PYTHON x64 ИМЕЕТ ВОЗМОЖНОСТЬ ВЗЗАИМОДЕЙСТВИЯ С x32 UIO ОБЪЕКТАМИ, НО МЫ РЕКОМЕНДУЕМ ДОПОЛНИТЕЛЬНО ИСПОЛЬЗОВАТЬ ИНТЕРПРЕТАТОР PYTHON x32 (ПОДРОБНЕЕ СМ. ФУНКЦИЮ Configure())

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"},{"title":"DEMO", "depth_start": 5, "depth_end": 5}]         
lDemoBaseUIOExistBool = UIDesktop.UIOSelectorSecs_WaitDisappear_Bool(lDemoBaseUIOSelector) # Ожидать исчезновение UIO объекта
```


* **Параметры**

    
    * **inSpecificationList** (*list**, **обязательный*) – UIO селектор, который определяет критерии поиска UIO объекта


    * **inWaitSecs** (*float**, **необязательный*) – Количество секунд, которые отвести на исчезновение UIO объекта. По умолчанию 24 часа (86400 секунд)



* **Результат**

    True - UIO объект был обнаружен. False - обратная ситуациая



### pyOpenRPA.Robot.UIDesktop.UIOSelectorUIOActivity_Run_Dict(inUIOSelector, inActionName, inArgumentList=None, inkwArgumentObject=None)
L-,W+: Выполнить активность inActionName над UIO объектом, полученным с помощью UIO селектора inUIOSelector. Описание возможных активностей см. ниже.

!ВНИМАНИЕ! ДАННАЯ ФУНКЦИОНАЛЬНОСТЬ В АВТОМАТИЧЕСКОМ РЕЖИМЕ ПОДДЕРЖИВАЕТ ВСЕ РАЗРЯДНОСТИ ПРИЛОЖЕНИЙ (32|64), КОТОРЫЕ ЗАПУЩЕНЫ В СЕСИИ. PYTHON x64 ИМЕЕТ ВОЗМОЖНОСТЬ ВЗЗАИМОДЕЙСТВИЯ С x32 UIO ОБЪЕКТАМИ, НО МЫ РЕКОМЕНДУЕМ ДОПОЛНИТЕЛЬНО ИСПОЛЬЗОВАТЬ ИНТЕРПРЕТАТОР PYTHON x32 (ПОДРОБНЕЕ СМ. ФУНКЦИЮ Configure())

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"}]            
lActivityResult = UIDesktop.UIOSelectorUIOActivity_Run_Dict(lDemoBaseUIOSelector, "click") # выполнить действие над UIO объектом с помощью UIO селектора.
```


* **Параметры**

    
    * **inUIOSelector** (*list**, **обязательный*) – UIO селектор, который определяет UIO объект, для которого будет представлен перечень доступных активностей.


    * **inActionName** (*str**, **обязательный*) – наименование активности, которую требуется выполнить над UIO объектом


    * **inArgumentList** (*list**, **необязательный*) – список передаваемых неименованных аргументов в функцию inActionName


    * **inkwArgumentObject** (*dict**, **необязательный*) – словарь передаваемых именованных аргументов в функцию inActionName



* **Результат**

    возвращает результат запускаемой функции с наименованием inActionName над UIO объектом



### pyOpenRPA.Robot.UIDesktop.UIOSelector_Exist_Bool(inUIOSelector)
L-,W+: Проверить существование хотя бы 1-го UIO объекта по заданному UIO селектору

!ВНИМАНИЕ! ДАННАЯ ФУНКЦИОНАЛЬНОСТЬ В АВТОМАТИЧЕСКОМ РЕЖИМЕ ПОДДЕРЖИВАЕТ ВСЕ РАЗРЯДНОСТИ ПРИЛОЖЕНИЙ (32|64), КОТОРЫЕ ЗАПУЩЕНЫ В СЕСИИ. PYTHON x64 ИМЕЕТ ВОЗМОЖНОСТЬ ВЗЗАИМОДЕЙСТВИЯ С x32 UIO ОБЪЕКТАМИ, НО МЫ РЕКОМЕНДУЕМ ДОПОЛНИТЕЛЬНО ИСПОЛЬЗОВАТЬ ИНТЕРПРЕТАТОР PYTHON x32 (ПОДРОБНЕЕ СМ. ФУНКЦИЮ Configure())

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"},{"title":"DEMO", "depth_start": 5, "depth_end": 5}]         
lDemoBaseUIOExistBool = UIDesktop.UIOSelector_Exist_Bool(lDemoBaseUIOSelector) # Получить булевый результат проверки существования UIO объекта
```


* **Параметры**

    **inUIOSelector** (*list**, **обязательный*) – UIO Селектор, который определяет критерии поиска UIO объектов



* **Результат**

    True - существует хотя бы 1 UIO объект. False - не существует ни одного UIO объекта по заданному UIO селектору



### pyOpenRPA.Robot.UIDesktop.UIOSelector_FocusHighlight(inUIOSelector)
L-,W+: Установить фокус и подсветить на несколько секунд на экране зеленой рамкой UIO объект, который соответствует входящему UIO селектору inUIOSelector

!ВНИМАНИЕ! ДАННАЯ ФУНКЦИОНАЛЬНОСТЬ В АВТОМАТИЧЕСКОМ РЕЖИМЕ ПОДДЕРЖИВАЕТ ВСЕ РАЗРЯДНОСТИ ПРИЛОЖЕНИЙ (32|64), КОТОРЫЕ ЗАПУЩЕНЫ В СЕСИИ. PYTHON x64 ИМЕЕТ ВОЗМОЖНОСТЬ ВЗЗАИМОДЕЙСТВИЯ С x32 UIO ОБЪЕКТАМИ, НО МЫ РЕКОМЕНДУЕМ ДОПОЛНИТЕЛЬНО ИСПОЛЬЗОВАТЬ ИНТЕРПРЕТАТОР PYTHON x32 (ПОДРОБНЕЕ СМ. ФУНКЦИЮ Configure())

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"}]            
UIDesktop.UIOSelector_FocusHighlight(lDemoBaseUIOSelector) # Установить фокус и подсветить UIO объект по UIO селектору
```


* **Параметры**

    **inUIOSelector** (*list**, **обязательный*) – UIO селектор, который определяет UIO объект, для которого будет представлен перечень доступных активностей.



### pyOpenRPA.Robot.UIDesktop.UIOSelector_GetChildList_UIOList(inUIOSelector=None, inBackend='win32')
L-,W+: Получить список дочерних UIO объектов по входящему UIO селектору inUIOSelector.

!ВНИМАНИЕ! ДАННАЯ ФУНКЦИОНАЛЬНОСТЬ В АВТОМАТИЧЕСКОМ РЕЖИМЕ ПОДДЕРЖИВАЕТ ВСЕ РАЗРЯДНОСТИ ПРИЛОЖЕНИЙ (32|64), КОТОРЫЕ ЗАПУЩЕНЫ В СЕСИИ. PYTHON x64 ИМЕЕТ ВОЗМОЖНОСТЬ ВЗЗАИМОДЕЙСТВИЯ С x32 UIO ОБЪЕКТАМИ, НО МЫ РЕКОМЕНДУЕМ ДОПОЛНИТЕЛЬНО ИСПОЛЬЗОВАТЬ ИНТЕРПРЕТАТОР PYTHON x32 (ПОДРОБНЕЕ СМ. ФУНКЦИЮ Configure())

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"}]            
lUIOList = UIDesktop.UIOSelector_GetChildList_UIOList(lDemoBaseUIOSelector) # Получить список дочерних UIO объектов с помощью UIO селектора
```


* **Параметры**

    
    * **inUIOSelector** (*list**, **обязательный*) – родительский UIO объект, полученный ранее с помощью UIO селектора.


    * **inBackend** (*str**, **необязательный*) – вид backend «win32» или «uia». По умолчанию mDefaultPywinautoBackend («win32»)



* **Результат**

    список дочерних UIO объектов



### pyOpenRPA.Robot.UIDesktop.UIOSelector_Get_BitnessInt(inSpecificationList)
L-,W+: Определить разрядность приложения по UIO селектору. Вернуть результат в формате целого числа (64 или 32)

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"},{"title":"DEMO", "depth_start": 5, "depth_end": 5}]         
lDemoBaseBitInt = UIDesktop.UIOSelector_Get_BitnessInt(lDemoBaseUIOSelector) # Определить разрядность приложения, в котором обнаружен UIO объект по селектору
```


* **Параметры**

    **inSpecificationList** (*list**, **обязательный*) – UIO селектор, который определяет критерии поиска UIO объекта



* **Результат**

    None - UIO объект не обнаружен; 64 (int) - разрядность приложения равна 64 битам; 32 (int) - разрядность приложения равна 32 битам



### pyOpenRPA.Robot.UIDesktop.UIOSelector_Get_BitnessStr(inSpecificationList)
L-,W+: Определить разрядность приложения по UIO селектору. Вернуть результат в формате строки («64» или «32»)

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"},{"title":"DEMO", "depth_start": 5, "depth_end": 5}]         
lDemoBaseBitStr = UIDesktop.UIOSelector_Get_BitnessStr(lDemoBaseUIOSelector) # Определить разрядность приложения, в котором обнаружен UIO объект по селектору
```


* **Параметры**

    **inSpecificationList** (*list**, **обязательный*) – UIO селектор, который определяет критерии поиска UIO объекта



* **Результат**

    None - UIO объект не обнаружен; «64» (str) - разрядность приложения равна 64 битам; «32» (str) - разрядность приложения равна 32 битам



### pyOpenRPA.Robot.UIDesktop.UIOSelector_Get_UIO(inSpecificationList, inElement=None, inFlagRaiseException=True)
L-,W+: Получить список UIO объект по UIO селектору. Если критериям UIO селектора удовлетворяет несколько UIO объектов - вернуть первый из списка

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"},{"title":"DEMO", "depth_start": 5, "depth_end": 5}]         
lDemoBaseUIOList = UIDesktop.UIOSelector_Get_UIO(lDemoBaseUIOSelector) #Получить 1-й UIO объект, которые удовлетворяет требованиям UIO селектора. В нашем примере либо None, либо UIO объект
```


* **Параметры**

    
    * **inSpecificationList** (*list**, **обязательный*) – UIO Селектор, который определяет критерии поиска UI элементов


    * **inElement** (*UIO объект**, **опциональный*) – Родительский элемент, от которого выполнить поиск UIO объектов по заданному UIO селектору. Если аргумент не задан, платформа выполнит поиск UIO объектов среди всех доступных приложений windows, которые запущены на текущей сессии


    * **inFlagRaiseException** (*bool**, **опциональный*) – True - формировать ошибку exception, если платформа не обнаружина ни одного UIO объекта по заданному UIO селектору. False - обратный случай. По умолчанию True



* **Результат**

    UIO объект, которые удовлетворяют условиям UIO селектора, или None



### pyOpenRPA.Robot.UIDesktop.UIOSelector_Get_UIOActivityList(inUIOSelector)
L-,W+: Получить список доступных действий/функций по UIO селектору inUIOSelector. Описание возможных активностей см. ниже.

!ВНИМАНИЕ! ДАННАЯ ФУНКЦИОНАЛЬНОСТЬ В АВТОМАТИЧЕСКОМ РЕЖИМЕ ПОДДЕРЖИВАЕТ ВСЕ РАЗРЯДНОСТИ ПРИЛОЖЕНИЙ (32|64), КОТОРЫЕ ЗАПУЩЕНЫ В СЕСИИ. PYTHON x64 ИМЕЕТ ВОЗМОЖНОСТЬ ВЗЗАИМОДЕЙСТВИЯ С x32 UIO ОБЪЕКТАМИ, НО МЫ РЕКОМЕНДУЕМ ДОПОЛНИТЕЛЬНО ИСПОЛЬЗОВАТЬ ИНТЕРПРЕТАТОР PYTHON x32 (ПОДРОБНЕЕ СМ. ФУНКЦИЮ Configure())

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"}]            
lActivityList = UIDesktop.UIOSelector_Get_UIOActivityList(lDemoBaseUIOSelector) # Получить список активностей по UIO селектору.
```


* **Параметры**

    **inUIOSelector** (*list**, **обязательный*) – UIO селектор, который определяет UIO объект, для которого будет представлен перечень доступных активностей.



### pyOpenRPA.Robot.UIDesktop.UIOSelector_Get_UIOInfo(inUIOSelector)
L-,W+: Получить свойства UIO объекта (element_info), по заданному UIO селектору. Ниже представлен перечень возвращаемых свойств.

Для backend = win32:


* automation_id (int)


* class_name (str)


* control_id (int)


* control_type (str)


* full_control_type (str)


* enabled (bool)


* handle (int)


* name (str)


* parent (object/UIO)


* process_id (int)


* rectangle (object/rect)


* rich_text (str)


* visible (bool)

Для backend = uia:


* automation_id (int)


* class_name (str)


* control_id (int)


* control_type (str)


* enabled (bool)


* framework_id (int)


* handle (int)


* name (str)


* parent (object/UIO)


* process_id (int)


* rectangle (object/rect)


* rich_text (str)


* runtime_id (int)


* visible (bool)

!ВНИМАНИЕ! ДАННАЯ ФУНКЦИОНАЛЬНОСТЬ В АВТОМАТИЧЕСКОМ РЕЖИМЕ ПОДДЕРЖИВАЕТ ВСЕ РАЗРЯДНОСТИ ПРИЛОЖЕНИЙ (32|64), КОТОРЫЕ ЗАПУЩЕНЫ В СЕСИИ. PYTHON x64 ИМЕЕТ ВОЗМОЖНОСТЬ ВЗЗАИМОДЕЙСТВИЯ С x32 UIO ОБЪЕКТАМИ, НО МЫ РЕКОМЕНДУЕМ ДОПОЛНИТЕЛЬНО ИСПОЛЬЗОВАТЬ ИНТЕРПРЕТАТОР PYTHON x32 (ПОДРОБНЕЕ СМ. ФУНКЦИЮ Configure())

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"}]            
lUIOElementInfoDict = UIDesktop.UIOSelector_Get_UIOInfo(lDemoBaseUIOSelector) #Получить свойства над UIO объектом с помощью UIO селектора.
```


* **Параметры**

    **inUIOSelector** (*list**, **обязательный*) – UIO селектор, который определяет UIO объект, для которого будет представлен перечень доступных активностей.



* **Результат**

    словарь свойств element_info: Пример {«control_id»: …, «process_id»: …}



### pyOpenRPA.Robot.UIDesktop.UIOSelector_Get_UIOInfoList(inUIOSelector, inElement=None)
L-,W+: Техническая функция: Получить список параметров последних уровней UIO селектора по UIO объектам, которые удовлетворяют входящим inUIOSelector, поиск по которым будет производится от уровня inElement.

!ВНИМАНИЕ! ДАННАЯ ФУНКЦИОНАЛЬНОСТЬ В АВТОМАТИЧЕСКОМ РЕЖИМЕ ПОДДЕРЖИВАЕТ ВСЕ РАЗРЯДНОСТИ ПРИЛОЖЕНИЙ (32|64), КОТОРЫЕ ЗАПУЩЕНЫ В СЕСИИ. PYTHON x64 ИМЕЕТ ВОЗМОЖНОСТЬ ВЗЗАИМОДЕЙСТВИЯ С x32 UIO ОБЪЕКТАМИ, НО МЫ РЕКОМЕНДУЕМ ДОПОЛНИТЕЛЬНО ИСПОЛЬЗОВАТЬ ИНТЕРПРЕТАТОР PYTHON x32 (ПОДРОБНЕЕ СМ. ФУНКЦИЮ Configure())

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"}]            
lUIOInfoList = UIDesktop.UIOSelector_Get_UIOInfoList(lDemoBaseUIOSelector) # Получить словарь параметров по UIO селектору.
```


* **Параметры**

    
    * **inUIOSelector** (*list**, **обязательный*) – UIO селектор, который определяет UIO объект, для которого будет произведено извлечение всех атрибутов на всех уровнях.


    * **inElement** (*UIO объект**, **необязательный*) – UIO объект, от которого выполнить поиск дочерних UIO объектов по UIO селектору inUIOSelector. По умолчанию None - поиск среди всех приложений.



* **Результат**

    dict, пример: {«title»:None,»rich_text»:None,»process_id»:None,»process»:None,»handle»:None,»class_name»:None,»control_type»:None,»control_id»:None,»rectangle»:{«left»:None,»top»:None,»right»:None,»bottom»:None}, „runtime_id“:None}



### pyOpenRPA.Robot.UIDesktop.UIOSelector_Get_UIOList(inSpecificationList, inElement=None, inFlagRaiseException=True)
L-,W+: Получить список UIO объектов по UIO селектору

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"},{"title":"DEMO", "depth_start": 5, "depth_end": 5}]         
lDemoBaseUIOList = UIDesktop.UIOSelector_Get_UIOList(lDemoBaseUIOSelector) #Получить список UIO объектов, которые удовлетворяют требованиям UIO селектора. В нашем примере либо [], либо [UIO объект]
```


* **Параметры**

    
    * **inSpecificationList** (*list**, **обязательный*) – UIO Селектор, который определяет критерии поиска UI элементов


    * **inElement** (*UIO объект**, **опциональный*) – Родительский элемент, от которого выполнить поиск UIO объектов по заданному UIO селектору. Если аргумент не задан, платформа выполнит поиск UIO объектов среди всех доступных приложений windows, которые запущены на текущей сессии


    * **inFlagRaiseException** (*bool**, **опциональный*) – True - формировать ошибку exception, если платформа не обнаружина ни одного UIO объекта по заданному UIO селектору. False - обратный случай. По умолчанию True



* **Результат**

    Список UIO объектов, которые удовлетворяют условиям UIO селектора



### pyOpenRPA.Robot.UIDesktop.UIOSelector_Highlight(inUIOSelector)
L-,W+: Подсветить на несколько секунд на экране зеленой рамкой UIO объект, который соответствует входящему UIO селектору inUIOSelector

!ВНИМАНИЕ! ДАННАЯ ФУНКЦИОНАЛЬНОСТЬ В АВТОМАТИЧЕСКОМ РЕЖИМЕ ПОДДЕРЖИВАЕТ ВСЕ РАЗРЯДНОСТИ ПРИЛОЖЕНИЙ (32|64), КОТОРЫЕ ЗАПУЩЕНЫ В СЕСИИ. PYTHON x64 ИМЕЕТ ВОЗМОЖНОСТЬ ВЗЗАИМОДЕЙСТВИЯ С x32 UIO ОБЪЕКТАМИ, НО МЫ РЕКОМЕНДУЕМ ДОПОЛНИТЕЛЬНО ИСПОЛЬЗОВАТЬ ИНТЕРПРЕТАТОР PYTHON x32 (ПОДРОБНЕЕ СМ. ФУНКЦИЮ Configure())

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"}]            
UIDesktop.UIOSelector_Highlight(lDemoBaseUIOSelector) # Подсветить UIO объект по UIO селектору
```


* **Параметры**

    **inUIOSelector** (*list**, **обязательный*) – UIO селектор, который определяет UIO объект, для которого будет представлен перечень доступных активностей.



### pyOpenRPA.Robot.UIDesktop.UIOSelector_SafeOtherGet_Process(inUIOSelector)
L-,W+: Получить процесс робота другой разрядности (если приложение UIO объекта выполняется в другой разрядности). Функция возвращает None, если разрядность робота совпадает с разрядностью приложения UIO объекта, либо если при инициализации робота не устанавливался интерпретатор другой разрядности.

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"},{"title":"DEMO", "depth_start": 5, "depth_end": 5}]         
lOtherBitnessProcess = UIDesktop.UIOSelector_SafeOtherGet_Process(lDemoBaseUIOSelector) # Вернуть процесс робота, схожей разрядности
```


* **Параметры**

    **inUIOSelector** (*list**, **обязательный*) – UIO селектор, который определяет критерии поиска UIO объекта



* **Результат**

    Процесс робота схожей разрядности



### pyOpenRPA.Robot.UIDesktop.UIOSelector_SearchChildByMouse_UIO(inElementSpecification)
L-,W+: Инициировать визуальный поиск UIO объекта с помощью указателя мыши. При наведении указателя мыши UIO объект выделяется зеленой рамкой. Остановить режим поиска можно с помощью зажима клавиши ctrl left на протяжении нескольких секунд. После этого в веб окне студии будет отображено дерево расположения искомого UIO объекта.

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"}]            
lUIO = UIDesktop.UIOSelector_SearchChildByMouse_UIO(lDemoBaseUIOSelector) # Инициировать поиск дочернего UIO объекта, который расположен внутри lDemoBaseUIOSelector.
```


* **Параметры**

    **inElementSpecification** (*list**, **обязательный*) – UIO селектор, который определяет критерии поиска родительского UIO объекта, в котором будет производиться поиск дочернего UIO объекта



* **Результат**

    UIO объект или None (если UIO не был обнаружен)



### pyOpenRPA.Robot.UIDesktop.UIOSelector_SearchChildByMouse_UIOTree(inUIOSelector)
L-,W+: Получить список уровней UIO объекта с указнием всех имеющихся атрибутов по входящему UIO селектору.

!ВНИМАНИЕ! ДАННАЯ ФУНКЦИОНАЛЬНОСТЬ В АВТОМАТИЧЕСКОМ РЕЖИМЕ ПОДДЕРЖИВАЕТ ВСЕ РАЗРЯДНОСТИ ПРИЛОЖЕНИЙ (32|64), КОТОРЫЕ ЗАПУЩЕНЫ В СЕСИИ. PYTHON x64 ИМЕЕТ ВОЗМОЖНОСТЬ ВЗЗАИМОДЕЙСТВИЯ С x32 UIO ОБЪЕКТАМИ, НО МЫ РЕКОМЕНДУЕМ ДОПОЛНИТЕЛЬНО ИСПОЛЬЗОВАТЬ ИНТЕРПРЕТАТОР PYTHON x32 (ПОДРОБНЕЕ СМ. ФУНКЦИЮ Configure())

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"}]            
lBackendStr = UIDesktop.UIOSelector_SearchChildByMouse_UIOTree(lDemoBaseUIOSelector) # Получить список атрибутов всех родительских элементов lDemoBaseUIOSelector.
```


* **Параметры**

    **inUIOSelector** (*list**, **обязательный*) – UIO селектор, который определяет UIO объект, для которого будет произведено извлечение всех атрибутов на всех уровнях.



* **Результат**

    list, список атрибутов на каждом уровне UIO объекта



### pyOpenRPA.Robot.UIDesktop.UIOSelector_SearchProcessNormalize_UIOSelector(inControlSpecificationArray)
L-,W+: Нормализовать UIO селектор для дальнейшего использования в функциях поиска процесса, в котором находится искомый UIO объект. Если недопустимых атрибутов не присутствует, то оставить как есть.

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelectorDitry = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"}]               
lDemoBaseUIOSelectorClean = UIDesktop.UIOSelector_SearchProcessNormalize_UIOSelector(lDemoBaseUIOSelectorDitry) # Очистить UIO селектор от недопустимых ключей для дальнейшего использования
```


* **Параметры**

    **inControlSpecificationArray** (*list**, **обязательный*) – UIO селектор, который определяет UIO объект, для которого будет представлен перечень доступных активностей.



* **Результат**

    нормализованный UIO селектор



### pyOpenRPA.Robot.UIDesktop.UIOSelector_SearchUIONormalize_UIOSelector(inControlSpecificationArray)
L-,W+: Нормализовать UIO селектор для дальнейшего использования в функциях поиск UIO объекта. Если недопустимых атрибутов не присутствует, то оставить как есть.

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelectorDitry = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"}]               
lDemoBaseUIOSelectorClean = UIDesktop.UIOSelector_SearchUIONormalize_UIOSelector(lDemoBaseUIOSelectorDitry) # Очистить UIO селектор от недопустимых ключей для дальнейшего использования
```


* **Параметры**

    **inControlSpecificationArray** (*list**, **обязательный*) – UIO селектор, который определяет UIO объект, для которого будет представлен перечень доступных активностей.



* **Результат**

    нормализованный UIO селектор



### pyOpenRPA.Robot.UIDesktop.UIOSelector_TryRestore_Dict(inSpecificationList)
L-,W+: Восстановить окно приложения на экране по UIO селектору inSpecificationList, если оно было свернуто. Функция обернута в try .. except - ошибок не возникнет.

!ВНИМАНИЕ! ДАННАЯ ФУНКЦИОНАЛЬНОСТЬ УЖЕ ИСПОЛЬЗУЕТСЯ В РЯДЕ ДРУГИХ ФУНКЦИЙ ТАК КАК АДРЕССАЦИЯ ПО UIA FRAMEWORK НЕДОСТУПНА, ЕСЛИ ПРИЛОЖЕНИЕ СВЕРНУТО.

!ВНИМАНИЕ! ДАННАЯ ФУНКЦИОНАЛЬНОСТЬ В АВТОМАТИЧЕСКОМ РЕЖИМЕ ПОДДЕРЖИВАЕТ ВСЕ РАЗРЯДНОСТИ ПРИЛОЖЕНИЙ (32|64), КОТОРЫЕ ЗАПУЩЕНЫ В СЕСИИ. PYTHON x64 ИМЕЕТ ВОЗМОЖНОСТЬ ВЗЗАИМОДЕЙСТВИЯ С x32 UIO ОБЪЕКТАМИ, НО МЫ РЕКОМЕНДУЕМ ДОПОЛНИТЕЛЬНО ИСПОЛЬЗОВАТЬ ИНТЕРПРЕТАТОР PYTHON x32 (ПОДРОБНЕЕ СМ. ФУНКЦИЮ Configure())

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"}]            
UIDesktop.UIOSelector_TryRestore_Dict(lDemoBaseUIOSelector) # Попытка восстановления свернутого окна по UIO селектору.
```


* **Параметры**

    **inSpecificationList** (*list**, **обязательный*) – UIO селектор, который определяет UIO объект, для которого будет произведено извлечение всех атрибутов на всех уровнях.



### pyOpenRPA.Robot.UIDesktop.UIOSelectorsSecs_WaitAppear_List(inSpecificationListList, inWaitSecs=86400.0, inFlagWaitAllInMoment=False)
L-,W+: Ожидать появление хотя бы 1-го / всех UIO объектов по заданным UIO селекторам

!ВНИМАНИЕ! ДАННАЯ ФУНКЦИОНАЛЬНОСТЬ В АВТОМАТИЧЕСКОМ РЕЖИМЕ ПОДДЕРЖИВАЕТ ВСЕ РАЗРЯДНОСТИ ПРИЛОЖЕНИЙ (32|64), КОТОРЫЕ ЗАПУЩЕНЫ В СЕСИИ. PYTHON x64 ИМЕЕТ ВОЗМОЖНОСТЬ ВЗЗАИМОДЕЙСТВИЯ С x32 UIO ОБЪЕКТАМИ, НО МЫ РЕКОМЕНДУЕМ ДОПОЛНИТЕЛЬНО ИСПОЛЬЗОВАТЬ ИНТЕРПРЕТАТОР PYTHON x32 (ПОДРОБНЕЕ СМ. ФУНКЦИЮ Configure())

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"},{"title":"DEMO", "depth_start": 5, "depth_end": 5}]         
lNotepadOKSelector = [{"title":"notepad"},{"title":"OK"}]
lNotepadCancelSelector = [{"title":"notepad"},{"title":"Cancel"}]
lDemoBaseUIOExistList = UIDesktop.UIOSelectorsSecs_WaitAppear_List([lDemoBaseUIOSelector, lNotepadOKSelector, lNotepadCancelSelector]) # Ожидать появление UIO объекта
```


* **Параметры**

    
    * **inSpecificationListList** (*list**, **обязательный*) – Список UIO селекторов, которые определяют критерии поиска UIO объектов

        Пример: [
        [{«title»:»notepad»},{«title»:»OK»}],
        [{«title»:»notepad»},{«title»:»Cancel»}]

    ]



    * **inWaitSecs** (*float**, **необязательный*) – Количество секунд, которые отвести на ожидание UIO объектов. По умолчанию 24 часа (86400 секунд)


    * **inFlagWaitAllInMoment** – True - Ожидать до того момента, пока не появятся все запрашиваемые UIO объекты на рабочей области



* **Результат**

    Список индексов, которые указывают на номер входящих UIO селекторов, которые были обнаружены на рабочей области. Пример: [0,2]



### pyOpenRPA.Robot.UIDesktop.UIOSelectorsSecs_WaitDisappear_List(inSpecificationListList, inWaitSecs=86400.0, inFlagWaitAllInMoment=False)
L-,W+:  Ожидать исчезновение хотя бы 1-го / всех UIO объектов по заданным UIO селекторам

!ВНИМАНИЕ! ДАННАЯ ФУНКЦИОНАЛЬНОСТЬ В АВТОМАТИЧЕСКОМ РЕЖИМЕ ПОДДЕРЖИВАЕТ ВСЕ РАЗРЯДНОСТИ ПРИЛОЖЕНИЙ (32|64), КОТОРЫЕ ЗАПУЩЕНЫ В СЕСИИ. PYTHON x64 ИМЕЕТ ВОЗМОЖНОСТЬ ВЗЗАИМОДЕЙСТВИЯ С x32 UIO ОБЪЕКТАМИ, НО МЫ РЕКОМЕНДУЕМ ДОПОЛНИТЕЛЬНО ИСПОЛЬЗОВАТЬ ИНТЕРПРЕТАТОР PYTHON x32 (ПОДРОБНЕЕ СМ. ФУНКЦИЮ Configure())

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"},{"title":"DEMO", "depth_start": 5, "depth_end": 5}]         
lNotepadOKSelector = [{"title":"notepad"},{"title":"OK"}]
lNotepadCancelSelector = [{"title":"notepad"},{"title":"Cancel"}]
lDemoBaseUIOExistList = UIDesktop.UIOSelectorsSecs_WaitDisappear_List([lDemoBaseUIOSelector, lNotepadOKSelector, lNotepadCancelSelector]) # Ожидать исчезновение UIO объектов
```


* **Параметры**

    
    * **inSpecificationListList** (*list**, **обязательный*) – Список UIO селекторов, которые определяют критерии поиска UIO объектов

        Пример: [
        [{«title»:»notepad»},{«title»:»OK»}],
        [{«title»:»notepad»},{«title»:»Cancel»}]

    ]



    * **inWaitSecs** (*float**, **необязательный*) – Количество секунд, которые отвести на ожидание исчезновения UIO объектов. По умолчанию 24 часа (86400 секунд)


    * **inFlagWaitAllInMoment** – True - Ожидать до того момента, пока не исчезнут все запрашиваемые UIO объекты на рабочей области



* **Результат**

    Список индексов, которые указывают на номер входящих UIO селекторов, которые были обнаружены на рабочей области. Пример: [0,2]



### pyOpenRPA.Robot.UIDesktop.UIOXY_SearchChild_ListDict(inRootElement, inX, inY, inHierarchyList=None)
L-,W+: Техническая функция: Получить иерархию вложенности UIO объекта по заданным корневому UIO объекту, координатам X и Y.

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"}]            
lUIO = UIDesktop.UIOSelector_Get_UIO(lDemoBaseUIOSelector) # Получить UIO объект с помощью UIO селектора
lUIOHierarchyList = UIDesktop.UIOXY_SearchChild_ListDict(lUIO, 100, 200) # Получить UIO объект с помощью UIO селектора родительского элемента и координат X / Y
```


* **Параметры**

    
    * **inRootElement** (*object UIO**, **обязательный*) – родительский UIO объект, полученный ранее с помощью UIO селектора.


    * **inX** (*int**, **обязательный*) – родительский UIO объект, полученный ранее с помощью UIO селектора.


    * **inY** (*int**, **обязательный*) – родительский UIO объект, полученный ранее с помощью UIO селектора.



* **Результат**

    Список словарей - уровней UIO объектов



### pyOpenRPA.Robot.UIDesktop.UIO_FocusHighlight(lWrapperObject, colour='green', thickness=2, fill=None, rect=None)
L-,W+: Установить фокус и выполнить подсветку UIO объекта на экране

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"}]            
lUIO = UIDesktop.UIOSelector_Get_UIO(lDemoBaseUIOSelector) # Получить UIO объект по UIO селектору
UIDesktop.UIO_FocusHighlight(lUIO) # Установить фокус и подсветить UIO объект по UIO селектору зеленым цветом с толщиной подсветки 2 px.
```


* **Параметры**

    
    * **lWrapperObject** (*object UIO**, **обязательный*) – UIO объект, который будет подсвечен


    * **colour** (*str**, **необязательный*) – цвет подсветки UIO объекта. Варианты: „red“, „green“, „blue“. По умолчанию „green“


    * **thickness** (*int**, **необязательный*) – толщина подсветки UIO объекта. По умолчанию 2



### pyOpenRPA.Robot.UIDesktop.UIO_GetCtrlIndex_Int(inElement)
L-,W+: Получить индекс UIO объекта inElement в списке родительского UIO объекта.

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"}]            
lUIO = UIDesktop.UIOSelector_Get_UIO(lDemoBaseUIOSelector) # Получить UIO объект по UIO селектору.
lUIOIndexInt = UIDesktop.UIO_GetCtrlIndex_Int(lUIO) # Получить индекс UIO объекта в списке у родительского UIO объекта.
```


* **Параметры**

    **inElement** (*list**, **обязательный*) – UIO объект, для которого требуется определить индекс в списке родительского UIO объекта.



* **Результат**

    int, индекс UIO объекта в списке родительского UIO объекта



### pyOpenRPA.Robot.UIDesktop.UIO_Highlight(lWrapperObject, colour='green', thickness=2, fill=None, rect=None, inFlagSetFocus=False)
L-,W+: Выполнить подсветку UIO объекта на экране

```
# UIDesktop: Взаимодействие с UI объектами приложений
from pyOpenRPA.Robot import UIDesktop
# 1С: UIO Селектор выбора базы
lDemoBaseUIOSelector = [{"title":"Запуск 1С:Предприятия","class_name":"V8TopLevelFrameTaxiStarter","backend":"uia"}]            
lUIO = UIDesktop.UIOSelector_Get_UIO(lDemoBaseUIOSelector) # Получить UIO объект по UIO селектору
UIDesktop.UIO_Highlight(lUIO) # Подсветить UIO объект по UIO селектору зеленым цветом с толщиной подсветки 2 px.
```


* **Параметры**

    
    * **lWrapperObject** (*object UIO**, **обязательный*) – UIO объект, который будет подсвечен


    * **colour** (*str**, **необязательный*) – цвет подсветки UIO объекта. Варианты: „red“, „green“, „blue“. По умолчанию „green“


    * **thickness** (*int**, **необязательный*) – толщина подсветки UIO объекта. По умолчанию 2


    * **inFlagSetFocus** (*bool**, **необязательный*) – признак установки фокуса на UIO объект перед подсветкой. По умолчанию False


## Селектор UIO

Селектор UIO - адрес одного и/или более UIO объектов. Селектор представлен в формате списка (list) словарей (dict) атрибутивных критериев. Поддерживает формат JSON, что позволяет обеспечить удобство форматирования и передачи через web интерфейс студии / оркестратора.

UIO селектор — это список характеристических словарей (спецификаций UIO). Данные спецификации UIO содержат условия, с помощью которых библиотека pyOpenRPA определит UIO, удовлетворяющий условиям, заданным в спецификации UIO. Индекс спецификации UIO в списке UIO селектора харакетризует уровень вложенности целевого UIO. Говоря другим языком, UIO селектор — это перечень условий, под которые может попасть 0, 1 или n UIO.

Ниже приведен перечень атрибутов — условий, которые можно использовать в спецификациях UIO:

**Формат селектора:**

[

    {

    > «depth_start» :: [int, начинается с 1] :: глубина, с которой начинается поиск (по умолчанию 1),

    > «depth_end» :: [int, начинается с 1] :: глубина, до которой ведется поиск (по умолчанию 1),

    > «ctrl_index» || «index» :: [int, начинается с 0] :: индекс UIO в списке у родительского UIO,

    > «title» :: [str] :: идентичное наименование атрибута *title* искомого объекта UIO,

    > «title_re» :: [str] :: регулярное выражение (python диалект) для отбора UIO, у которого атрибут *title* должен удовлетворять условию данного регулярного выражения,

    > «rich_text» :: [str] :: идентичное наименование атрибута *rich_text* искомого объекта UIO,

    > «rich_text_re» :: [str] :: регулярное выражение (python диалект) для отбора UIO, у которого атрибут *rich_text* должен удовлетворять условию данного регулярного выражения,

    > «class_name» :: [str] :: идентичное наименование атрибута *class_name* искомого объекта UIO,

    > «class_name_re» :: [str] :: регулярное выражение (python диалект) для отбора UIO, у которого атрибут *class_name* должен удовлетворять условию данного регулярного выражения,

    > «friendly_class_name» :: [str] :: идентичное наименование атрибута *friendly_class_name* искомого объекта UIO,

    > «friendly_class_name_re» :: [str] :: регулярное выражение (python диалект) для отбора UIO, у которого атрибут *friendly_class_name* должен удовлетворять условию данного регулярного выражения,

    > «control_type» :: [str] :: идентичное наименование атрибута *control_type* искомого объекта UIO,

    > «control_type_re» :: [str] :: регулярное выражение (python диалект) для отбора UIO, у которого атрибут *control_type* должен удовлетворять условию данного регулярного выражения,

    > «is_enabled» :: [bool] :: признак, что UIO доступен для выполнения действий,

    > «is_visible» :: [bool] :: признак, что UIO отображается на экране,

    > «backend» :: [str, «win32» || «uia»] :: вид способа адресации к UIO (по умолчанию «win32»). Внимание! Данный атрибут может быть указан только для первого элемента списка UIO селектора. Для остальных элементов списка данный атрибут будет проигнорирован.

    },
    { … спецификация UIO следующего уровня иерархии }

]

**Пример UIO селектора:**
[

> {«class_name»:»CalcFrame», «backend»:»win32»}, # Спецификация UIO 1-го уровня вложенности
> {«title»:»Hex», «depth_start»:3, «depth_end»: 3} # Спецификация UIO 1+3-го уровня вложенности (так как установлены атрибуты depth_start|depth_stop, определяющие глубину поиска UIO)

]

**UIO объект - свойства и методы (общие)**


* process_id(): Возвращает идентификатор процесса, которому принадлежит это окно


* window_text(): Текст окна элемента. Довольно много элементов управления имеют другой текст, который виден, например, элементы управления редактированием обычно имеют пустую строку для window_text, но все равно имеют текст, отображаемый в окне редактирования.


* rectangle(): Возвращает прямоугольник элемента: {«сверху», «слева», «справа», «снизу»} Прямоугольник() - это прямоугольник элемента на экране. Координаты указаны в левом верхнем углу экрана. Этот метод возвращает прямоугольную структуру, которая имеет атрибуты - top, left, right, bottom. и имеет методы width() и height(). См. раздел win32structures.Прямую кишку для получения дополнительной информации.


* right_click_input(coords=(None, None)): Щелкните правой кнопкой мыши на указанных координатах


* click_input(button=“left“, coords=(None, None), button_down=True, button_up=True, double=False, wheel_dist=0, use_log=True, pressed=““, absolute=False, key_down=True, key_up=True): Щелкните по указанным координатам кнопкой мыши, чтобы щелкнуть. Один из «влево», «вправо», «посередине» или «x» (по умолчанию: «влево», «переместить» - это особый случай) определяет координаты, по которым нужно щелкнуть.(По умолчанию: центр элемента управления) дважды Укажите, следует ли выполнять двойной щелчок или нет (по умолчанию: False) wheel_dist Расстояние для перемещения колеса мыши (по умолчанию: 0) Внимание: Этот метод отличается от метода щелчка тем, что он требует, чтобы элемент управления был виден на экране, но выполняет более реалистичную симуляцию щелчка. Этот метод также уязвим, если пользователь перемещает мышь, поскольку это может легко переместить мышь с элемента управления до завершения click_input.


* double_click_input(button=“left“, coords=(None, None)): Дважды щелкните по указанным координатам


* press_mouse_input(button=“left“, coords=(None, None), pressed=““, absolute=True, key_down=True, key_up=True): Нажмите кнопку мыши с помощью SendInput


* drag_mouse_input(dst=(0, 0), src=None, button=“left“, pressed=““, absolute=True): Нажмите на src, перетащите его и перетащите на dst dst - это объект-оболочка назначения или просто координаты. src - это исходный объект-оболочка или координаты. Если src равен None, self используется в качестве исходного объекта. кнопка - это кнопка мыши, которую нужно удерживать во время перетаскивания. Это может быть “влево”, “вправо”, “посередине” или “x”. Нажата клавиша на клавиатуре, которую нужно нажимать во время перетаскивания. абсолютные указывает, следует ли использовать абсолютные координаты для расположения указателя мыши


* wheel_mouse_input(coords=(None, None), wheel_dist=1, pressed=““): Прокрутить колесо мыши


* draw_outline(colour=“green“, thickness=2, fill=<MagicMock name=“mock.win32defines.BS_NULL“ id=“140124673757368“>, rect=None): Нарисуйте контур вокруг окна. цвет может быть либо целым числом, либо одним из «красного», «зеленого», «синего» (по умолчанию «зеленый») толщина толщина прямоугольника (по умолчанию 2) заливка как заполнить прямоугольник (по умолчанию BS_NULL) укажите координаты прямоугольника для рисования (по умолчанию используется прямоугольник элемента управления)


* element_info: Свойство, доступное только для чтения, для получения объекта ElementInfo


* from_point(x, y): Получить объект-оболочку для элемента в заданных координатах экрана (x, y)


* get_properties(): Возвращает свойства элемента управления в виде словаря.


* is_child(parent): Возвращает значение True, если этот элемент является дочерним элементом ‘parent’. Элемент является дочерним элементом другого элемента, когда он является прямым элементом другого элемента. Элемент является прямым потомком данного элемента, если родительский элемент является цепочкой родительских элементов для дочернего элемента.


* is_dialog(): Возвращает значение True, если элемент управления является окном верхнего уровня


* is_enabled(): Независимо от того, включен элемент или нет. Проверяет, что как родительский элемент верхнего уровня (возможно, диалоговое окно), которому принадлежит этот элемент, так и сам элемент включены.Если вы хотите дождаться, пока элемент станет включенным (или дождаться, пока он станет отключенным), используйте Application.wait(„visible“) или Application.wait_not(„visible“).Если вы хотите немедленно вызвать исключение, если элемент не включен, вы можете использовать BaseWrapper.verify_enabled(). Функция BaseWrapper.Verify Ready() вызывается, если окно одновременно не видно и не включено.


* is_visible(): Является ли элемент видимым или нет. Проверяет, видны ли как родительский элемент верхнего уровня (возможно, диалоговое окно), которому принадлежит этот элемент, так и сам элемент. Если вы хотите дождаться, пока элемент станет видимым (или дождаться, пока он станет скрытым), используйте Application.wait(„visible“) или Application.wait_not(„visible“). Если вы хотите немедленно вызвать исключение, если элемент не виден, вы можете использовать BaseWrapper.verify_visible(). Базовая оболочка.verify_actible() вызывается, если элемент одновременно не виден и не включен.


* parent(): Возвращает родительский элемент этого элемента Обратите внимание, что родительским элементом элемента управления не обязательно является диалоговое окно или другое главное окно. Например, поле группы может быть родительским для некоторых переключателей. Чтобы получить главное (или окно верхнего уровня), затем используйте BaseWrapper.top_level_parent().


* root(): Возвращаемая оболочка для корневого элемента (рабочий стол)


* set_focus(): Установить фокус на этот элемент


* texts(): Возвращает текст для каждого элемента этого элемента управления Это список строк для элемента управления. Часто переопределяется извлечение всех строк из элемента управления с несколькими элементами. Это всегда список с одной или несколькими строками: Первый элемент - это текст окна элемента управления Последующие элементы содержат текст любых элементов элемента управления (например, элементы в listbox/combobox, вкладки в tabcontrol)


* type_keys(keys, pause=None, with_spaces=False, with_tabs=False, with_newlines=False, turn_off_numlock=True, set_foreground=True, vk_packet=True): Введите ключи для элемента с помощью клавиатуры.send_keys. Ограниченная функциональность. Для более полной функциональности рекомендуем ознакомится с pyOpenPRA.Robot.Keyboard


* was_maximized(): Проверить, было ли окно развернуто перед сворачиванием или нет

**UIO свойства и методы (дополнение к базовым методам для win32 элементов)**

**Кнопка (Button || CheckBox || RadioButton || GroupBox)**


* check(): Установить флажок


* uncheck(): Снять флажок


* get_check_state(): Вернуть состояние проверки флажка. Состояние проверки представлено целым числом 0 - непроверено 1 - проверено 2 - неопределенно. Следующие константы определены в модуле win32defines BST_UNCHECKED = 0 BST_CHECKED = 1 BST_INDETERMINATE = 2


* click(button=“left“, pressed=““, coords=(0, 0), double=False, absolute=False): Клик на кнопку управления


* is_checked(): Возвращает True, если флажок установлен, False, если флажок не установлен, None, если значение не определено


* is_dialog(): Кнопки никогда не являются диалоговыми окнами, поэтому возвращайте значение False


* set_check_indeterminate(): Установить флажок в положение неопределенный


* friendly_class_name(): Возвращает имя класса кнопки. Они могут выглядеть следующим образом: Кнопки, этот метод возвращает “Button”; Флажки, этот метод возвращает “флажок”; RadioButtons, этот метод возвращает “RadioButton”; GroupBoxes, этот метод возвращает “GroupBox”

**Поле выбора нескольких значений из списка (ComboBox)**


* friendlyclassname = „ComboBox“


* windowclasses = [„ComboBox“, „WindowsForms\\d\*.COMBOBOX..\*“, „.\*ComboBox“]


* dropped_rect(): Получить выпадающий прямоугольник в поле со списком


* get_properties(): Возвращает свойства элемента управления в виде словаря


* item_count(): Возвращает количество элементов в поле со списком


* item_data(item): Возвращает данные элемента, связанные с элементом, если таковые имеются


* item_texts(): Возвращает текст элементов выпадающего списка


* select(item): Выбрать элемент со списком элемент может быть либо индексом элемента для выбора на основе 0, либо строкой, которую вы хотите выбрать


* selected_index(): Возвращает выбранный индекс


* selected_text():Возвращает выделенный текст


* texts(): Возвращает текст элементов в выпадающем списке

**Поле ввода (Edit)**


* friendlyclassname = „Edit“


* windowclasses = [„Edit“, „.\*Edit“, „TMemo“, „WindowsForms\\d\*.EDIT..\*“, „ThunderTextBox“, „ThunderRT6TextBox“]


* get_line(line_index): Возвращает указанную строку


* line_count(): Возвращает, сколько строк есть в редактировании


* line_length(line_index): Возвращает количество символов в строке


* select(start=0, end=None): Установите выбор редактирования элемента управления редактированием


* selection_indices(): Начальный и конечный индексы текущего выбора


* set_edit_text(text, pos_start=None, pos_end=None): Задать текст элемента управления редактированием


* set_text(text, pos_start=None, pos_end=None): Задать текст элемента управления редактированием


* set_window_text(text, append=False): Переопределите set_window_text для элементов управления редактированием, поскольку он не должен использоваться для элементов управления редактированием. Элементы управления редактированием должны использовать либо set_edit_text(), либо type_keys() для изменения содержимого элемента управления редактированием.


* text_block(): Получить текст элемента управления редактированием


* texts(): Получить текст элемента управления редактированием

**Поле выбора 1-го значения из списка (ListBox)**


* friendlyclassname = „ListBox“


* windowclasses = [„ListBox“, „WindowsForms\\d\*.LISTBOX..\*“, „.\*ListBox“]


* get_item_focus(): Возвращает индекс текущего выбора в списке


* is_single_selection(): Проверить, имеет ли поле списка режим одиночного выбора


* item_count(): Возвращает количество элементов в списке


* item_data(i): Возвращает item_data, если таковые имеются, связанные с элементом


* item_texts(): Возвращает текст элементов списка


* select(item, select=True): Выбрать элемент списка элемент может быть либо индексом элемента для выбора на основе 0, либо строкой, которую вы хотите выбрать


* selected_indices(): Выбранные в данный момент индексы списка


* set_item_focus(item): Установить фокус по элементу


* texts(): Получить текст элемента управления редактированием

**Выпадающее меню (PopupMenu)**


* friendlyclassname = „PopupMenu“


* windowclasses = [„#32768“]


* is_dialog(): Возвращает, является ли это диалогом

**Текст (Static)**


* friendlyclassname= „Static“


* windowclasses= [„Static“, „WindowsForms\\d\*.STATIC..\*“, „TPanel“, „.\*StaticText“]

**Инициализация 2-х разрядностей для UIO**

pyOpenRPA позволяет обеспечить максимальную совместимость со всеми приложениями, которые выполняются на компьютере. Мы рекомендуем разрабатывать робота под интерпретатором Python x64. В дополнение к нему Вы можете подключить Python x32 (см. ниже пример подключения). Если планируемый робот не будет взаимодействовать через pyOpenRPA.Robot.UIDesktop с другой разрядность, то эту настройку можно не применять.

```
from pyOpenRPA.Robot import UIDesktop
# В нашем случае процесс робота будет исполняться на Python x64. Дополнительно подключим Python x32 (делать это только, если вы планируете работать в другой разрядностью в рамках робота)
lPyOpenRPA_SettingsDict = {
        "Python32FullPath": "..\\Resources\\WPy32-3720\\python-3.7.2\\python.exe",# Путь к интерпретатору Python.exe x32
        "Python64FullPath": "..\\Resources\\WPy64-3720\\python-3.7.2.amd64\\python.exe", # Путь к интерпретатору Python.exe x64
        "Python32ProcessName": "pyOpenRPA_UIDesktopX32.exe", # Наименование процесса робота x32 в диспетчере задач. Установите свое наименование
        "Python64ProcessName": "pyOpenRPA_UIDesktopX64.exe" # Наименование процесса робота x64 в диспетчере задач. Установите свое наименование
}
# Инициализировать 2-й разрядность.
UIDesktop.Utils.ProcessBitness.SettingsInit(lPyOpenRPA_SettingsDict)
# Теперь при вызове функций pyOpenRPA.Robot.UIDesktop платформа pyOpenRPA будет отслеживать разрядность приложения и отправлять соответсвующий вызов на идентичную разрядность.
```

## Быстрая навигация


* [Сообщество pyOpenRPA (telegram)](https://t.me/pyOpenRPA)


* [Сообщество pyOpenRPA (tenchat)](https://tenchat.ru/iMaslov?utm_source=19f2a84f-3268-437f-950c-d987ae42af24)


* [Сообщество pyOpenRPA (вконтакте)](https://vk.com/pyopenrpa)


* [Презентация pyOpenRPA](https://pyopenrpa.ru/Index/pyOpenRPA_product_service.pdf)


* [Портал pyOpenRPA](https://pyopenrpa.ru)


* [Репозиторий pyOpenRPA](https://gitlab.com/UnicodeLabs/OpenRPA)

		.. v1.3.1 replace:: v1.3.1
