# 2. Функции StopSafe

How to use StopSafe on the robot side

```
from pyOpenRPA.Tools import StopSafe
StopSafe.Init(inLogger=None)
StopSafe.IsSafeStop() # True - WM_CLOSE SIGNAL has come. taskkill /im someprocess.exe
```

# How to use
# from pyOpenRPA.Tools import StopSafe
# StopSafe.Init(inLogger=None)
# StopSafe.IsSafeStop() # True - WM_CLOSE SIGNAL has come. taskkill /im someprocess.exe

**Functions:**

| `Init`([inLogger])

 | Init the StopSafe module.

 |
| `IsStopSafe`()

                                      | Check if stop signal has come.

                                                                                                                                                                                 |

### pyOpenRPA.Tools.StopSafe.Init(inLogger=None)
Init the StopSafe module. After that you can use def IsStopSafe() to check if close signal has come.


* **Параметры**

    **inLogger** – Logger to log messages about StopSafe



* **Результат**

    


### pyOpenRPA.Tools.StopSafe.IsStopSafe()
Check if stop signal has come.


* **Результат**

    

**Functions:**

| `LiveDebugCheckThread`(\*\*inKWARGS)

                  | Create thread to wait file appear «init_debug» in the working directory.

                                                                                                                                       |

### pyOpenRPA.Tools.Debugger.LiveDebugCheckThread(\*\*inKWARGS)
Create thread to wait file appear «init_debug» in the working directory.

		.. v1.3.1 replace:: v1.3.1
