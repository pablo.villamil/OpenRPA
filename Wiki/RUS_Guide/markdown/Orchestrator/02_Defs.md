# 2. Функции

## Общее

Раздел содержит всю необходимую информацию о функциях pyOpenRPA.Orchestrator

При необходимости вы всегда можете обратиться в центр поддержки клиентов pyOpenRPA. Контакты: 2. Лицензия & Контакты

## Что такое активность (ActivityItem)?

Архитектура pyOpenRPA позволяет обмениваться сообщениями о выполнении функций через механизм активностей (ActivityItem).

На стороне Агента и Оркестратора реализована процессорная очередь, которая последовательно выполняет поставленные активности. Результат этих активностей сообщается инициатору (см. функции группы Agent… в Оркестраторе)

## Функции

```
# ПРИМЕР 1 (ОСНОВНОЙ)
from pyOpenRPA import Orchestrator
Orchestrator.OSCMD(inCMDStr = "git status", inRunAsyncBool=True)

# ПРИМЕР 2 (ВСПОМОГАТЕЛЬНЫЙ)
from pyOpenRPA.Orchestrator import __Orchestrator__
__Orchestrator__.OSCMD(inCMDStr = "git status", inRunAsyncBool=True)
```

**Группа функций Agent…**

Взаимодействие между Оркестратором и Агентом, который развертнут на других графических сессиях, где будет происходить запуск робота.

**Группа функций GSettings…**

Вспомогательные функции для работы с глобальным словарем настроек Оркестратора

**Группа функций Storage…**

Функции для взаимодействия со специальным хранилищем переменных, предназначенного для хранения информации от роботов.

!ВНИМАНИЕ! Данное хранилище сохраняется при перезагрузке Оркестратора из панели управления.

**Группа функций OS…**

Функции взаимодействия с командной строкой на сессии, где запущен Оркестратор.

**Группа функций Process…**

Запуск / остановка процессов на сессии Оркестратора.

**Группа функций Processor…**

Функции взаимодействия с процессорной очередью. Если требуется выполнить синхронизацию нескольких разных задач, то можно их отправлять в процессорную очередь.

**Группа функций Python…**

Функции взаимодействия с Python модулями.

**Группа функций RDPSession…**

Запуск, отключение, перезапуск, отправка CMD команд, раскрыть на весь экран на RDP сессию

**Группа функций Web…**

Управление веб-сервером Оркестратора.

**Группа функций UAC…**

Управление ролевой моделью доступа пользователей к панели управления Оркестратора. Актуально для подключения бизнес-пользователей.

**Группа функций Scheduler…**

Установка расписания на различные активности.

Описание каждой функции начинается с обозначения L+,W+, что означает, что функция поддерживается в ОС Linux (L) и поддерживается в Windows (W)

**Functions:**

| `ActivityItemCreate`(inDef[, inArgList, …])

 | L+,W+: Создать Активность (ActivityItem).

 |
| `ActivityItemDefAliasCreate`(inDef[, …])

          | L+,W+: Создать синоним (текстовый ключ) для инициации выполнения функции в том случае, если запрос на выполнения пришел из вне (передача функций невозможна).

 |
| `ActivityItemDefAliasModulesLoad`()

               | L+,W+: Загрузить все функции из импортированных модулей sys.modules в ActivityItem синонимы - полезно для отладки со стороны панели управления.

               |
| `ActivityItemDefAliasUpdate`(inDef, inAliasStr)

   | L+,W+: Обновить синоним (текстовый ключ) для инициации выполнения функции в том случае, если запрос на выполнения пришел из вне (передача функций невозможна).

 |
| `ActivityItemHelperDefAutofill`(inDef)

            | L+,W+: Анализ аргументов функции по синониму (текстовому ключу).

                                                                                               |
| `ActivityItemHelperDefList`([inDefQueryStr])

      | L+,W+: Получить список синонимов (текстовых ключей), доступных для использования в Активностях (ActivityItem).

                                                 |
| `AgentActivityItemAdd`(inHostNameStr, …[, …])

     | L+,W+: Добавить активность в словарь активностей выбранного Агента

                                                                                             |
| `AgentActivityItemExists`(inHostNameStr, …)

       | L+,W+: Выполнить проверку, что активность (ActivityItem) была отправлена на сторону Агента.

                                                                    |
| `AgentActivityItemReturnExists`(inGUIDStr[, …])

   | L+,W+: Выполнить проверку, что активность (ActivityItem) была выполнена на стороне Агента и результат был получен на стороне Оркестратора.

                     |
| `AgentActivityItemReturnGet`(inGUIDStr[, …])

      | L+,W+: Ожидает появления результата по активности (ActivityItem).

                                                                                              |
| `AgentOSCMD`(inHostNameStr, inUserStr, inCMDStr)

  | L+,W+: Отправка команды командной строки на сессию, где работает pyOpenRPA.Agent.

                                                                              |
| `AgentOSFileBinaryDataBase64StrAppend`(…[, …])

    | L+,W+: Добавить бинарную информацию в существующий бинарный файл, который будет расположен по адресу inFilePathStr на стороне Агента с содержимым, декодированным с формата base64: inFileDataBase64Str

 |
| `AgentOSFileBinaryDataBase64StrCreate`(…[, …])

    | L+,W+: Создать бинарный файл, который будет расположен по адресу inFilePathStr на стороне Агента с содержимым, декодированным с формата base64: inFileDataBase64Str

                                     |
| `AgentOSFileBinaryDataBase64StrReceive`(…[, …])

   | L+,W+: Выполнить чтение бинарного файла и получить содержимое в формате base64 (строка)

                                                                                                                 |
| `AgentOSFileBinaryDataBytesCreate`(…[, …])

        | L+,W+: Создать бинарный файл, который будет расположен по адресу inFilePathStr на стороне Агента с содержимым inFileDataBytes

                                                                           |
| `AgentOSFileBinaryDataReceive`(inHostNameStr, …)

  | L+,W+: Чтение бинарного файла на стороне Агента по адресу inFilePathStr.

                                                                                                                                |
| `AgentOSFileSend`(inHostNameStr, inUserStr, …)

    | L+,W+: Отправить файл по адресу inOrchestratorFilePathStr со стороны Оркестратора и сохранить по адресу inAgentFilePathStr на стороне Агента.

                                                           |
| `AgentOSFileTextDataStrCreate`(inHostNameStr, …)

  | L+,W+: Создать текстовый файл, который будет расположен по адресу inFilePathStr на стороне Агента с содержимым inFileDataStr в кодировке inEncodingStr

                                                  |
| `AgentOSFileTextDataStrReceive`(inHostNameStr, …)

 | L+,W+: Чтение текстового файла на стороне Агента по адресу inFilePathStr.

                                                                                                                               |
| `AgentOSLogoff`(inHostNameStr, inUserStr)

         | L+,W+: Выполнить операцию logoff на стороне пользователя.

                                                                                                                                               |
| `AgentProcessWOExeUpperUserListGet`(…[, …])

       | L-,W+: Получить список процессов, которые выполняется на сессии Агента.

                                                                                                                                 |
| `GSettingsGet`([inGSettings])

                     | L+,W+: Вернуть глобальный словарь настроек Оркестратора.

                                                                                                                                                |
| `GSettingsKeyListValueAppend`(inValue[, …])

       | L+,W+: Применить операцию .append к обьекту, расположенному по адресу списка ключей inKeyList в глобальном словаре настроек Оркестратора GSettings.

                                                     |
| `GSettingsKeyListValueGet`([inKeyList, …])

        | L+,W+: Получить значение из глобального словаря настроек Оркестратора GSettings по списку ключей.

                                                                                                       |
| `GSettingsKeyListValueOperatorPlus`(inValue[, …])

 | L+,W+: Применить оператор сложения (+) к обьекту, расположенному по адресу списка ключей inKeyList в глобальном словаре настроек Оркестратора GSettings.

                                                |
| `GSettingsKeyListValueSet`(inValue[, …])

          | L+,W+: Установить значение из глобального словаря настроек Оркестратора GSettings по списку ключей.

                                                                                                     |
| `OSCMD`(inCMDStr[, inRunAsyncBool, inLogger])

     | L-,W+: Отправить команду на выполнение на сессию, где выполняется Оркестратор.

                                                                                                                          |
| `OSCredentialsVerify`(inUserStr, inPasswordStr)

   | L+,W+: Выполнить верификацию доменного (локального) пользователя по паре логин/пароль

                                                                                                                   |
| `OSLogoff`()

                                      | L+,W+: Выполнить отключение сессии, на которой выполняется Оркестратор.

                                                                                                                                 |
| `OSRemotePCRestart`(inHostStr[, inForceBool, …])

  | L-,W+: Отправить сигнал на удаленную перезагрузку операционной системы.

                                                                                                                                 |
| `OSRestart`([inForceBool, inLogger])

              | L+,W+: Отправить сигнал на перезагрузку операционной системы.

                                                                                                                                           |
| `Orchestrator`([inGSettings, …])

                  | L+,W+: Инициализация ядра Оркестратора (всех потоков)

                                                                                                                                                   |
| `OrchestratorInitWait`()

                          | L+,W+: Ожидать инициализацию ядра Оркестратора

                                                                                                                                                          |
| `OrchestratorIsAdmin`()

                           | L+,W+: Проверить, запущен ли Оркестратор с правами администратора.

                                                                                                                                      |
| `OrchestratorIsInited`()

                          | L+,W+: Проверить, было ли проинициализировано ядро Оркестратора

                                                                                                                                         |
| `OrchestratorLoggerGet`()

                         | L+,W+: Получить логгер Оркестратора

                                                                                                                                                                     |
| `OrchestratorPySearchInit`(inGlobPatternStr[, …])

 | L+,W+: Выполнить поиск и инициализацию пользовательских .py файлов в Оркестраторе (например панелей управления роботов)

                                                                                 |
| `OrchestratorRerunAsAdmin`()

                      | L-,W+: Перезапустить Оркестратор с правами локального администратора.

                                                                                                                                   |
| `OrchestratorRestart`([inGSettings])

              | L+,W+: Перезапуск Оркестратора с сохранением информации о запущенных RDP сессиях.

                                                                                                                       |
| `OrchestratorScheduleGet`()

                       | L+,W+: Базовый объект расписания, который можно использовать для запуска / остановки роботов.

                                                                                                           |
| `OrchestratorSessionRestore`([inGSettings])

       | L+,W+: Восстановить состояние Оркестратора, если ранее состояние Оркестратора было сохранено с помощью функции OrchestratorSessionSave:

                                                                 |
| `OrchestratorSessionSave`([inGSettings])

          | L+,W+: Сохранить состояние Оркестратора (для дальнейшего восстановления в случае перезапуска):

                                                                                                          |
| `OrchestratorThreadStart`(inDef, \*inArgList, …)

   | L+,W+: Запустить функцию в отдельном потоке.

                                                                                                                                                            |
| `ProcessDefIntervalCall`(inDef, inIntervalSecFloat)

 | L+,W+: Периодический вызов функции Python.

                                                                                                                                                              |
| `ProcessIsStarted`(inProcessNameWOExeStr)

           | L-,W+: Проверить, запущен ли процесс, который в наименовании содержит inProcessNameWOExeStr.

                                                                                                            |
| `ProcessListGet`([inProcessNameWOExeList])

          | L-,W+: Вернуть список процессов, запущенных на ОС, где работает Оркестратор.

                                                                                                                            |
| `ProcessStart`(inPathStr, inArgList[, …])

           | L-,W+: Запуск процесса на сессии Оркестратора, если на ОС не запущен процесс inStopProcessNameWOExeStr.

                                                                                                 |
| `ProcessStop`(inProcessNameWOExeStr, …[, …])

        | L-,W+: Остановить процесс на ОС, где работает Оркестратор, под учетной записью inUserNameStr.

                                                                                                           |
| `ProcessorActivityItemAppend`([inGSettings, …])

     | L+,W+: Добавить активность (ActivityItem) в процессорную очередь.

                                                                                                                                       |
| `ProcessorActivityItemCreate`(inDef[, …])

           | L+,W+: Создать Активность (ActivityItem).

                                                                                                                                                               |
| `ProcessorAliasDefCreate`(inDef[, inAliasStr, …])

   | L+,W+: Создать синоним (текстовый ключ) для инициации выполнения функции в том случае, если запрос на выполнения пришел из вне (передача функций невозможна).

                                           |
| `ProcessorAliasDefUpdate`(inDef, inAliasStr[, …])

   | L+,W+: Обновить синоним (текстовый ключ) для инициации выполнения функции в том случае, если запрос на выполнения пришел из вне (передача функций невозможна).

                                          |
| `PythonStart`(inModulePathStr, inDefNameStr[, …])

   | L+,W+: Импорт модуля и выполнение функции в процессе Оркестратора.

                                                                                                                                      |
| `RDPSessionCMDRun`(inRDPSessionKeyStr, inCMDStr)

    | L-,W+: Отправить CMD команду на удаленную сесиию через RDP окно (без Агента).

                                                                                                                           |
| `RDPSessionConnect`(inRDPSessionKeyStr[, …])

        | L-,W+: Выполнить подключение к RDP и следить за стабильностью соединения со стороны Оркестратора.

                                                                                                       |
| `RDPSessionDisconnect`(inRDPSessionKeyStr[, …])

     | L-,W+: Выполнить отключение RDP сессии и прекратить мониторить его активность.

                                                                                                                          |
| `RDPSessionFileStoredRecieve`(…[, inGSettings])

     | L-,W+: Получение файла со стороны RDP сессии на сторону Оркестратора через UI инструменты RDP окна (без Агента).

                                                                                        |
| `RDPSessionFileStoredSend`(inRDPSessionKeyStr, …)

   | L-,W+: Отправка файла со стороны Оркестратора на сторону RDP сессии через UI инструменты RDP окна (без Агента).

                                                                                         |
| `RDPSessionLogoff`(inRDPSessionKeyStr[, …])

         | L-,W+: Выполнить отключение (logoff) на RDP сессии и прекратить мониторить активность со стороны Оркестратора.

                                                                                          |
| `RDPSessionMonitorStop`(inRDPSessionKeyStr[, …])

    | L-,W+: Прекратить мониторить активность RDP соединения со стороны Оркестратора.

                                                                                                                         |
| `RDPSessionProcessStartIfNotRunning`(…[, …])

        | L-,W+: Выполнить запуск процесса на RDP сессии через графические UI инструменты (без Агента).

                                                                                                           |
| `RDPSessionProcessStop`(inRDPSessionKeyStr, …)

      | L-,W+: Отправка CMD команды в RDP окне на остановку процесса (без Агента).

                                                                                                                              |
| `RDPSessionReconnect`(inRDPSessionKeyStr[, …])

      | L-,W+: Выполнить переподключение RDP сессии и продолжить мониторить его активность.

                                                                                                                     |
| `RDPTemplateCreate`(inLoginStr, inPasswordStr)

      | L-,W+: Создать шаблон подключения RDP (dict).

                                                                                                                                                           |
| `SchedulerActivityTimeAddWeekly`([…])

               | L+,W+: Добавить активность по расписанию.

                                                                                                                                                               |
| `StorageRobotExists`(inRobotNameStr)

                | L+,W+: Проверить, существует ли ключ inRobotNameStr в хранилище пользовательской информации StorageDict (GSettings > StarageDict)

                                                                       |
| `StorageRobotGet`(inRobotNameStr)

                   | L+,W+: Получить содержимое по ключу робота inRobotNameStr в хранилище пользовательской информации StorageDict (GSettings > StarageDict)

                                                                 |
| `UACKeyListCheck`(inRequest, inRoleKeyList)

         | L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.2, см.

                                                                                                                                                              |
| `UACSuperTokenUpdate`(inSuperTokenStr[, …])

         | L+,W+: Добавить супертокен (полный доступ).

                                                                                                                                                             |
| `UACUpdate`(inADLoginStr[, inADStr, …])

             | L+,W+: Дообогащение словаря доступа UAC пользователя inADStrinADLoginStr.

                                                                                                                               |
| `UACUserDictGet`(inRequest)

                         | L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.2, см.

                                                                                                                                                              |
| `WebAuditMessageCreate`([inAuthTokenStr, …])

        | L+,W+: [ИЗМЕНЕНИЕ В 1.3.1] Создание сообщения ИТ аудита с такими сведениями как (Домен, IP, логин и тд.).

                                                                                               |
| `WebCPUpdate`(inCPKeyStr[, inHTMLRenderDef, …])

     | L+,W+: Добавить панель управления робота в Оркестратор.

                                                                                                                                                 |
| `WebListenCreate`([inServerKeyStr, …])

              | L+,W+: Настроить веб-сервер Оркестратора.

                                                                                                                                                               |
| `WebRequestGet`()

                                   | L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.2] Вернуть экземпляр HTTP запроса, если функция вызвана в потоке, который был порожден для отработки HTTP запроса пользователя.

                                     |
| `WebRequestHostGet`(inRequest)

                      | L+,W+: Получить наименование хоста, с которого поступил запрос

                                                                                                                                          |
| `WebRequestParseBodyBytes`([inRequest])

             | L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.1, см.

                                                                                                                                                              |
| `WebRequestParseBodyJSON`([inRequest])

              | L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.1, см.

                                                                                                                                                              |
| `WebRequestParseBodyStr`([inRequest])

               | L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.1, см.

                                                                                                                                                              |
| `WebRequestParseFile`([inRequest])

                  | L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.1, см.

                                                                                                                                                              |
| `WebRequestParsePath`([inRequest])

                  | L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.1, см.

                                                                                                                                                              |
| `WebRequestResponseSend`(inResponeStr[, …])

         | L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.1, см.

                                                                                                                                                              |
| `WebURLConnectDef`(inMethodStr, inURLStr, …)

        | L+,W+: Подключить функцию Python к URL.

                                                                                                                                                                 |
| `WebURLConnectFile`(inMethodStr, inURLStr, …)

       | L+,W+: Подключить файл к URL.

                                                                                                                                                                           |
| `WebURLConnectFolder`(inMethodStr, inURLStr, …)

     | L+,W+: Подключить папку к URL.

                                                                                                                                                                          |
| `WebURLIndexChange`([inURLIndexStr])

                | L+,W+: Изменить адрес главной страницы Оркестратора.

                                                                                                                                                    |
| `WebUserDomainGet`([inAuthTokenStr])

                | L+,W+: Получить домен авторизованного пользователя.

                                                                                                                                                     |
| `WebUserInfoGet`([inRequest])

                       | L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.2, см.

                                                                                                                                                              |
| `WebUserIsSuperToken`([inAuthTokenStr])

             | L+,W+: [ИЗМЕНЕНИЕ В 1.3.1] Проверить, авторизован ли HTTP запрос с помощью супер токена (токен, который не истекает).

                                                                                   |
| `WebUserLoginGet`([inAuthTokenStr])

                 | L+,W+: Получить логин авторизованного пользователя.

                                                                                                                                                     |
| `WebUserUACCheck`([inAuthTokenStr, inKeyList])

      | L+,W+: Проверить UAC доступ списка ключей для пользователя

                                                                                                                                              |
| `WebUserUACHierarchyGet`([inAuthTokenStr])

          | L+,W+: [ИЗМЕНЕНИЕ В 1.3.1] Вернуть словарь доступа UAC в отношении пользователя, который выполнил HTTP запрос inRequest

                                                                                 |

### pyOpenRPA.Orchestrator.__Orchestrator__.ActivityItemCreate(inDef, inArgList=None, inArgDict=None, inArgGSettingsStr=None, inArgLoggerStr=None, inGUIDStr=None, inThreadBool=False)
L+,W+: Создать Активность (ActivityItem). Активность можно использовать в ProcessorActivityItemAppend или в Processor.ActivityListExecute или в функциях работы с Агентами.

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

# ВАРИАНТ 1
def TestDef(inArg1Str, inGSettings, inLogger):
    pass
lActivityItem = Orchestrator.ActivityItemCreate(
    inDef = TestDef,
    inArgList=[],
    inArgDict={"inArg1Str": "ArgValueStr"},
    inArgGSettingsStr = "inGSettings",
    inArgLoggerStr = "inLogger")
# lActivityItem:
#   {
#       "Def":TestDef,
#       "ArgList":inArgList,
#       "ArgDict":inArgDict,
#       "ArgGSettings": "inArgGSettings",
#       "ArgLogger": "inLogger"
#   }

# ВАРИАНТ 2
def TestDef(inArg1Str):
    pass
Orchestrator.ActivityItemDefAliasUpdate(
    inGSettings = gSettings,
    inDef = TestDef,
    inAliasStr="TestDefAlias")
lActivityItem = Orchestrator.ActivityItemCreate(
    inDef = "TestDefAlias",
    inArgList=[],
    inArgDict={"inArg1Str": "ArgValueStr"},
    inArgGSettingsStr = None,
    inArgLoggerStr = None)
# lActivityItem:
#   {
#       "Def":"TestDefAlias",
#       "ArgList":inArgList,
#       "ArgDict":inArgDict,
#       "ArgGSettings": None,
#       "ArgLogger": None
#   }
```


* **Параметры**

    
    * **inDef** – Функция Python или синоним (текстовый ключ)


    * **inArgList** – Список (list) неименованных аргументов к функции


    * **inArgDict** – Словарь (dict) именованных аргументов к функции


    * **inArgGSettingsStr** – Текстовое наименование аргумента GSettings (если требуется передавать)


    * **inArgLoggerStr** – Текстовое наименование аргумента logger (если требуется передавать)


    * **inGUIDStr** – ГУИД идентификатор активности (ActivityItem). Если ГУИД не указан, то он будет сгенерирован автоматически


    * **inThreadBool** – True - выполнить ActivityItem в новом потоке; False - выполнить последовательно в общем потоке процессорной очереди



* **Результат**

    lActivityItemDict= {

        «Def»:inDef, # def link or def alias (look gSettings[«Processor»][«AliasDefDict»])
        «ArgList»:inArgList, # Args list
        «ArgDict»:inArgDict, # Args dictionary
        «ArgGSettings»: inArgGSettingsStr, # Name of GSettings attribute: str (ArgDict) or index (for ArgList)
        «ArgLogger»: inArgLoggerStr, # Name of GSettings attribute: str (ArgDict) or index (for ArgList)
        «GUIDStr»: inGUIDStr,
        «ThreadBool»: inThreadBool

    }




### pyOpenRPA.Orchestrator.__Orchestrator__.ActivityItemDefAliasCreate(inDef, inAliasStr=None, inGSettings=None)
L+,W+: Создать синоним (текстовый ключ) для инициации выполнения функции в том случае, если запрос на выполнения пришел из вне (передача функций невозможна).

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

def TestDef():
    pass
lAliasStr = Orchestrator.ActivityItemDefAliasCreate(
    inGSettings = gSettings,
    inDef = TestDef,
    inAliasStr="TestDefAlias")
# Now you can call TestDef by the alias from var lAliasStr with help of ActivityItem (key Def = lAliasStr)
```


* **Параметры**

    
    * **inDef** – функция Python


    * **inAliasStr** – Строковый ключ (синоним), который можно будет использовать в Активности (ActivityItem)


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



* **Результат**

    Строковый ключ, который был назначен. Ключ может быть изменен, если входящий текстовый ключ был уже занят.



### pyOpenRPA.Orchestrator.__Orchestrator__.ActivityItemDefAliasModulesLoad()
L+,W+: Загрузить все функции из импортированных модулей sys.modules в ActivityItem синонимы - полезно для отладки со стороны панели управления.


### pyOpenRPA.Orchestrator.__Orchestrator__.ActivityItemDefAliasUpdate(inDef, inAliasStr, inGSettings=None)
L+,W+: Обновить синоним (текстовый ключ) для инициации выполнения функции в том случае, если запрос на выполнения пришел из вне (передача функций невозможна).

```
# USAGE
from pyOpenRPA import Orchestrator

def TestDef():
    pass
Orchestrator.ActivityItemDefAliasUpdate(
    inGSettings = gSettings,
    inDef = TestDef,
    inAliasStr="TestDefAlias")
# Now you can call TestDef by the alias "TestDefAlias" with help of ActivityItem (key Def = "TestDefAlias")
```


* **Параметры**

    
    * **inDef** – функция Python


    * **inAliasStr** – Строковый ключ (синоним), который можно будет использовать в Активности (ActivityItem)


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



* **Результат**

    Строковый ключ, который был назначен. Ключ будет тем же, если входящий текстовый ключ был уже занят.



### pyOpenRPA.Orchestrator.__Orchestrator__.ActivityItemHelperDefAutofill(inDef)
L+,W+: Анализ аргументов функции по синониму (текстовому ключу).


* **Параметры**

    **inDef** – Часть текстового ключ (начало / середина / конец)



* **Результат**

    Преднастроенная структура активности (ActivityItem)         
    {

    > »Def»: None,
    > «ArgList»: [],
    > «ArgDict»: {},
    > «ArgGSettingsStr»: None,
    > «ArgLoggerStr»: None

    }




### pyOpenRPA.Orchestrator.__Orchestrator__.ActivityItemHelperDefList(inDefQueryStr=None)
L+,W+: Получить список синонимов (текстовых ключей), доступных для использования в Активностях (ActivityItem).


* **Параметры**

    **inDefStr** – Часть текстового ключ (начало / середина / конец)



* **Результат**

    Список доступных ключей в формате: [«ActivityItemDefAliasUpdate», «ActivityItemDefAliasCreate», etc…]



### pyOpenRPA.Orchestrator.__Orchestrator__.AgentActivityItemAdd(inHostNameStr, inUserStr, inActivityItemDict, inGSettings=None)
L+,W+: Добавить активность в словарь активностей выбранного Агента


* **Параметры**

    
    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)


    * **inHostNameStr** – Наименование хоста, на котором запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inUserStr** – Наименование пользователя, на графической сессии которого запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inActivityItemDict** – Активность (ActivityItem). См. функцию ProcessorActivityitemCreate



* **Результат**

    ГУИД (GUID) строка Активности (ActivityItem). Далее можно ожидать результат этой функции по ГУИД с помощью функции AgentActivityItemReturnGet



### pyOpenRPA.Orchestrator.__Orchestrator__.AgentActivityItemExists(inHostNameStr, inUserStr, inGUIDStr, inGSettings=None)
L+,W+: Выполнить проверку, что активность (ActivityItem) была отправлена на сторону Агента.


* **Параметры**

    
    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)


    * **inGUIDStr** – ГУИД (GUID) активности (ActivityItem)



* **Результат**

    True - Активность присутствует ; False - Активность еще не была отправлена на сторону Агента



### pyOpenRPA.Orchestrator.__Orchestrator__.AgentActivityItemReturnExists(inGUIDStr, inGSettings=None)
L+,W+: Выполнить проверку, что активность (ActivityItem) была выполнена на стороне Агента и результат был получен на стороне Оркестратора.


* **Параметры**

    
    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)


    * **inGUIDStr** – ГУИД (GUID) активности (ActivityItem)



* **Результат**

    True - Активность присутствует; False - Активность еще не была выполнена на стороне Агента



### pyOpenRPA.Orchestrator.__Orchestrator__.AgentActivityItemReturnGet(inGUIDStr, inCheckIntervalSecFloat=0.5, inGSettings=None, inTimeoutSecFloat=None)
L+,W+: Ожидает появления результата по активности (ActivityItem). Возвращает результат выполнения активности.

!ВНИМАНИЕ! Замораживает поток, пока не будет получен результат. 
!ВНИМАНИЕ! Запускать следует после того как будет инициализировано ядро Оркестратора (см. функцию OrchestratorInitWait), иначе будет инициирована ошибка.


* **Параметры**

    
    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)


    * **inGUIDStr** – ГУИД (GUID) активности (ActivityItem)


    * **inCheckIntervalSecFloat** – Интервал в секундах, с какой частотой выполнять проверку результата. По умолчанию 0.5



* **Результат**

    Результат выполнения активности. !ВНИМАНИЕ! Возвращаются только то результаты, которые могут быть интерпретированы в JSON формате.



### pyOpenRPA.Orchestrator.__Orchestrator__.AgentOSCMD(inHostNameStr, inUserStr, inCMDStr, inRunAsyncBool=True, inSendOutputToOrchestratorLogsBool=True, inCMDEncodingStr='cp1251', inGSettings=None, inCaptureBool=True)
L+,W+: Отправка команды командной строки на сессию, где работает pyOpenRPA.Agent. Результат выполнения команды можно выводить в лог оркестратора.


* **Параметры**

    
    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)


    * **inHostNameStr** – Наименование хоста, на котором запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inUserStr** – Наименование пользователя, на графической сессии которого запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inCMDStr** – Команда для исполнения на стороне сессии Агента


    * **inRunAsyncBool** – True - Агент не ожидает окончания выполнения команды. !ВНИМАНИЕ! Логирование в такой ситуации будет невозможно; False - Агент ожидает окончания выполнения операции.


    * **inSendOutputToOrchestratorLogsBool** – True - отправлять весь вывод от команды в логи Оркестратора; Flase - Не отправлять; Default True


    * **inCMDEncodingStr** – Кодировка DOS среды, в которой исполняется команда. Если некорректно установить кодировку - русские символы будут испорчены. По умолчанию установлена «cp1251»


    * **inCaptureBool** – True - не запускать приложение как отдельное. Результат выполнения команды будет выводиться в окне Агента (если окно Агента присутствует на экране). False - команда будет запущена в отдельном DOS окне.



* **Результат**

    ГУИД (GUID) строка Активности (ActivityItem). Далее можно ожидать результат этой функции по ГУИД с помощью функции AgentActivityItemReturnGet



### pyOpenRPA.Orchestrator.__Orchestrator__.AgentOSFileBinaryDataBase64StrAppend(inHostNameStr, inUserStr, inFilePathStr, inFileDataBase64Str, inGSettings=None)
L+,W+: Добавить бинарную информацию в существующий бинарный файл, который будет расположен по адресу inFilePathStr на стороне Агента с содержимым, декодированным с формата base64: inFileDataBase64Str


* **Параметры**

    
    * **inHostNameStr** – Наименование хоста, на котором запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inUserStr** – Наименование пользователя, на графической сессии которого запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inFilePathStr** – Полный путь к сохраняемому файлу на стороне Агента.


    * **inFileDataBase64Str** – Строка в формате base64 для отправки в создаваемый файл на стороне Агента.


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



* **Результат**

    ГУИД (GUID) строка Активности (ActivityItem). Далее можно ожидать результат этой функции по ГУИД с помощью функции AgentActivityItemReturnGet



### pyOpenRPA.Orchestrator.__Orchestrator__.AgentOSFileBinaryDataBase64StrCreate(inHostNameStr, inUserStr, inFilePathStr, inFileDataBase64Str, inGSettings=None)
L+,W+: Создать бинарный файл, который будет расположен по адресу inFilePathStr на стороне Агента с содержимым, декодированным с формата base64: inFileDataBase64Str


* **Параметры**

    
    * **inHostNameStr** – Наименование хоста, на котором запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inUserStr** – Наименование пользователя, на графической сессии которого запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inFilePathStr** – Полный путь к сохраняемому файлу на стороне Агента.


    * **inFileDataBase64Str** – Строка в формате base64 для отправки в создаваемый файл на стороне Агента.


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



* **Результат**

    ГУИД (GUID) строка Активности (ActivityItem). Далее можно ожидать результат этой функции по ГУИД с помощью функции AgentActivityItemReturnGet



### pyOpenRPA.Orchestrator.__Orchestrator__.AgentOSFileBinaryDataBase64StrReceive(inHostNameStr, inUserStr, inFilePathStr, inGSettings=None)
L+,W+: Выполнить чтение бинарного файла и получить содержимое в формате base64 (строка)


* **Параметры**

    
    * **inHostNameStr** – Наименование хоста, на котором запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inUserStr** – Наименование пользователя, на графической сессии которого запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inFilePathStr** – Путь к бинарному файлу на чтение на стороне Агента


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



* **Результат**

    ГУИД (GUID) строка Активности (ActivityItem). Далее можно ожидать результат этой функции по ГУИД с помощью функции AgentActivityItemReturnGet



### pyOpenRPA.Orchestrator.__Orchestrator__.AgentOSFileBinaryDataBytesCreate(inHostNameStr, inUserStr, inFilePathStr, inFileDataBytes, inGSettings=None)
L+,W+: Создать бинарный файл, который будет расположен по адресу inFilePathStr на стороне Агента с содержимым inFileDataBytes


* **Параметры**

    
    * **inHostNameStr** – Наименование хоста, на котором запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inUserStr** – Наименование пользователя, на графической сессии которого запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inFilePathStr** – Полный путь к сохраняемому файлу на стороне Агента.


    * **inFileDataBytes** – Строка байт (b““) для отправки в создаваемый файл на стороне Агента.


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



* **Результат**

    ГУИД (GUID) строка Активности (ActivityItem). Далее можно ожидать результат этой функции по ГУИД с помощью функции AgentActivityItemReturnGet



### pyOpenRPA.Orchestrator.__Orchestrator__.AgentOSFileBinaryDataReceive(inHostNameStr, inUserStr, inFilePathStr)
L+,W+: Чтение бинарного файла на стороне Агента по адресу inFilePathStr.

!ВНИМАНИЕ - СИНХРОННАЯ! Функция не завершится, пока не будет получен результат чтения на стороне Агента.


* **Параметры**

    
    * **inHostNameStr** – Наименование хоста, на котором запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inUserStr** – Наименование пользователя, на графической сессии которого запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inFilePathStr** – Путь к бинарному файлу, который требуется прочитать на стороне Агента



* **Результат**

    Строка байт (b““) - содержимое бинарного файла



### pyOpenRPA.Orchestrator.__Orchestrator__.AgentOSFileSend(inHostNameStr, inUserStr, inOrchestratorFilePathStr, inAgentFilePathStr, inGSettings=None)
L+,W+: Отправить файл по адресу inOrchestratorFilePathStr со стороны Оркестратора и сохранить по адресу inAgentFilePathStr на стороне Агента.
Поддерживает передачу крупных файлов (более 2-х Гб.). Функция является синхронной - не закончит свое выполнение, пока файл не будет передан полностью.

!ВНИМАНИЕ - ПОТОКОБЕЗОПАСНАЯ! Вы можете вызвать эту функцию до инициализации ядра Оркестратора. Оркестратор добавит эту функцию в процессорную очередь на исполение. Если вам нужен результат функции, то необходимо сначала убедиться в том, что ядро Оркестратора было инициализированно (см. функцию OrchestratorInitWait).


* **Параметры**

    
    * **inHostNameStr** – Наименование хоста, на котором запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inUserStr** – Наименование пользователя, на графической сессии которого запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inOrchestratorFilePathStr** – Полный путь к передаваемому файлу на стороне Оркестратора.


    * **inAgentFilePathStr** – Полный путь к локации, в которую требуется сохранить передаваемый файл.


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



* **Результат**

    ГУИД (GUID) строка Активности (ActivityItem). Далее можно ожидать результат этой функции по ГУИД с помощью функции AgentActivityItemReturnGet



### pyOpenRPA.Orchestrator.__Orchestrator__.AgentOSFileTextDataStrCreate(inHostNameStr, inUserStr, inFilePathStr, inFileDataStr, inEncodingStr='utf-8', inGSettings=None)
L+,W+: Создать текстовый файл, который будет расположен по адресу inFilePathStr на стороне Агента с содержимым inFileDataStr в кодировке inEncodingStr


* **Параметры**

    
    * **inHostNameStr** – Наименование хоста, на котором запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inUserStr** – Наименование пользователя, на графической сессии которого запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inFilePathStr** – Полный путь к сохраняемому файлу на стороне Агента.


    * **inFileDataStr** – Строка для отправки в создаваемый файл на стороне Агента.


    * **inEncodingStr** – Кодировка текстового файла. По умолчанию utf-8


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



* **Результат**

    ГУИД (GUID) строка Активности (ActivityItem). Далее можно ожидать результат этой функции по ГУИД с помощью функции AgentActivityItemReturnGet



### pyOpenRPA.Orchestrator.__Orchestrator__.AgentOSFileTextDataStrReceive(inHostNameStr, inUserStr, inFilePathStr, inEncodingStr='utf-8', inGSettings=None)
L+,W+: Чтение текстового файла на стороне Агента по адресу inFilePathStr. По ГИУД с помощью функции AgentActivityItemReturnGet можно будет получить текстовую строку данных, которые были расположены в файле.

!ВНИМАНИЕ - АСИНХРОННАЯ! Функция завершится сразу, не дожидаясь окончания выполнения операции на стороне Агента.


* **Параметры**

    
    * **inHostNameStr** – Наименование хоста, на котором запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inUserStr** – Наименование пользователя, на графической сессии которого запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inFilePathStr** – Путь к бинарному файлу, который требуется прочитать на стороне Агента


    * **inEncodingStr** – Кодировка текстового файла. По умолчанию utf-8


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



* **Результат**

    ГУИД (GUID) строка Активности (ActivityItem). Далее можно ожидать результат этой функции по ГУИД с помощью функции AgentActivityItemReturnGet



### pyOpenRPA.Orchestrator.__Orchestrator__.AgentOSLogoff(inHostNameStr, inUserStr)
L+,W+: Выполнить операцию logoff на стороне пользователя.


* **Параметры**

    
    * **inHostNameStr** – Наименование хоста, на котором запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inUserStr** – Наименование пользователя, на графической сессии которого запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления



* **Результат**

    ГУИД (GUID) строка Активности (ActivityItem). Далее можно ожидать результат этой функции по ГУИД с помощью функции AgentActivityItemReturnGet



### pyOpenRPA.Orchestrator.__Orchestrator__.AgentProcessWOExeUpperUserListGet(inHostNameStr, inUserStr, inGSettings=None)
L-,W+: Получить список процессов, которые выполняется на сессии Агента. Все процессы фиксируются без постфикса .exe, а также в верхнем регистре.

ПРИМЕР РЕЗУЛЬТАТА, КОТОРЫЙ МОЖНО ПОЛУЧИТЬ ПО ГУИД ЧЕРЕЗ ФУНКЦИЮ AgentActivityItemReturnGet: [«ORCHESTRATOR», «AGENT», «CHROME», «EXPLORER», …]


* **Параметры**

    
    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)


    * **inHostNameStr** – Наименование хоста, на котором запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления


    * **inUserStr** – Наименование пользователя, на графической сессии которого запущен Агент. Наименования подключенных агентов доступно для просмотра в панели управления



* **Результат**

    ГУИД (GUID) строка Активности (ActivityItem). Далее можно ожидать результат этой функции по ГУИД с помощью функции AgentActivityItemReturnGet



### pyOpenRPA.Orchestrator.__Orchestrator__.GSettingsGet(inGSettings=None)
L+,W+: Вернуть глобальный словарь настроек Оркестратора. Во время выполнения программы глобальный словарь настроек существует в единственном экземпляре (синглтон)


* **Параметры**

    **inGSettings** – Дополнительный словарь настроек, который необходимо добавить в основной глобальный словарь настроек Оркестратора (синглтон)



* **Результат**

    Глобальный словарь настроек GSettings



### pyOpenRPA.Orchestrator.__Orchestrator__.GSettingsKeyListValueAppend(inValue, inKeyList=None, inGSettings=None)
L+,W+: Применить операцию .append к обьекту, расположенному по адресу списка ключей inKeyList в глобальном словаре настроек Оркестратора GSettings. Пример: Добавить значение в конец списка (list).

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

Orchestrator.GSettingsKeyListValueAppend(
    inGSettings = gSettings,
    inValue = "NewValue",
    inKeyList=["NewKeyDict","NewKeyList"]):
# result inGSettings: {
#    ... another keys in gSettings ...,
#    "NewKeyDict":{
#        "NewKeyList":[
#            "NewValue"
#        ]
#    }
#}
```


* **Параметры**

    
    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)


    * **inValue** – Значение для установки в глобальный словарь настроек Оркестратора GSettings


    * **inKeyList** – Список ключей, по адресу которого выполнить добавление в конец списка (list)



### pyOpenRPA.Orchestrator.__Orchestrator__.GSettingsKeyListValueGet(inKeyList=None, inGSettings=None)
L+,W+: Получить значение из глобального словаря настроек Оркестратора GSettings по списку ключей.


* **Параметры**

    
    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)


    * **inKeyList** – Список ключей, по адресу которого получить значение из GSettings



* **Результат**

    Значение (тип данных определяется форматом хранения в GSettings)



### pyOpenRPA.Orchestrator.__Orchestrator__.GSettingsKeyListValueOperatorPlus(inValue, inKeyList=None, inGSettings=None)
L+,W+: Применить оператор сложения (+) к обьекту, расположенному по адресу списка ключей inKeyList в глобальном словаре настроек Оркестратора GSettings. Пример: соединить 2 списка (list).

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

Orchestrator.GSettingsKeyListValueOperatorPlus(
    inGSettings = gSettings,
    inValue = [1,2,3],
    inKeyList=["NewKeyDict","NewKeyList"]):
# result inGSettings: {
#    ... another keys in gSettings ...,
#    "NewKeyDict":{
#        "NewKeyList":[
#            "NewValue",
#            1,
#            2,
#            3
#        ]
#    }
#}
```


* **Параметры**

    
    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)


    * **inValue** – Значение для установки в глобальный словарь настроек Оркестратора GSettings


    * **inKeyList** – Список ключей, по адресу которого выполнить добавление в конец списка (list)



### pyOpenRPA.Orchestrator.__Orchestrator__.GSettingsKeyListValueSet(inValue, inKeyList=None, inGSettings=None)
L+,W+: Установить значение из глобального словаря настроек Оркестратора GSettings по списку ключей.

Пример: Для того, чтобы установить значение для ключа car в словаре {«complex»:{«car»:»green»}, «simple»:»HELLO»}, необходимо передать список ключей: [«complex», «car»]


* **Параметры**

    
    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)


    * **inValue** – Значение для установки в глобальный словарь настроек Оркестратора GSettings


    * **inKeyList** – Список ключей, по адресу которого установить значение в GSettings



### pyOpenRPA.Orchestrator.__Orchestrator__.OSCMD(inCMDStr, inRunAsyncBool=True, inLogger=None)
L-,W+: Отправить команду на выполнение на сессию, где выполняется Оркестратор.


* **Параметры**

    
    * **inCMDStr** – Команда на отправку


    * **inRunAsyncBool** – True - выполнить команду в асинхронном режиме (не дожидаться окончания выполнения программы и не захватывать результат выполнения); False - Ждать окончания выполнения и захватывать результат


    * **inLogger** – Логгер, в который отправлять информацию о результате выполнения команды



* **Результат**

    Строка результата выполнения команды. Если inRunAsyncBool = False



### pyOpenRPA.Orchestrator.__Orchestrator__.OSCredentialsVerify(inUserStr, inPasswordStr, inDomainStr='')
L+,W+: Выполнить верификацию доменного (локального) пользователя по паре логин/пароль


* **Параметры**

    
    * **inUserStr** – Наименование пользователя


    * **inPasswordStr** – Пароль


    * **inDomainStr** – Домен. Если домена нет - не указывать или «»



* **Результат**

    True - Учетные данные верны; False - Учетные данные представлены некорректно



### pyOpenRPA.Orchestrator.__Orchestrator__.OSLogoff()
L+,W+: Выполнить отключение сессии, на которой выполняется Оркестратор.


* **Результат**

    


### pyOpenRPA.Orchestrator.__Orchestrator__.OSRemotePCRestart(inHostStr, inForceBool=True, inLogger=None)
L-,W+: Отправить сигнал на удаленную перезагрузку операционной системы.

!ВНИМАНИЕ! Перезапуск будет принят, если учетная запись имеет полномочия на перезапуск на соответсвующей машине.


* **Параметры**

    
    * **inHostStr** – Имя хоста, который требуется перезагрузить


    * **inForceBool** – True - принудительная перезагрузка; False - мягкая перезагрузка (дождаться окончания выполнения всех операций). По умолчанию True


    * **inLogger** – Логгер, в который отправлять информацию о результате выполнения команды



* **Результат**

    


### pyOpenRPA.Orchestrator.__Orchestrator__.OSRestart(inForceBool=True, inLogger=None)
L+,W+: Отправить сигнал на перезагрузку операционной системы.

!ВНИМАНИЕ! Перезапуск будет принят, если учетная запись имеет полномочия на перезапуск на соответсвующей машине.


* **Параметры**

    
    * **inForceBool** – True - принудительная перезагрузка; False - мягкая перезагрузка (дождаться окончания выполнения всех операций). По умолчанию True


    * **inLogger** – Логгер, в который отправлять информацию о результате выполнения команды



* **Результат**

    


### pyOpenRPA.Orchestrator.__Orchestrator__.Orchestrator(inGSettings=None, inDumpRestoreBool=True, inRunAsAdministratorBool=True)
L+,W+: Инициализация ядра Оркестратора (всех потоков)


* **Параметры**

    
    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)


    * **inDumpRestoreBool** – True - Восстановить информацию о RDP сессиях и StorageDict; False - не восстанавливать


    * **inRunAsAdministratorBool** – True - Проверить права администратратора и обеспечить их; False - Не обеспечивать права администратора



### pyOpenRPA.Orchestrator.__Orchestrator__.OrchestratorInitWait()
L+,W+: Ожидать инициализацию ядра Оркестратора

!ВНИМАНИЕ!: НИ В КОЕМ СЛУЧАЕ НЕ ЗАПУСКАТЬ ЭТУ ФУНКЦИЮ В ОСНОВНОМ ПОТОКЕ, ГДЕ ПРОИСХОДИТ ИНИЦИАЛИЗАЦИЯ ЯДРА ОРКЕСТРАТОРА - ВОЗНИКНЕТ ВЕЧНЫЙ ЦИКЛ


### pyOpenRPA.Orchestrator.__Orchestrator__.OrchestratorIsAdmin()
L+,W+: Проверить, запущен ли Оркестратор с правами администратора. Права администратора нужны Оркестратору только для контроля графической сессии, на которой он запущен. Если эти права выделить индивидуально, то права администратора будут необязательны.


* **Результат**

    True - Запущен с правами администратора; False - Не запущен с правами администратора



### pyOpenRPA.Orchestrator.__Orchestrator__.OrchestratorIsInited()
L+,W+: Проверить, было ли проинициализировано ядро Оркестратора


* **Результат**

    True - Ядро Оркестратора было проинициализировано; False - Требуется время на инициализацию



* **Тип результата**

    bool



### pyOpenRPA.Orchestrator.__Orchestrator__.OrchestratorLoggerGet()
L+,W+: Получить логгер Оркестратора


* **Результат**

    Логгер



### pyOpenRPA.Orchestrator.__Orchestrator__.OrchestratorPySearchInit(inGlobPatternStr, inDefStr=None, inDefArgNameGSettingsStr=None, inAsyncInitBool=False, inPackageLevelInt=0)
L+,W+: Выполнить поиск и инициализацию пользовательских .py файлов в Оркестраторе (например панелей управления роботов)

Добавляет инициализированный модуль в пространство sys.modules как imported (имя модуля = имя файла без расширения).

ВНИМАНИЕ! ПРЕОБРАЗУЕТ ПУТИ МЕЖДУ WINDOWS И LINUX НОТАЦИЯМИ

```
# ВАРИАНТ ИСПОЛЬЗОВАНИЯ 1 (инициализация модуля py без вызова каких-либо функций внутри)
# автоинициализация всех .py файлов, с префиксом CP_, которые расположены в папке ControlPanel
Orchestrator.OrchestratorPySearchInit(inGlobPatternStr="ControlPanel\CP_*.py")

# ВАРИАНТ ИСПОЛЬЗОВАНИЯ 2 (инициализация модуля py с вызовом функции внутри) - преимущественно для обратной совместимости старых версий панелей управления < 1.2.7
# автоинициализация всех .py файлов, с префиксом CP_, которые расположены в папке ControlPanel
Orchestrator.OrchestratorPySearchInit(inGlobPatternStr="ControlPanel\CP_*.py", inDefStr="SettingsUpdate", inDefArgNameGSettingsStr="inGSettings")

# ДЛЯ СПРАВКИ & ИСТОРИИ: Код выше позволил отказаться от блока кода ниже для каждого .py файла
## !!! For Relative import !!! CP Version Check
try:
    sys.path.insert(0,os.path.abspath(os.path.join(r"")))
    from ControlPanel import CP_VersionCheck
    CP_VersionCheck.SettingsUpdate(inGSettings=gSettings)
except Exception as e:
    gSettings["Logger"].exception(f"Exception when init CP. See below.")
```


* **Параметры**

    
    * **inGlobPatternStr** – Пример «..\*\*\*_CP\*.py»


    * **inDefStr** – ОПЦИОНАЛЬНО Строковое наименование функции. Преимущественно для обратной совместимости


    * **inDefArgNameGSettingsStr** – ОПЦИОНАЛЬНО Наименование аргумента, в который требуется передать GSettings (если необходимо)


    * **inAsyncInitBool** – ОПЦИОНАЛЬНО True - Инициализация py модулей в отдельных параллельных потоках - псевдопараллельное выполнение. False - последовательная инициализация


    * **inPackageLevelInt** – ОПЦИОНАЛЬНО Уровень вложенности модуля в пакет. По умолчанию 0 (не является модулем пакета)



* **Результат**

    Сведения об инициализированных модулях в структуре: { «ModuleNameStr»:{«PyPathStr»: «», «Module»: …},  …}



### pyOpenRPA.Orchestrator.__Orchestrator__.OrchestratorRerunAsAdmin()
L-,W+: Перезапустить Оркестратор с правами локального администратора. Права администратора нужны Оркестратору только для контроля графической сессии, на которой он запущен. Если эти права выделить индивидуально, то права администратора будут необязательны.


* **Результат**

    True - Запущен с правами администратора; False - Не запущен с правами администратора



### pyOpenRPA.Orchestrator.__Orchestrator__.OrchestratorRestart(inGSettings=None)
L+,W+: Перезапуск Оркестратора с сохранением информации о запущенных RDP сессиях.


* **Параметры**

    **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



### pyOpenRPA.Orchestrator.__Orchestrator__.OrchestratorScheduleGet()
L+,W+: Базовый объект расписания, который можно использовать для запуска / остановки роботов.
Подробнее про объект schedule и его примеры использования см. по адресу: schedule.readthedocs.io

```
# Однопоточный schedule
Orchestrator.OrchestratorScheduleGet().every(5).seconds.do(lProcess.StatusCheckStart)

#Многопоточный schedule. cм. описание Orchestrator.OrchestratorThreadStart
Orchestrator.OrchestratorScheduleGet().every(5).seconds.do(Orchestrator.OrchestratorThreadStart,lProcess.StatusCheckStart)
```


* **Результат**

    schedule объект



### pyOpenRPA.Orchestrator.__Orchestrator__.OrchestratorSessionRestore(inGSettings=None)
L+,W+: Восстановить состояние Оркестратора, если ранее состояние Оркестратора было сохранено с помощью функции OrchestratorSessionSave:


* RDP сессий, которые контролирует Оркестратор


* Хранилища DataStorage в глобальном словаре настроек GSettings. DataStorage поддерживает хранение объектов Python

(до версии 1.2.7)

    _SessionLast_GSettings.pickle (binary)

(начиная с версии 1.2.7)

    _SessionLast_RDPList.json (encoding = «utf-8»)
    _SessionLast_StorageDict.pickle (binary)


* **Параметры**

    **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



### pyOpenRPA.Orchestrator.__Orchestrator__.OrchestratorSessionSave(inGSettings=None)
L+,W+: Сохранить состояние Оркестратора (для дальнейшего восстановления в случае перезапуска):


* RDP сессий, которые контролирует Оркестратор


* Хранилища DataStorage в глобальном словаре настроек GSettings. DataStorage поддерживает хранение объектов Python

(до версии 1.2.7)

    _SessionLast_GSettings.pickle (binary)

(начиная с версии 1.2.7)

    _SessionLast_RDPList.json (encoding = «utf-8»)
    _SessionLast_StorageDict.pickle (binary)


* **Параметры**

    **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



### pyOpenRPA.Orchestrator.__Orchestrator__.OrchestratorThreadStart(inDef, \*inArgList, \*\*inArgDict)
L+,W+: Запустить функцию в отдельном потоке. В таком случае получить результат выполнения функции можно только через общие переменные. (Например через GSettings)


* **Параметры**

    
    * **inDef** – Python функция


    * **inArgList** – Список неименованных аргументов функции inDef


    * **inArgDict** – Словарь именованных аргументов функции inDef



* **Результат**

    threading.Thread экземпляр



### pyOpenRPA.Orchestrator.__Orchestrator__.ProcessDefIntervalCall(inDef, inIntervalSecFloat, inIntervalAsyncBool=False, inDefArgList=None, inDefArgDict=None, inDefArgGSettingsNameStr=None, inDefArgLoggerNameStr=None, inExecuteInNewThreadBool=True, inLogger=None, inGSettings=None)
L+,W+: Периодический вызов функции Python.


* **Параметры**

    
    * **inDef** – Функция Python, которую потребуется периодически вызывать


    * **inIntervalSecFloat** – Интервал между вызовами функции в сек.


    * **inIntervalAsyncBool** – False - ожидать интервал inIntervalSecFloat только после окончания выполнения предыдущей итерации; True - Ожидать интервал сразу после запуска итерации


    * **inDefArgList** – Список (list) неименованных аргументов для передачи в функцию. По умолчанию None


    * **inDefArgDict** – Словарь (dict) именованных аргументов для передачи в функцию. По умолчанию None


    * **inDefArgGSettingsNameStr** – Наименование аргумента глобального словаря настроек Оркестратора GSettings (опционально)


    * **inDefArgLoggerNameStr** – Наименование аргумента логгера Оркестратора (опционально)


    * **inExecuteInNewThreadBool** – True - периодический вызов выполнять в отдельном потоке (не останавливать выполнение текущего потока); False - Выполнение периодического вызова в текущем потоке, в котором была вызвана функция ProcessDefIntervalCall. По умолчанию: True


    * **inLogger** – Логгер для фиксации сообщений выполнения функции (опционально)


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



### pyOpenRPA.Orchestrator.__Orchestrator__.ProcessIsStarted(inProcessNameWOExeStr)
L-,W+: Проверить, запущен ли процесс, который в наименовании содержит inProcessNameWOExeStr.

!ВНИМАНИЕ! ПРОВЕРЯЕТ ВСЕ ПРОЦЕССЫ ОПЕРАЦИОННОЙ СИСТЕМЫ. И ДРУГИХ ПОЛЬЗОВАТЕЛЬСКИХ СЕССИЙ, ЕСЛИ ОНИ ЗАПУЩЕНЫ НА ЭТОЙ МАШИНЕ.

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

lProcessIsStartedBool = Orchestrator.ProcessIsStarted(inProcessNameWOExeStr = "notepad")
# lProcessIsStartedBool is True - notepad.exe is running on the Orchestrator machine
```


* **Параметры**

    **inProcessNameWOExeStr** – Наименование процесса без расширения .exe (WO = WithOut = Без) Пример: Для проверки процесса блокнота нужно указывать «notepad», не «notepad.exe»



* **Результат**

    True - Процесс запущен на ОС ; False - Процесс не запущен на ОС, где работает Оркестратор



### pyOpenRPA.Orchestrator.__Orchestrator__.ProcessListGet(inProcessNameWOExeList=None)
L-,W+: Вернуть список процессов, запущенных на ОС, где работает Оркестратор. В списке отсортировать процессы по количеству используемой оперативной памяти. Также можно указать перечень процессов, которые интересуют - функция будет показывать активные из них.

!ВНИМАНИЕ! ДЛЯ ПОЛУЧЕНИЯ СПИСКА ВСЕХ ПРОЦЕССОВ ОС НЕОБХОДИМО ЗАПУСКАТЬ ОРКЕСТРАТОР С ПРАВАМИ АДМИНИСТРАТОРА.

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

lProcessList = Orchestrator.ProcessListGet()
# Return the list of the process on the machine.
# !ATTENTION! RUn orchestrator as administrator to get all process list on the machine.
```


* **Параметры**

    **inProcessNameWOExeList** – Список процессов, среди которых искать активные. Если параметр не указывать - вернет перечень всех доступных процессов



* **Результат**

    Сведения о запущенных процессах в следующем формате {
    «ProcessWOExeList»: [«notepad»,»…»],
    «ProcessWOExeUpperList»: [«NOTEPAD»,»…»],
    «ProcessDetailList»: [

    > {

    >     „pid“: 412, # Идентификатор процесса
    >     „username“: «DESKTOPUSER»,
    >     „name“: „notepad.exe“,
    >     „vms“: 13.77767775, # В Мб
    >     „NameWOExeUpperStr“: „NOTEPAD“,
    >     „NameWOExeStr“: «„notepad“»},

    > {…}]




### pyOpenRPA.Orchestrator.__Orchestrator__.ProcessStart(inPathStr, inArgList, inStopProcessNameWOExeStr=None)
L-,W+: Запуск процесса на сессии Оркестратора, если на ОС не запущен процесс inStopProcessNameWOExeStr.

!ВНИМАНИЕ! ПРИ ПРОВЕРКЕ РАНЕЕ ЗАПУЩЕННЫХ ПРОЦЕССОВ ПРОВЕРЯЕТ ВСЕ ПРОЦЕССЫ ОПЕРАЦИОННОЙ СИСТЕМЫ. И ДРУГИХ ПОЛЬЗОВАТЕЛЬСКИХ СЕССИЙ, ЕСЛИ ОНИ ЗАПУЩЕНЫ НА ЭТОЙ МАШИНЕ.

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

Orchestrator.ProcessStart(
    inPathStr = "notepad"
    inArgList = []
    inStopProcessNameWOExeStr = "notepad")
# notepad.exe will be started if no notepad.exe is active on the machine
```


* **Параметры**

    
    * **inPathStr** – Путь к файлу запускаемого процесса


    * **inArgList** – Список аргументов, передаваемых в запускаемый процесс. Пример: [«test.txt»]


    * **inStopProcessNameWOExeStr** – Доп. контроль: Не запускать процесс, если обнаружен запущенный процесс под наименованием inStopProcessNameWOExeStr без расширения .exe (WO = WithOut = Без)



### pyOpenRPA.Orchestrator.__Orchestrator__.ProcessStop(inProcessNameWOExeStr, inCloseForceBool, inUserNameStr='%username%')
L-,W+: Остановить процесс на ОС, где работает Оркестратор, под учетной записью inUserNameStr.

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

Orchestrator.ProcessStop(
    inProcessNameWOExeStr = "notepad"
    inCloseForceBool = True
    inUserNameStr = "USER_99")
# Will close process "notepad.exe" on the user session "USER_99" (!ATTENTION! if process not exists no exceptions will be raised)
```


* **Параметры**

    
    * **inProcessNameWOExeStr** – Наименование процесса без расширения .exe (WO = WithOut = Без). Пример: Для проверки процесса блокнота нужно указывать «notepad», не «notepad.exe»


    * **inCloseForceBool** – True - принудительно завершить процесс. False - отправить сигнал на безопасное отключение (!ВНИМАНИЕ! - ОС не позволяет отправлять сигнал на безопасное отключение на другую сесиию - только на той, где работает Оркестратор)


    * **inUserNameStr** – Наименование пользователя, под именем которого искать процесс для остановки. По умолчанию «%username%» - искать процесс на текущей сессии



### pyOpenRPA.Orchestrator.__Orchestrator__.ProcessorActivityItemAppend(inGSettings=None, inDef=None, inArgList=None, inArgDict=None, inArgGSettingsStr=None, inArgLoggerStr=None, inActivityItemDict=None)
L+,W+: Добавить активность (ActivityItem) в процессорную очередь.

```
# USAGE
from pyOpenRPA import Orchestrator

# EXAMPLE 1
def TestDef(inArg1Str, inGSettings, inLogger):
    pass
lActivityItem = Orchestrator.ProcessorActivityItemAppend(
    inGSettings = gSettingsDict,
    inDef = TestDef,
    inArgList=[],
    inArgDict={"inArg1Str": "ArgValueStr"},
    inArgGSettingsStr = "inGSettings",
    inArgLoggerStr = "inLogger")
# Activity have been already append in the processor queue

# EXAMPLE 2
def TestDef(inArg1Str):
    pass
Orchestrator.ProcessorAliasDefUpdate(
    inGSettings = gSettings,
    inDef = TestDef,
    inAliasStr="TestDefAlias")
lActivityItem = Orchestrator.ProcessorActivityItemCreate(
    inDef = "TestDefAlias",
    inArgList=[],
    inArgDict={"inArg1Str": "ArgValueStr"},
    inArgGSettingsStr = None,
    inArgLoggerStr = None)
Orchestrator.ProcessorActivityItemAppend(
    inGSettings = gSettingsDict,
    inActivityItemDict = lActivityItem)
# Activity have been already append in the processor queue
```


* **Параметры**

    
    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)


    * **inDef** – Функция Python или синоним (текстовый ключ)


    * **inArgList** – Список (list) неименованных аргументов к функции


    * **inArgDict** – Словарь (dict) именованных аргументов к функции


    * **inArgGSettingsStr** – Текстовое наименование аргумента GSettings (если требуется передавать)


    * **inArgLoggerStr** – Текстовое наименование аргумента logger (если требуется передавать)


    * **inGUIDStr** – ГУИД идентификатор активности (ActivityItem). Если ГУИД не указан, то он будет сгенерирован автоматически


    * **inThreadBool** – True - выполнить ActivityItem в новом потоке; False - выполнить последовательно в общем потоке процессорной очереди


    * **inActivityItemDict** – Альтернативный вариант заполнения, если уже имеется Активность (ActivityItem). В таком случае не требуется заполнять аргументы inDef, inArgList, inArgDict, inArgGSettingsStr, inArgLoggerStr


:return ГУИД активности (ActivityItem)


### pyOpenRPA.Orchestrator.__Orchestrator__.ProcessorActivityItemCreate(inDef, inArgList=None, inArgDict=None, inArgGSettingsStr=None, inArgLoggerStr=None, inGUIDStr=None, inThreadBool=False)
L+,W+: Создать Активность (ActivityItem). Активность можно использовать в ProcessorActivityItemAppend или в Processor.ActivityListExecute или в функциях работы с Агентами.

Старая версия. Новую версию см. в ActivityItemCreate

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

# ВАРИАНТ 1
def TestDef(inArg1Str, inGSettings, inLogger):
    pass
lActivityItem = Orchestrator.ProcessorActivityItemCreate(
    inDef = TestDef,
    inArgList=[],
    inArgDict={"inArg1Str": "ArgValueStr"},
    inArgGSettingsStr = "inGSettings",
    inArgLoggerStr = "inLogger")
# lActivityItem:
#   {
#       "Def":TestDef,
#       "ArgList":inArgList,
#       "ArgDict":inArgDict,
#       "ArgGSettings": "inArgGSettings",
#       "ArgLogger": "inLogger"
#   }

# ВАРИАНТ 2
def TestDef(inArg1Str):
    pass
Orchestrator.ProcessorAliasDefUpdate(
    inGSettings = gSettings,
    inDef = TestDef,
    inAliasStr="TestDefAlias")
lActivityItem = Orchestrator.ProcessorActivityItemCreate(
    inDef = "TestDefAlias",
    inArgList=[],
    inArgDict={"inArg1Str": "ArgValueStr"},
    inArgGSettingsStr = None,
    inArgLoggerStr = None)
# lActivityItem:
#   {
#       "Def":"TestDefAlias",
#       "ArgList":inArgList,
#       "ArgDict":inArgDict,
#       "ArgGSettings": None,
#       "ArgLogger": None
#   }
```


* **Параметры**

    
    * **inDef** – Функция Python или синоним (текстовый ключ)


    * **inArgList** – Список (list) неименованных аргументов к функции


    * **inArgDict** – Словарь (dict) именованных аргументов к функции


    * **inArgGSettingsStr** – Текстовое наименование аргумента GSettings (если требуется передавать)


    * **inArgLoggerStr** – Текстовое наименование аргумента logger (если требуется передавать)


    * **inGUIDStr** – ГУИД идентификатор активности (ActivityItem). Если ГУИД не указан, то он будет сгенерирован автоматически


    * **inThreadBool** – True - выполнить ActivityItem в новом потоке; False - выполнить последовательно в общем потоке процессорной очереди



* **Результат**

    lActivityItemDict= {

        «Def»:inDef, # def link or def alias (look gSettings[«Processor»][«AliasDefDict»])
        «ArgList»:inArgList, # Args list
        «ArgDict»:inArgDict, # Args dictionary
        «ArgGSettings»: inArgGSettingsStr, # Name of GSettings attribute: str (ArgDict) or index (for ArgList)
        «ArgLogger»: inArgLoggerStr, # Name of GSettings attribute: str (ArgDict) or index (for ArgList)
        «GUIDStr»: inGUIDStr,
        «ThreadBool»: inThreadBool

    }




### pyOpenRPA.Orchestrator.__Orchestrator__.ProcessorAliasDefCreate(inDef, inAliasStr=None, inGSettings=None)
L+,W+: Создать синоним (текстовый ключ) для инициации выполнения функции в том случае, если запрос на выполнения пришел из вне (передача функций невозможна).

Старая версия. Новую версию см. ActivityItemDefAliasCreate

```
# USAGE
from pyOpenRPA import Orchestrator

def TestDef():
    pass
lAliasStr = Orchestrator.ProcessorAliasDefCreate(
    inGSettings = gSettings,
    inDef = TestDef,
    inAliasStr="TestDefAlias")
# Now you can call TestDef by the alias from var lAliasStr with help of ActivityItem (key Def = lAliasStr)
```


* **Параметры**

    
    * **inDef** – функция Python


    * **inAliasStr** – Строковый ключ (синоним), который можно будет использовать в Активности (ActivityItem)


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



* **Результат**

    Строковый ключ, который был назначен. Ключ может быть изменен, если входящий текстовый ключ был уже занят.



### pyOpenRPA.Orchestrator.__Orchestrator__.ProcessorAliasDefUpdate(inDef, inAliasStr, inGSettings=None)
L+,W+: Обновить синоним (текстовый ключ) для инициации выполнения функции в том случае, если запрос на выполнения пришел из вне (передача функций невозможна).

Старая версия. Новую версию см. ActivityItemDefAliasUpdate

```
# USAGE
from pyOpenRPA import Orchestrator

def TestDef():
    pass
Orchestrator.ProcessorAliasDefUpdate(
    inGSettings = gSettings,
    inDef = TestDef,
    inAliasStr="TestDefAlias")
# Now you can call TestDef by the alias "TestDefAlias" with help of ActivityItem (key Def = "TestDefAlias")
```


* **Параметры**

    
    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)


    * **inDef** – функция Python


    * **inAliasStr** – Строковый ключ (синоним), который можно будет использовать в Активности (ActivityItem)



* **Результат**

    Строковый ключ, который был назначен. Ключ будет тем же, если входящий текстовый ключ был уже занят.



### pyOpenRPA.Orchestrator.__Orchestrator__.PythonStart(inModulePathStr, inDefNameStr, inArgList=None, inArgDict=None, inLogger=None)
L+,W+: Импорт модуля и выполнение функции в процессе Оркестратора.

**NOTE**: Импорт модуля inModulePathStr будет происходить каждый раз в вызовом функции PythonStart.

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

Orchestrator.PythonStart(
    inModulePathStr="ModuleToCall.py", # inModulePathStr: Working Directory\ModuleToCall.py
    inDefNameStr="TestDef")
# Import module in Orchestrator process and call def "TestDef" from module "ModuleToCall.py"
```


* **Параметры**

    
    * **inModulePathStr** – Абсолютный или относительный путь (относительно рабочей директории процесса Оркестратора), по которому расположен импортируемый .py файл


    * **inDefNameStr** – Наименование функции в модуле .py


    * **inArgList** – Список (list) неименованных аргументов для передачи в функцию. По умолчанию None


    * **inArgDict** – Словарь (dict) именованных аргументов для передачи в функцию. По умолчанию None


    * **inLogger** – Логгер для фиксации сообщений выполнения функции (опционально)



### pyOpenRPA.Orchestrator.__Orchestrator__.RDPSessionCMDRun(inRDPSessionKeyStr, inCMDStr, inModeStr='CROSSCHECK', inGSettings=None)
L-,W+: Отправить CMD команду на удаленную сесиию через RDP окно (без Агента).

!ВНИМАНИЕ! Данная функция может работать нестабильно из-за использования графических элементов UI при работе с RDP. Мы рекомендуем использовать конструкцию взаимодействия Оркестратора с Агентом.

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

lResultDict = Orchestrator.RDPSessionCMDRun(
    inGSettings = gSettings,
    inRDPSessionKeyStr = "RDPKey",
    inModeStr = 'LISTEN')
# Orchestrator will send CMD to RDP and return the result (see return section)
```


* **Параметры**

    
    * **inRDPSessionKeyStr** – Ключ RDP сессии - необходим для дальнейшей идентификации


    * **inCMDStr** – Команда CMD, которую отправить на удаленную сессию


    * **inModeStr** – По умолчанию «CROSSCHECK». Варианты:
    «LISTEN» - Получить результат выполнения операции. Внимание! необходим общий буфер обмена с сессией Оркестратора!
    «CROSSCHECK» - Выполнить проверку, что операция была выполнена (без получения результата отработки CMD команды). Внимание! необходим общий буфер обмена с сессией Оркестратора!
    «RUN» - Не получать результат и не выполнять проверку


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



* **Результат**

    Результат выполнения операции в соответсвии с параметрами инициализации функции. Выходная структура:

        {

            «OutStr»: <> # Результат выполнения CMD (если inModeStr = «LISTEN»)
            «IsResponsibleBool»: True|False # True - RDP приняло команду; False - обратная связь не была получена (если inModeStr = «CROSSCHECK»)

    }




### pyOpenRPA.Orchestrator.__Orchestrator__.RDPSessionConnect(inRDPSessionKeyStr, inRDPTemplateDict=None, inHostStr=None, inPortStr=None, inLoginStr=None, inPasswordStr=None, inGSettings=None, inRedirectClipboardBool=True)
L-,W+: Выполнить подключение к RDP и следить за стабильностью соединения со стороны Оркестратора.
!ВНИМАНИЕ! - Подключение будет проигнорировано, если соединение по таком RDP ключу уже было инициализировано ранее.

2 способа использования функции:
ВАРИАНТ 1 (ОСНОВНОЙ): inGSettings, inRDPSessionKeyStr, inRDPTemplateDict. Для получения inRDPTemplateDict см. функцию Orchestrator.RDPTemplateCreate
ВАРИАНТ 2 (ОБРАТНАЯ СОВМЕСТИМОСТЬ ДО ВЕРСИИ 1.1.20): inGSettings, inRDPSessionKeyStr, inHostStr, inPortStr, inLoginStr, inPasswordStr

```
# ПРИМЕР (ВАРИАНТ 1)
from pyOpenRPA import Orchestrator

lRDPItemDict = Orchestrator.RDPTemplateCreate(
    inLoginStr = "USER_99",
    inPasswordStr = "USER_PASS_HERE", inHostStr="127.0.0.1", inPortInt = 3389, inWidthPXInt = 1680,
    inHeightPXInt = 1050, inUseBothMonitorBool = False, inDepthBitInt = 32, inSharedDriveList=None)
Orchestrator.RDPSessionConnect(
    inGSettings = gSettings,
    inRDPSessionKeyStr = "RDPKey",
    inRDPTemplateDict = lRDPItemDict)
# Orchestrator will create RDP session by the lRDPItemDict configuration
```


* **Параметры**

    
    * **inRDPSessionKeyStr** – Ключ RDP сессии - необходим для дальнейшей идентификации


    * **inRDPTemplateDict** – Конфигурационный словарь (dict) RDP подключения (см. функцию Orchestrator.RDPTemplateCreate)


    * **inLoginStr** – Логин учетной записи, на которую будет происходить вход по RDP. Обратная совместимость до версии v 1.1.20. Мы рекомендуем использовать inRDPTemplateDict (см. функцию Orchestrator.RDPTemplateCreate)


    * **inPasswordStr** – Пароль учетной записи, на которую будет происходить вход по RDP. !ВНИМАНИЕ! Пароль нигде не сохраняется - конфиденциальность обеспечена. Обратная совместимость до версии v 1.1.20. Мы рекомендуем использовать inRDPTemplateDict (см. функцию Orchestrator.RDPTemplateCreate)


    * **inHostStr** – Имя хоста, к которому планируется подключение по RDP. Пример «77.77.22.22». Обратная совместимость до версии v 1.1.20. Мы рекомендуем использовать inRDPTemplateDict (см. функцию Orchestrator.RDPTemplateCreate)


    * **inPortInt** – RDP порт, по которому будет происходить подключение. По умолчанию 3389 (стандартный порт RDP). Обратная совместимость до версии v 1.1.20. Мы рекомендуем использовать inRDPTemplateDict (см. функцию Orchestrator.RDPTemplateCreate)


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)


    * **inRedirectClipboardBool** – True - Синхронизировать буфер обмена между сессией Оркестратора и удаленной RDP сессией; False - Не синхронизировать буфер обмена. По умолчанию True (в целях обратной совместимости). !ВНИМАНИЕ! Для учетных записей роботов мы рекомендуем не синхронизировать буфер обмена, так как это может привести к ошибкам роботов, которые работают с клавиатурой и буфером обмена



### pyOpenRPA.Orchestrator.__Orchestrator__.RDPSessionDisconnect(inRDPSessionKeyStr, inBreakTriggerProcessWOExeList=None, inGSettings=None)
L-,W+: Выполнить отключение RDP сессии и прекратить мониторить его активность.

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

Orchestrator.RDPSessionDisconnect(
    inGSettings = gSettings,
    inRDPSessionKeyStr = "RDPKey")
# Orchestrator will disconnect RDP session and will stop to monitoring current RDP
```


* **Параметры**

    
    * **inRDPSessionKeyStr** – Ключ RDP сессии - необходим для дальнейшей идентификации


    * **inBreakTriggerProcessWOExeList** – Список процессов, наличие которых в диспетчере задач приведет к прерыванию задачи по остановке RDP соединения. Пример [«notepad»]


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



### pyOpenRPA.Orchestrator.__Orchestrator__.RDPSessionFileStoredRecieve(inRDPSessionKeyStr, inRDPFilePathStr, inHostFilePathStr, inGSettings=None)
L-,W+: Получение файла со стороны RDP сессии на сторону Оркестратора через UI инструменты RDP окна (без Агента).

!ВНИМАНИЕ! Данная функция может работать нестабильно из-за использования графических элементов UI при работе с RDP. Мы рекомендуем использовать конструкцию взаимодействия Оркестратора с Агентом.

!ВНИМАНИЕ! Для работы функции требуется открыть доступ по RDP к тем дискам, с которых будет производится копирование файла.

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

lResultDict = Orchestrator.RDPSessionFileStoredRecieve(
    inGSettings = gSettings,
    inRDPSessionKeyStr = "RDPKey",
    inHostFilePathStr = "TESTDIR\Test.py",
    inRDPFilePathStr = "C:\RPA\TESTDIR\Test.py")
# Orchestrator will send CMD to RDP and return the result (see return section)
```


* **Параметры**

    
    * **inRDPSessionKeyStr** – Ключ RDP сессии - необходим для дальнейшей идентификации


    * **inRDPFilePathStr** – Откуда скопировать файл. !Абсолютный! путь на стороне RDP сессии. Пример: «C:RPATESTDIRTest.py»


    * **inHostFilePathStr** – Куда скопировать файл. Относительный или абсолютный путь к файлу на стороне Оркестратора. Пример: «TESTDIRTest.py»


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



### pyOpenRPA.Orchestrator.__Orchestrator__.RDPSessionFileStoredSend(inRDPSessionKeyStr, inHostFilePathStr, inRDPFilePathStr, inGSettings=None)
L-,W+: Отправка файла со стороны Оркестратора на сторону RDP сессии через UI инструменты RDP окна (без Агента).

!ВНИМАНИЕ! Данная функция может работать нестабильно из-за использования графических элементов UI при работе с RDP. Мы рекомендуем использовать конструкцию взаимодействия Оркестратора с Агентом.

!ВНИМАНИЕ! Для работы функции требуется открыть доступ по RDP к тем дискам, с которых будет производится копирование файла.

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

lResultDict = Orchestrator.RDPSessionFileStoredSend(
    inGSettings = gSettings,
    inRDPSessionKeyStr = "RDPKey",
    inHostFilePathStr = "TESTDIR\Test.py",
    inRDPFilePathStr = "C:\RPA\TESTDIR\Test.py")
# Orchestrator will send CMD to RDP and return the result (see return section)
```


* **Параметры**

    
    * **inRDPSessionKeyStr** – Ключ RDP сессии - необходим для дальнейшей идентификации


    * **inHostFilePathStr** – Откуда взять файл. Относительный или абсолютный путь к файлу на стороне Оркестратора. Пример: «TESTDIRTest.py»


    * **inRDPFilePathStr** – Куда скопировать файл. !Абсолютный! путь на стороне RDP сессии. Пример: «C:RPATESTDIRTest.py»


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



### pyOpenRPA.Orchestrator.__Orchestrator__.RDPSessionLogoff(inRDPSessionKeyStr, inBreakTriggerProcessWOExeList=None, inGSettings=None)
L-,W+: Выполнить отключение (logoff) на RDP сессии и прекратить мониторить активность со стороны Оркестратора.

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

Orchestrator.RDPSessionLogoff(
    inGSettings = gSettings,
    inRDPSessionKeyStr = "RDPKey",
    inBreakTriggerProcessWOExeList = ['Notepad'])
# Orchestrator will logoff the RDP session
```


* **Параметры**

    
    * **inRDPSessionKeyStr** – Ключ RDP сессии - необходим для дальнейшей идентификации


    * **inBreakTriggerProcessWOExeList** – Список процессов, наличие которых в диспетчере задач приведет к прерыванию задачи по остановке RDP соединения. Пример [«notepad»]


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



* **Результат**

    True - Отключение прошло успешно; False - были зафиксированы ошибки при отключении.



### pyOpenRPA.Orchestrator.__Orchestrator__.RDPSessionMonitorStop(inRDPSessionKeyStr, inGSettings=None)
L-,W+: Прекратить мониторить активность RDP соединения со стороны Оркестратора. Данная функция не уничтожает активное RDP соединение.

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

Orchestrator.RDPSessionMonitorStop(
    inGSettings = gSettings,
    inRDPSessionKeyStr = "RDPKey")
# Orchestrator will stop the RDP monitoring
```


* **Параметры**

    
    * **inRDPSessionKeyStr** – Ключ RDP сессии - необходим для дальнейшей идентификации


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



### pyOpenRPA.Orchestrator.__Orchestrator__.RDPSessionProcessStartIfNotRunning(inRDPSessionKeyStr, inProcessNameWEXEStr, inFilePathStr, inFlagGetAbsPathBool=True, inGSettings=None)
L-,W+: Выполнить запуск процесса на RDP сессии через графические UI инструменты (без Агента).

!ВНИМАНИЕ! Данная функция может работать нестабильно из-за использования графических элементов UI при работе с RDP. Мы рекомендуем использовать конструкцию взаимодействия Оркестратора с Агентом.

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

Orchestrator.RDPSessionProcessStartIfNotRunning(
    inGSettings = gSettings,
    inRDPSessionKeyStr = "RDPKey",
    inProcessNameWEXEStr = 'Notepad.exe',
    inFilePathStr = "path\to    he\executable\file.exe"
    inFlagGetAbsPathBool = True)
# Orchestrator will start the process in RDP session
```


* **Параметры**

    
    * **inRDPSessionKeyStr** – Ключ RDP сессии - необходим для дальнейшей идентификации


    * **inProcessNameWEXEStr** – Наименование процесса с расширением .exe (WEXE - WITH EXE - С EXE). Параметр позволяет не допустить повторного запуска процесса, если он уже был запущен. Example: «Notepad.exe»


    * **inFilePathStr** – Путь к файлу запуска процесса на стороне RDP сессии


    * **inFlagGetAbsPathBool** – True - Преобразовать относительный путь inFilePathStr в абсолютный с учетом рабочей директории Оркестратора; False - Не выполнять преобразований


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



### pyOpenRPA.Orchestrator.__Orchestrator__.RDPSessionProcessStop(inRDPSessionKeyStr, inProcessNameWEXEStr, inFlagForceCloseBool, inGSettings=None)
L-,W+: Отправка CMD команды в RDP окне на остановку процесса (без Агента).

!ВНИМАНИЕ! Данная функция может работать нестабильно из-за использования графических элементов UI при работе с RDP. Мы рекомендуем использовать конструкцию взаимодействия Оркестратора с Агентом.

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

lResultDict = Orchestrator.RDPSessionProcessStop(
    inGSettings = gSettings,
    inRDPSessionKeyStr = "RDPKey",
    inProcessNameWEXEStr = 'notepad.exe',
    inFlagForceCloseBool = True)
# Orchestrator will send CMD to RDP and return the result (see return section)
```


* **Параметры**

    
    * **inRDPSessionKeyStr** – Ключ RDP сессии - необходим для дальнейшей идентификации


    * **inProcessNameWEXEStr** – Наименование процесса, который требуется выключить с расширением .exe (WEXE - WITH EXE - С EXE). Пример: «Notepad.exe»


    * **inFlagForceCloseBool** – True - Принудительное отключение. False - Отправка сигнала на безопасное отключение (если процесс поддерживает)


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



### pyOpenRPA.Orchestrator.__Orchestrator__.RDPSessionReconnect(inRDPSessionKeyStr, inRDPTemplateDict=None, inGSettings=None)
L-,W+: Выполнить переподключение RDP сессии и продолжить мониторить его активность.

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

lRDPItemDict = Orchestrator.RDPTemplateCreate(
    inLoginStr = "USER_99",
    inPasswordStr = "USER_PASS_HERE", inHostStr="127.0.0.1", inPortInt = 3389, inWidthPXInt = 1680,
    inHeightPXInt = 1050, inUseBothMonitorBool = False, inDepthBitInt = 32, inSharedDriveList=None)
Orchestrator.RDPSessionReconnect(
    inGSettings = gSettings,
    inRDPSessionKeyStr = "RDPKey",
    inRDPTemplateDict = inRDPTemplateDict)
# Orchestrator will reconnect RDP session and will continue to monitoring current RDP
```


* **Параметры**

    
    * **inRDPSessionKeyStr** – Ключ RDP сессии - необходим для дальнейшей идентификации


    * **inRDPTemplateDict** – Конфигурационный словарь (dict) RDP подключения (см. функцию Orchestrator.RDPTemplateCreate)


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



### pyOpenRPA.Orchestrator.__Orchestrator__.RDPTemplateCreate(inLoginStr, inPasswordStr, inHostStr='127.0.0.1', inPortInt=3389, inWidthPXInt=1680, inHeightPXInt=1050, inUseBothMonitorBool=False, inDepthBitInt=32, inSharedDriveList=None, inRedirectClipboardBool=True)
L-,W+: Создать шаблон подключения RDP (dict). Данный шаблон далее можно использовать в Orchestrator.RDPSessionConnect

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

lRDPItemDict = Orchestrator.RDPTemplateCreate(
    inLoginStr = "USER_99",
    inPasswordStr = "USER_PASS_HERE",
    inHostStr="127.0.0.1",
    inPortInt = 3389,
    inWidthPXInt = 1680,
    inHeightPXInt = 1050,
    inUseBothMonitorBool = False,
    inDepthBitInt = 32,
    inSharedDriveList=None,
    inRedirectClipboardBool=False)
```


* **Параметры**

    
    * **inLoginStr** – Логин учетной записи, на которую будет происходить вход по RDP


    * **inPasswordStr** – Пароль учетной записи, на которую будет происходить вход по RDP. !ВНИМАНИЕ! Пароль нигде не сохраняется - конфиденциальность обеспечена


    * **inHostStr** – Имя хоста, к которому планируется подключение по RDP. Пример «77.77.22.22»


    * **inPortInt** – RDP порт, по которому будет происходить подключение. По умолчанию 3389 (стандартный порт RDP)


    * **inWidthPXInt** – Разрешение ширины RDP графической сессии в пикселях. По умолчанию 1680


    * **inHeightPXInt** – Разрешение высоты RDP графической сессии в пикселях. По умолчанию 1050


    * **inUseBothMonitorBool** – True - Использовать все подключенные мониторы на RDP сессии; False - Использовать только один монитор на подключенной RDP сессии


    * **inDepthBitInt** – Глубина цвета на удаленной RDP графической сессии. Допустимые варианты: 32 || 24 || 16 || 15. По умолчанию 32


    * **inSharedDriveList** – Перечень общих дисков, доступ к которым предоставить на сторону удаленной RDP сессии. Пример: [«c», «d»]


    * **inRedirectClipboardBool** – True - Синхронизировать буфер обмена между сессией Оркестратора и удаленной RDP сессией; False - Не синхронизировать буфер обмена. По умолчанию True (в целях обратной совместимости). !ВНИМАНИЕ! Для учетных записей роботов мы рекомендуем не синхронизировать буфер обмена, так как это может привести к ошибкам роботов, которые работают с клавиатурой и буфером обмена



* **Результат**

    {

        «Host»: inHostStr,  # Адрес хоста, пример «77.77.22.22»
        «Port»: str(inPortInt),  # RDP порт, пример «3389»
        «Login»: inLoginStr,  # Логин УЗ, пример «test»
        «Password»: inPasswordStr,  # Пароль УЗ, пример «test»
        «Screen»: {

        > »Width»: inWidthPXInt,  # Разрешение ширины RDP графической сессии в пикселях. По умолчанию 1680
        > «Height»: inHeightPXInt,  Разрешение высоты RDP графической сессии в пикселях. По умолчанию 1050
        > «FlagUseAllMonitors»: inUseBothMonitorBool,  
        > «DepthBit»: str(inDepthBitInt)

        },
        «SharedDriveList»: inSharedDriveList,  
        «RedirectClipboardBool»: True, 
        ###### PROGRAM VARIABLE ############
        «SessionHex»: «77777sdfsdf77777dsfdfsf77777777»,  
        «SessionIsWindowExistBool»: False,
        «SessionIsWindowResponsibleBool»: False,
        «SessionIsIgnoredBool»: False

    }




### pyOpenRPA.Orchestrator.__Orchestrator__.SchedulerActivityTimeAddWeekly(inTimeHHMMStr='23:55:', inWeekdayList=None, inActivityList=None, inGSettings=None)
L+,W+: Добавить активность по расписанию. Допускается указание времени и дней недели, в которые производить запуск.

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

# ВАРИАНТ 1
def TestDef(inArg1Str):
    pass
lActivityItem = Orchestrator.ProcessorActivityItemCreate(
    inDef = TestDef,
    inArgList=[],
    inArgDict={"inArg1Str": "ArgValueStr"},
    inArgGSettingsStr = None,
    inArgLoggerStr = None)
Orchestrator.SchedulerActivityTimeAddWeekly(
    inGSettings = gSettingsDict,
    inTimeHHMMStr = "04:34",
    inWeekdayList=[2,3,4],
    inActivityList = [lActivityItem])
# Activity will be executed at 04:34 Wednesday (2), thursday (3), friday (4)
```


* **Параметры**

    
    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)


    * **inTimeHHMMStr** – Время запуска активности. Допускаются значения от «00:00» до «23:59». Example: «05:29»


    * **inWeekdayList** – Список (list) дней недели, в которые выполнять запуск список активностей (ActivityItem list). 0 (понедельник) - 6 (воскресенье). Пример: [0,1,2,3,4]. По умолчанию устанавливается каждый день [0,1,2,3,4,5,6]


    * **inActivityList** – Список активностей (ActivityItem list) на исполнение



### pyOpenRPA.Orchestrator.__Orchestrator__.StorageRobotExists(inRobotNameStr)
L+,W+: Проверить, существует ли ключ inRobotNameStr в хранилище пользовательской информации StorageDict (GSettings > StarageDict)


* **Параметры**

    **inRobotNameStr** – Наименование (ключ) робота. !ВНИМАНИЕ! Наименование чувствительно к регистру



* **Результат**

    True - ключ робота присутствует в хранилище; False - отсутствует



### pyOpenRPA.Orchestrator.__Orchestrator__.StorageRobotGet(inRobotNameStr)
L+,W+: Получить содержимое по ключу робота inRobotNameStr в хранилище пользовательской информации StorageDict (GSettings > StarageDict)


* **Параметры**

    **inRobotNameStr** – Наименование (ключ) робота. !ВНИМАНИЕ! Наименование чувствительно к регистру



* **Результат**

    Значение или структура, которая расположена по адресу (GSettings > StarageDict > inRobotNameStr)



### pyOpenRPA.Orchestrator.__Orchestrator__.UACKeyListCheck(inRequest, inRoleKeyList)
L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.2, см. WebUserUACCheck] Проверить права доступа для пользователя запроса по списку ключей до права.

```
# ВАРИАНТ ИСПОЛЬЗОВАНИЯ 1 (инициализация модуля py без вызова каких-либо функций внутри)
# автоинициализация всех .py файлов, с префиксом CP_, которые расположены в папке ControlPanel
Orchestrator.UACKeyListCheck(inRequest=lRequest, inRoleKeyList=["ROBOT1","DISPLAY_DASHBOARD"])
```


* **Параметры**

    
    * **inRequest** – Экземпляр request (from http.server import BaseHTTPRequestHandler)


    * **inRoleKeyList** – Список ключей, права доступа к которому требуется проверить



* **Результат**

    True - Пользователь обладает соответствующим правом; False - Пользователь не обладает соответствующим правом



### pyOpenRPA.Orchestrator.__Orchestrator__.UACSuperTokenUpdate(inSuperTokenStr, inGSettings=None)
L+,W+: Добавить супертокен (полный доступ). Супертокены используются для подключения к Оркестратору по API


* **Параметры**

    
    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)


    * **inSuperTokenStr** – Кодовая строковая комбинация, которая будет предоставлять доступ роботу / агенту для взаимодействия с Оркестратором по API



### pyOpenRPA.Orchestrator.__Orchestrator__.UACUpdate(inADLoginStr, inADStr='', inADIsDefaultBool=True, inURLList=None, inRoleHierarchyAllowedDict=None, inGSettings=None)
L+,W+: Дообогащение словаря доступа UAC пользователя inADStrinADLoginStr. Если ранее словарь не устанавливался, то по умолчанию он {}. Далее идет дообогащение теми ключами, которые присутствуют в inRoleHierarchyAllowedDict


* **Параметры**

    
    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)


    * **inADLoginStr** – Логин пользователя


    * **inADStr** – Домен пользователя. Если пусто - локальный компьютер или домен по-умолчанию, который настроен в ОС


    * **inADIsDefaultBool** – True - домен настроен по умолчанию; False - домен требуется обязательно указывать


    * **inURLList** – Список разрешенных URL для пользователя. Для добавления URL рекомендуем использовать функции WebURLConnectDef, WebURLConnectFile, WebURLConnectFolder
    Формат: {

    > »Method»: «GET» || «POST», 
    > «URL»: «/GetScreenshot», 
    > «MatchType»: «BeginWith» || «Equal» || «EqualCase» || «Contains» || «EqualNoParam», 
    > «ResponseDefRequestGlobal»: Функция python || «ResponseFilePath»: Путь к файлу ||  «ResponseFolderPath»: Путь к папке, в которой искать файлы,
    > «ResponseContentType»: пример MIME «image/png»,
    > «UACBool»:False - не выполнять проверку прав доступа по запросу, 
    > «UseCacheBool»: True - кэшировать ответ},



    * **inRoleHierarchyAllowedDict** – Словарь доступа пользователя UAC. Пустой словарь - полный доступ



### pyOpenRPA.Orchestrator.__Orchestrator__.UACUserDictGet(inRequest)
L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.2, см. WebUserUACHierarchyGet] Вернуть UAC (User Access Control) словарь доступов для пользователя, который отправил запрос. Пустой словарь - супердоступ (доступ ко всему)


* **Параметры**

    **inRequest** – Экземпляр request (from http.server import BaseHTTPRequestHandler)



* **Результат**

    Словарь доступов пользователя. Пустой словарь - супердоступ (доступ ко всему)



### pyOpenRPA.Orchestrator.__Orchestrator__.WebAuditMessageCreate(inAuthTokenStr: Optional[str] = None, inHostStr: Optional[str] = None, inOperationCodeStr: str = '-', inMessageStr: str = '-')
L+,W+: [ИЗМЕНЕНИЕ В 1.3.1] Создание сообщения ИТ аудита с такими сведениями как (Домен, IP, логин и тд.). Данная функция особенно актуальна в том случае, если требуется реализовать дополнительные меры контроля ИТ системы.

```
# ПРИМЕР
from pyOpenRPA import Orchestrator

lWebAuditMessageStr = Orchestrator.WebAuditMessageCreate(
    inRequest = lRequest,
    inOperationCodeStr = "OP_CODE_1",
    inMessageStr="Success"):

# Логгирование сообщения
lLogger.info(lWebAuditMessageStr)
```


* **Параметры**

    
    * **inAuthTokenStr** (*str**, **опционально*) – Токен авторизации пользователя / бота, по умолчанию None (не установлен)


    * **inHostStr** (*str**, **опционально*) – IP адрес хоста пользователя / бота, по умолчанию None (не установлен)


    * **inOperationCodeStr** – Код операции, который принят в компании в соответствии с регламентными процедурами


    * **inMessageStr** – Дополнительное сообщение, которое необходимо отправить в сообщение об ИТ аудите



* **Результат**

    Формат сообщения: «WebAudit :: [DOMAINUSER@101.121.123.12](mailto:DOMAINUSER@101.121.123.12) :: operation code :: message»



### pyOpenRPA.Orchestrator.__Orchestrator__.WebCPUpdate(inCPKeyStr, inHTMLRenderDef=None, inJSONGeneratorDef=None, inJSInitGeneratorDef=None, inGSettings=None)
L+,W+: Добавить панель управления робота в Оркестратор. Для панели управления открыт доступ для использования функции-генератора HTML, генератора JSON данных, генератора JS кода.


* **Параметры**

    
    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)


    * **inCPKeyStr** – Текстовый ключ панели управления. Ключ для каждой панели управления должен быть уникальным!


    * **inHTMLRenderDef** – Функция Python для генерации HTML кода. Для использования Jinja2 шаблонов HTML см. pyOpenRPA.Orchestrator.Managers.ControlPanel


    * **inJSONGeneratorDef** – Функция Python для генерации JSON контейнера для отправки на клиентскую часть Оркестратора


    * **inJSInitGeneratorDef** – Функция Python для генерации JS кода для отправки на клиентскую часть Оркестратора



### pyOpenRPA.Orchestrator.__Orchestrator__.WebListenCreate(inServerKeyStr='Default', inAddressStr='0.0.0.0', inPortInt=1024, inCertFilePEMPathStr=None, inKeyFilePathStr=None, inGSettings=None)
L+,W+: Настроить веб-сервер Оркестратора.


* **Параметры**

    
    * **inAddressStr** – IP адрес для прослушивания. Если «0.0.0.0», то прослушивать запросы со всех сетевых карт. Если «127.0.0.1», то слушать запросы только с той ОС, на которой работает Оркестратор


    * **inPortInt** – Номер порта для прослушивания. Если HTTP - 80; Если HTTPS - 443. По умолчанию 1024 (Связано с тем, что в линукс можно устанавливать порты выше 1000). Допускается установка других портов


    * **inCertFilePEMPathStr** – Путь файлу сертификата, сгенерированного в .pem (base64) формате. Обязателен при использовании защищенного HTTPS/SSL соединения.


    * **inKeyFilePathStr** – Путь к файлу закрытого ключа в base64 формате


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



* **Результат**

    


### pyOpenRPA.Orchestrator.__Orchestrator__.WebRequestGet()
L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.2] Вернуть экземпляр HTTP запроса, если функция вызвана в потоке, который был порожден для отработки HTTP запроса пользователя.


### pyOpenRPA.Orchestrator.__Orchestrator__.WebRequestHostGet(inRequest)
L+,W+: Получить наименование хоста, с которого поступил запрос


* **Параметры**

    **inRequest** (*fastapi.Request**, **опционально*) – Экземпляр fastapi.Request, по умолчанию None



* **Результат**

    Наименование хоста



* **Тип результата**

    str



### pyOpenRPA.Orchestrator.__Orchestrator__.WebRequestParseBodyBytes(inRequest=None)
L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.1, см. FASTAPI] Извлечь данные в байт виде из тела (body) HTTP запроса.


* **Параметры**

    **inRequest** – Экземпляр HTTP request. Опционален, если сообщение фиксируется из под потока, который был инициирован запросом пользователя



* **Результат**

    Строка байт b““ или None (если тело запроса было пустым)



### pyOpenRPA.Orchestrator.__Orchestrator__.WebRequestParseBodyJSON(inRequest=None)
L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.1, см. FASTAPI] Извлечь из тела (body) запроса HTTP JSON данные и преобразовать в Dict / List структуры языка Python.


* **Параметры**

    **inRequest** – Экземпляр HTTP request. Опционален, если сообщение фиксируется из под потока, который был инициирован запросом пользователя



* **Результат**

    Словарь (dict), список (list) или None (если тело запроса было пустым)



### pyOpenRPA.Orchestrator.__Orchestrator__.WebRequestParseBodyStr(inRequest=None)
L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.1, см. FASTAPI] Извлечь данные в виде строки из тела (body) HTTP запроса.


* **Параметры**

    **inRequest** – Экземпляр HTTP request. Опционален, если сообщение фиксируется из под потока, который был инициирован запросом пользователя



* **Результат**

    Текстовая строка „“ или None (если тело запроса было пустым)



### pyOpenRPA.Orchestrator.__Orchestrator__.WebRequestParseFile(inRequest=None)
L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.1, см. FASTAPI] Извлечь файл (наименование + содержимое в виде строки байт b““) из HTTP запроса пользователя.


* **Параметры**

    **inRequest** – Экземпляр HTTP request. Опционален, если сообщение фиксируется из под потока, который был инициирован запросом пользователя



* **Результат**

    Кортеж формата (FileNameStr, FileBodyBytes) or (None, None), если файл не был обнаружен



### pyOpenRPA.Orchestrator.__Orchestrator__.WebRequestParsePath(inRequest=None)
L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.1, см. FASTAPI] Извлечь декодированный URL путь из HTTP запроса пользователя в формате строки.


* **Параметры**

    **inRequest** – Экземпляр HTTP request. Опционален, если сообщение фиксируется из под потока, который был инициирован запросом пользователя



* **Результат**

    str, пример: /pyOpenRPA/Debugging/DefHelper



### pyOpenRPA.Orchestrator.__Orchestrator__.WebRequestResponseSend(inResponeStr, inRequest=None, inContentTypeStr: Optional[str] = None, inHeadersDict: Optional[dict] = None)
L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.1, см. FASTAPI] Установить ответ на HTTP запрос пользователя.


* **Параметры**

    
    * **inResponeStr** – Тело ответа (строка)


    * **inRequest** – Экземпляр HTTP request. Опционален, если сообщение фиксируется из под потока, который был инициирован запросом пользователя


    * **inContentTypeStr** – МИМЕ тип. Пример: «html/text»


    * **inHeadersDict** – Словарь (dict) ключей, которые добавить в headers HTTP ответа на запрос пользователя



### pyOpenRPA.Orchestrator.__Orchestrator__.WebURLConnectDef(inMethodStr, inURLStr, inMatchTypeStr, inDef, inContentTypeStr='application/octet-stream', inGSettings=None, inUACBool=None)
L+,W+: Подключить функцию Python к URL.


* **Параметры**

    
    * **inMethodStr** – Метод доступа по URL «GET» || «POST»


    * **inURLStr** – URL адрес. Пример  «/index»


    * **inMatchTypeStr** – Тип соответсвия строки URL с inURLStr: «BeginWith» || «Contains» || «Equal» || «EqualCase» || «EqualNoParam»


    * **inDef** – Функция Python. Допускаются функции, которые принимают следующие наборы параметров: 2:[inRequest, inGSettings], 1: [inRequest], 0: []


    * **inContentTypeStr** – МИМЕ тип. По умолчанию: «application/octet-stream»


    * **inUACBool** – True - Выполнять проверку прав доступа пользователя перед отправкой ответа; False - не выполнять проверку прав доступа пользователя


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



### pyOpenRPA.Orchestrator.__Orchestrator__.WebURLConnectFile(inMethodStr, inURLStr, inMatchTypeStr, inFilePathStr, inContentTypeStr=None, inGSettings=None, inUACBool=None, inUseCacheBool=False)
L+,W+: Подключить файл к URL.


* **Параметры**

    
    * **inMethodStr** – Метод доступа по URL «GET» || «POST»


    * **inURLStr** – URL адрес. Пример  «/index»


    * **inMatchTypeStr** – Тип соответсвия строки URL с inURLStr: «BeginWith» || «Contains» || «Equal» || «EqualCase» || «EqualNoParam»


    * **inFilePathStr** – Путь к файлу на диске, который возвращать пользователю по HTTP


    * **inContentTypeStr** – МИМЕ тип. Если None - выполнить автоматическое определение


    * **inUACBool** – True - Выполнять проверку прав доступа пользователя перед отправкой ответа; False - не выполнять проверку прав доступа пользователя


    * **inUseCacheBool** – True - выполнить кэширование страницы, чтобы в следющих запросах открыть быстрее; False - не кэшировать


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



### pyOpenRPA.Orchestrator.__Orchestrator__.WebURLConnectFolder(inMethodStr, inURLStr, inMatchTypeStr, inFolderPathStr, inGSettings=None, inUACBool=None, inUseCacheBool=False)
L+,W+: Подключить папку к URL.


* **Параметры**

    
    * **inMethodStr** – Метод доступа по URL «GET» || «POST»


    * **inURLStr** – URL адрес. Пример  «/index»


    * **inMatchTypeStr** – Тип соответсвия строки URL с inURLStr: «BeginWith» || «Contains» || «Equal» || «EqualCase» || «EqualNoParam»


    * **inFolderPathStr** – Путь к папке на диске, в которой искать файл и возвращать пользователю по HTTP


    * **inUACBool** – True - Выполнять проверку прав доступа пользователя перед отправкой ответа; False - не выполнять проверку прав доступа пользователя


    * **inUseCacheBool** – True - выполнить кэширование страницы, чтобы в следющих запросах открыть быстрее; False - не кэшировать


    * **inGSettings** – Глобальный словарь настроек Оркестратора (синглтон)



### pyOpenRPA.Orchestrator.__Orchestrator__.WebURLIndexChange(inURLIndexStr: str = '/')
L+,W+: Изменить адрес главной страницы Оркестратора. По умолчанию „/“


* **Параметры**

    **inURLIndexStr** (*str**, **опционально*) – Новый адрес главной страницы Оркестратора.



### pyOpenRPA.Orchestrator.__Orchestrator__.WebUserDomainGet(inAuthTokenStr: Optional[str] = None)
L+,W+: Получить домен авторизованного пользователя. Если авторизация не производилась - вернуть None


* **Параметры**

    **inAuthTokenStr** (*str**, **опционально*) – Токен авторизации пользователя / бота, по умолчанию None (не установлен)



* **Результат**

    Домен пользователя



* **Тип результата**

    str



### pyOpenRPA.Orchestrator.__Orchestrator__.WebUserInfoGet(inRequest=None)
L+,W+: [ПРЕКРАЩЕНИЕ ПОДДЕРЖКИ В 1.3.2, см. WebUserLoginGet, WebUserDomainGet] Информация о пользователе, который отправил HTTP запрос.


* **Параметры**

    **inRequest** – Экземпляр HTTP request. Опционален, если сообщение фиксируется из под потока, который был инициирован запросом пользователя



* **Результат**

    Сведения в формате {«DomainUpperStr»: «PYOPENRPA», «UserNameUpperStr»: «IVAN.MASLOV»}



### pyOpenRPA.Orchestrator.__Orchestrator__.WebUserIsSuperToken(inAuthTokenStr: Optional[str] = None)
L+,W+: [ИЗМЕНЕНИЕ В 1.3.1] Проверить, авторизован ли HTTP запрос с помощью супер токена (токен, который не истекает).


* **Параметры**

    **inAuthTokenStr** (*str**, **опционально*) – Токен авторизации пользователя / бота, по умолчанию None (не установлен)



* **Результат**

    True - является супертокеном; False - не является супертокеном; None - авторизация не производилась



### pyOpenRPA.Orchestrator.__Orchestrator__.WebUserLoginGet(inAuthTokenStr: Optional[str] = None)
L+,W+: Получить логин авторизованного пользователя. Если авторизация не производилась - вернуть None


* **Параметры**

    **inAuthTokenStr** (*str**, **опционально*) – Токен авторизации пользователя / бота, по умолчанию None (не установлен)



* **Результат**

    Логин пользователя



* **Тип результата**

    str



### pyOpenRPA.Orchestrator.__Orchestrator__.WebUserUACCheck(inAuthTokenStr: Optional[str] = None, inKeyList: Optional[list] = None)
L+,W+: Проверить UAC доступ списка ключей для пользователя


* **Параметры**

    **inAuthTokenStr** (*str**, **опционально*) – Токен авторизации пользователя / бота, по умолчанию None (не установлен)



* **Результат**

    True - доступ имеется, False - доступа нет



* **Тип результата**

    bool



### pyOpenRPA.Orchestrator.__Orchestrator__.WebUserUACHierarchyGet(inAuthTokenStr: Optional[str] = None)
L+,W+: [ИЗМЕНЕНИЕ В 1.3.1] Вернуть словарь доступа UAC в отношении пользователя, который выполнил HTTP запрос inRequest


* **Параметры**

    **inAuthTokenStr** (*str**, **опционально*) – Токен авторизации пользователя / бота, по умолчанию None (не установлен)



* **Результат**

    UAC словарь доступа или {}, что означает полный доступ


## Быстрая навигация


* [Сообщество pyOpenRPA (telegram)](https://t.me/pyOpenRPA)


* [Сообщество pyOpenRPA (tenchat)](https://tenchat.ru/iMaslov?utm_source=19f2a84f-3268-437f-950c-d987ae42af24)


* [Сообщество pyOpenRPA (вконтакте)](https://vk.com/pyopenrpa)


* [Презентация pyOpenRPA](https://pyopenrpa.ru/Index/pyOpenRPA_product_service.pdf)


* [Портал pyOpenRPA](https://pyopenrpa.ru)


* [Репозиторий pyOpenRPA](https://gitlab.com/UnicodeLabs/OpenRPA)

		.. v1.3.1 replace:: v1.3.1
