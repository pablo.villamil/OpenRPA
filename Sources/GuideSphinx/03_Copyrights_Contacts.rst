.. _3.-Copyrights-&-Contacts:

####################################
2. Лицензия & Контакты
####################################

**************************************************
Лицензия
**************************************************

Лицензия pyOpenRPA разрешает бесплатное использование только для некоммерческих организаций и физических лиц (не ИП и не самозанятый). В остальных случаях требуется получение цифрового сертификата от правообладателя (ООО "ОПЕН РПА").

Получить, проверить сертификат, а также ознакомиться с текстом лицензионного соглашения Вы можете по адресу:
https://pyopenrpa.ru/verification 

pyOpenRPA не использует какие-либо инструменты физической блокировки функциональности своего ПО.
По всем вопросам Вы можете обратиться к правообладателю, контакты см. по адресу:
https://pyopenrpa.ru/Index/pyOpenRPA_product_service.pdf 
Используя ПО pyOpenRPA Вы осознаете свою ответственность в случаях нарушения лицензионного законодательства и совершения неправомерных действий.

ВНИМАНИЕ! НЕЗНАНИЕ ЗАКОНА НЕ ОСВОБОЖДАЕТ ОТ ОТВЕТСТВЕННОСТИ.

pyOpenRPA - роботы Вам помогут!

**************************************************
Автор
**************************************************
Маслов Иван Дмитриевич (с 2019г.)

**************************************************
Правообладатель
**************************************************
**с 2022г.**
ООО "ОПЕН РПА", ОГРН 1227700251350, ИНН/КПП 9718191421/771801001 
г. Москва: 125310, улица Муравская
г. Санкт-Петербург: 197022, улица Льва Толстого

**с 2019г. по 2021г.**
Маслов Иван Дмитриевич (с 2019г.)

**************************************************
Центр поддержки клиентов
**************************************************

У вас остались вопросы? Мы вам поможем!

- Телефон/WhatsApp: +7 (995) 233-45-31
- Почта: Support@pyOpenRPA.ru
- Телеграм: @pyOpenRPA_support :: `Написать <https://t.me/pyOpenRPA_support>`_
- Портал: https://pyopenrpa.ru/
- Телеграм канал: @pyOpenRPA :: `Присоединиться <https://t.me/pyOpenRPA>`_

**************************************************
Иван Маслов (генеральный директор ООО "ОПЕН РПА")
**************************************************

- Телефон/WhatsApp: +7 (906) 722-39-25
- Почта: Ivan.Maslov@pyOpenRPA.ru
- Телеграм: @IvanMaslov :: `Написать <https://t.me/IvanMaslov>`_
- Портал: https://pyopenrpa.ru/
- Телеграм канал: @pyOpenRPA :: `Присоединиться <https://t.me/pyOpenRPA>`_


**********************************************************
Используемые сторонние компоненты (лицензионная чистота)
**********************************************************

- WinPython, v3.7.1 32-bit & 64-bit, лицензия MIT https://github.com/winpython/winpython
- Selenium, v3.141.0, лицензия Apache 2.0 https://github.com/SeleniumHQ/selenium/blob/trunk/LICENSE
- pywinauto, v0.6.5, лицензия BSD 3-Clause https://github.com/pywinauto/pywinauto
- Semantic UI, v2.4.1, лицензия MIT https://github.com/Semantic-Org/Semantic-UI
- PyAutoGUI, v0.9.44, лицензия BSD 3-Clause https://github.com/asweigart/pyautogui
- keyboard, v0.13.3, лицензия MIT https://github.com/boppreh/keyboard)
- pywin32 (win32api), v228, Python Software Foundation лицензия (PSF) https://github.com/mhammond/pywin32
- WMI, v1.4.9, лицензия MIT, http://www.opensource.org/licenses/mit-license.php
- psutil, v5.6.2, лицензия BSD 3-Clause https://github.com/giampaolo/psutil/blob/master/LICENSE
- Pillow PIL, v6.0.0, лицензия HPND https://github.com/python-pillow/Pillow/blob/main/LICENSE
- requests, v2.22.0, лицензия Apache 2.0 https://github.com/psf/requests/blob/main/LICENSE
- JsRender, v1.0.2, лицензия MIT https://github.com/BorisMoore/jsrender/blob/master/MIT-LICENSE.txt
- Handlebars, v4.1.2, лицензия MIT, https://github.com/handlebars-lang/handlebars.js/blob/master/LICENSE
- jinja2, v2.11.2, лицензия BSD 3-Clause, https://github.com/pallets/jinja/blob/main/LICENSE.rst
- JupiterNotebook v6.1.4, лицензия BSD 3-Clause, https://github.com/jupyter/notebook/blob/main/LICENSE
- schedule, v1.1.0,  лицензия MIT, https://github.com/dbader/schedule/blob/master/LICENSE.txt
- pyscreeze
- opencv
- numpy

******************************
Быстрая навигация
******************************

- `Сообщество pyOpenRPA (telegram) <https://t.me/pyOpenRPA>`_
- `Сообщество pyOpenRPA (tenchat) <https://tenchat.ru/iMaslov?utm_source=19f2a84f-3268-437f-950c-d987ae42af24>`_
- `Сообщество pyOpenRPA (вконтакте) <https://vk.com/pyopenrpa>`_
- `Презентация pyOpenRPA <https://pyopenrpa.ru/Index/pyOpenRPA_product_service.pdf>`_
- `Портал pyOpenRPA <https://pyopenrpa.ru>`_
- `Репозиторий pyOpenRPA <https://gitlab.com/UnicodeLabs/OpenRPA>`_