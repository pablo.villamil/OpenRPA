import json
def SettingsGet():
    #Init config
    lResult = {
        "URL":"http://localhost:8081/SQLInsert", #Server host, example "http://localhost" Without / in the end
        "Method":"POST", #Method to send HTTP (GET, POST...)
        "Headers":{}, #Dictionary
        "Cookies":{}, #Dictionary
        "JSON":[{"TableName":"Test", "RowDict":{"Name":"Name1","Description":"DescTest", "Money":100, "Date":"01.01.2020"}}] #JSON data
    }
    return lResult