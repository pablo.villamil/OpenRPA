# 2. Функции Agent

## Общее

Здесь представлено описание всех функций, которые используются на стороне графических учетных записей. Функции доступны для использования, благодаря компоненту Агента, который запускается на всех графических сессиях робота.

## Описание функций

Описание каждой функции начинается с обозначения L+,W+, что означает, что функция поддерживается в ОС Linux (L) и поддерживается в Windows (W)

**Functions:**

| `OSCMD`(inCMDStr[, inRunAsyncBool, …])

 | L-,W+: Execute CMD on the Agent daemonic process

 |
| `OSFileBinaryDataBase64StrAppend`(…[, …])

 | L+,W+: Create binary file by the base64 string (safe for JSON transmition)

 |
| `OSFileBinaryDataBase64StrCreate`(…[, …])

 | L+,W+: Создать бинарный файл на стороне Агента по полученной строке в формате base64 (формат безопасен для передачи по JSON протоколу)

 |
| `OSFileBinaryDataBase64StrReceive`(inFilePathStr)

 | L+,W+: Прочитать бинарный файл на стороне агента и отправить на сторону оркестратора в формате base64 (формат безопасный для передачи в формате JSON)

 |
| `OSFileMTimeGet`(inFilePathStr)

                   | L+,W+: Read file modification time timestamp format (float)

                                                                                           |
| `OSFileTextDataStrCreate`(inFilePathStr, …)

       | L+,W+:Создать текстовый файл на стороне Агента

                                                                                                        |
| `OSFileTextDataStrReceive`(inFilePathStr[, …])

    | L+,W+: Прочитать текстовый файл на стороне агента и отправить на сторону оркестратора

                                                                 |
| `ProcessWOExeUpperUserListGet`()

                  | L-,W+: Вернуть список процессов, запущенных под пользователем на стороне агента

                                                                       |

### pyOpenRPA.Agent.__Agent__.OSCMD(inCMDStr, inRunAsyncBool=True, inGSettings=None, inSendOutputToOrchestratorLogsBool=True, inCMDEncodingStr='cp1251', inCaptureBool=True)
L-,W+: Execute CMD on the Agent daemonic process


* **Параметры**

    
    * **inCMDStr** – command to execute on the Agent session


    * **inRunAsyncBool** – True - Agent processor don’t wait execution; False - Agent processor wait cmd execution


    * **inGSettings** – Agent global settings dict


    * **inSendOutputToOrchestratorLogsBool** – True - catch cmd execution output and send it to the Orchestrator logs; Flase - else case; Default True


    * **inCMDEncodingStr** – Set the encoding of the DOS window on the Agent server session. Windows is beautiful :) . Default is «cp1251» early was «cp866» - need test


    * **inCaptureBool** – !ATTENTION! If you need to start absolutely encapsulated app - set this flag as False. If you set True - the app output will come to Agent



* **Результат**

    


### pyOpenRPA.Agent.__Agent__.OSFileBinaryDataBase64StrAppend(inFilePathStr, inFileDataBase64Str, inGSettings=None)
L+,W+: Create binary file by the base64 string (safe for JSON transmition)


### pyOpenRPA.Agent.__Agent__.OSFileBinaryDataBase64StrCreate(inFilePathStr, inFileDataBase64Str, inGSettings=None)
L+,W+: Создать бинарный файл на стороне Агента по полученной строке в формате base64 (формат безопасен для передачи по JSON протоколу)


### pyOpenRPA.Agent.__Agent__.OSFileBinaryDataBase64StrReceive(inFilePathStr, inGSettings=None)
L+,W+: Прочитать бинарный файл на стороне агента и отправить на сторону оркестратора в формате base64 (формат безопасный для передачи в формате JSON)


* **Параметры**

    
    * **inFilePathStr** – Абсолютный путь к читаемому файлу


    * **inGSettings** – Глобальный словарь настроек Агента (необязательный)



* **Результат**

    Содержимое бинарного файле, преобразованное в формат base64 (используй base64.b64decode для декодирования в байты). Вернет None запрашиваемый файл не существует



### pyOpenRPA.Agent.__Agent__.OSFileMTimeGet(inFilePathStr: str)
L+,W+: Read file modification time timestamp format (float)


* **Параметры**

    **inFilePathStr** – Абсолютный путь к файлу, дату которого требуется получить



* **Результат**

    Временной слепок (timestamp) в формате float. Вернет None, если запрашиваемый файл не существует



### pyOpenRPA.Agent.__Agent__.OSFileTextDataStrCreate(inFilePathStr, inFileDataStr, inEncodingStr='utf-8', inGSettings=None)
L+,W+:Создать текстовый файл на стороне Агента


* **Параметры**

    
    * **inFilePathStr** – Абсолютный путь к создаваемому файлу


    * **inFileDataStr** – Текст, отправляемый в создаваемый файл


    * **inEncodingStr** – Кодировка создаваемого файла. По-умолчанию „utf-8“


    * **inGSettings** – Глобальный файл настроек



* **Результат**

    


### pyOpenRPA.Agent.__Agent__.OSFileTextDataStrReceive(inFilePathStr, inEncodingStr='utf-8', inGSettings=None)
L+,W+: Прочитать текстовый файл на стороне агента и отправить на сторону оркестратора


* **Параметры**

    
    * **inFilePathStr** – Абсолютный путь к читаемому файлу


    * **inEncodingStr** – Кодировка создаваемого файла. По-умолчанию „utf-8“


    * **inGSettings** – Глобальный словарь настроек



* **Результат**

    Строка - содержимое текстового файла. Возвращает None, если файл не существует



### pyOpenRPA.Agent.__Agent__.ProcessWOExeUpperUserListGet()
L-,W+: Вернуть список процессов, запущенных под пользователем на стороне агента


* **Результат**

    Список процессов в формате: [«NOTEPAD»,»…»] (без постфикса .exe и в верхнем регистре)


## Быстрая навигация


* [Сообщество pyOpenRPA (telegram)](https://t.me/pyOpenRPA)


* [Сообщество pyOpenRPA (tenchat)](https://tenchat.ru/iMaslov?utm_source=19f2a84f-3268-437f-950c-d987ae42af24)


* [Сообщество pyOpenRPA (вконтакте)](https://vk.com/pyopenrpa)


* [Презентация pyOpenRPA](https://pyopenrpa.ru/Index/pyOpenRPA_product_service.pdf)


* [Портал pyOpenRPA](https://pyopenrpa.ru)


* [Репозиторий pyOpenRPA](https://gitlab.com/UnicodeLabs/OpenRPA)

		.. v1.3.1 replace:: v1.3.1
