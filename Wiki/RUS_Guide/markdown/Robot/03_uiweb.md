# 3. Функции UIWeb

## Общее

Здесь представлено описание всех функций, необходимых для максимально эффективного управления web интерфейсами различных приложений.

## Описание функций

Описание каждой функции начинается с обозначения L+,W+, что означает, что функция поддерживается в ОС Linux (L) и Windows (W)

**Functions:**

| `BrowserChange`(inBrowser)

 | L+,W+: Выполнить смену активного браузера (при необходимости).

 |
| `BrowserChromeStart`([inDriverExePathStr, …])

       | L+,W+: Выполнить запуск браузера Chrome.

                                                                                                                                                                       |
| `BrowserClose`()

                                    | L+,W+: Закрыть браузер

                                                                                                                                                                                         |
| `PageJSExecute`(inJSStr, \*inArgList)

                | L+,W+: Отправить на выполнение на сторону браузера код JavaScript.

                                                                                                                                             |
| `PageOpen`(inURLStr)

                                | L+,W+: Открыть страницу inURLStr в браузере и дождаться ее загрузки.

                                                                                                                                           |
| `PageScrollTo`([inVerticalPxInt, …])

                | L+,W+: Выполнить прокрутку страницы (по вертикали или по горизонтали)

                                                                                                                                          |
| `UIOAttributeGet`(inUIO, inAttributeStr)

            | L+,W+: Получить обычный (нестилевой) атрибут у UI элемента.

                                                                                                                                                    |
| `UIOAttributeRemove`(inUIO, inAttributeStr)

         | L+,W+: Удалить обычный (нестилевой) атрибут у UI элемента.

                                                                                                                                                     |
| `UIOAttributeSet`(inUIO, inAttributeStr, inValue)

   | L+,W+: Установить обычный (нестилевой) атрибут у UI элемента.

                                                                                                                                                  |
| `UIOAttributeStyleGet`(inUIO, inAttributeStr)

       | L+,W+: Получить стилевой атрибут у UI элемента.

                                                                                                                                                                |
| `UIOAttributeStyleRemove`(inUIO, inAttributeStr)

    | L+,W+: Удалить стилевой атрибут у UI элемента.

                                                                                                                                                                 |
| `UIOAttributeStyleSet`(inUIO, inAttributeStr, …)

    | L+,W+: Установить стилевой атрибут у UI элемента.

                                                                                                                                                              |
| `UIOClick`(inUIO)

                                   | L+,W+: Выполнить нажатие по элементу inUIO.

                                                                                                                                                                    |
| `UIOMouseSearchInit`()

                              | L+,W+: Инициализирует процесс поиска UI элемента с помощью мыши.

                                                                                                                                               |
| `UIOMouseSearchReturn`()

                            | L+,W+: Возвращает UIO объект, над которым находится указатель мыши.

                                                                                                                                            |
| `UIOSelectorClick`(inUIOSelectorStr)

                | L+,W+: Выполнить нажатие по элементу с селектором inUIOSelectorStr.

                                                                                                                                            |
| `UIOSelectorDetect`(inUIOSelectorStr)

               | L+,W+: Идентифицировать стиль селектора (CSS или XPATH)

                                                                                                                                                        |
| `UIOSelectorFirst`(inUIOSelectorStr[, inUIO])

       | L+,W+: Получить UIO объект по UIO селектору.

                                                                                                                                                                   |
| `UIOSelectorHighlight`(inUIOSelectorStr[, …])

       | L+,W+: Выполнить подсвечивание UI элемента с селектором inUIOSelectorStr.

                                                                                                                                      |
| `UIOSelectorList`(inUIOSelectorStr[, inUIO])

        | L+,W+: Получить список UIO объектов по UIO селектору.

                                                                                                                                                          |
| `UIOSelectorWaitAppear`(inUIOSelectorStr[, …])

      | L+,W+: Ожидать появление UI элемента на веб странице (блокирует выполнение потока), заданного по UIO селектору inUIOSelectorStr.

                                                                               |
| `UIOSelectorWaitDisappear`(inUIOSelectorStr[, …])

   | L+,W+: Ожидать исчезновение UI элемента с веб страницы (блокирует выполнение потока), заданного по UIO селектору inUIOSelectorStr.

                                                                             |
| `UIOTextGet`(inUIO)

                                 | L+,W+: Получить текст UI элемента.

                                                                                                                                                                             |

### pyOpenRPA.Robot.UIWeb.BrowserChange(inBrowser)
L+,W+: Выполнить смену активного браузера (при необходимости).

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
lBrowser1 = UIWeb.BrowserChromeStart()
UIWeb.BrowserChange(inBrowser=None)
lBrowser2 = UIWeb.BrowserChromeStart()
UIWeb.BrowserClose()
UIWeb.BrowserChange(inBrowser=lBrowser1)
UIWeb.BrowserClose()
```


* **Параметры**

    **inBrowser** (*webdriver.Chrome*) – Объект браузера



### pyOpenRPA.Robot.UIWeb.BrowserChromeStart(inDriverExePathStr: Optional[str] = None, inChromeExePathStr: Optional[str] = None, inExtensionPathList: Optional[list] = None, inProfilePathStr: Optional[str] = None)
L+,W+: Выполнить запуск браузера Chrome. Если вы скачали pyOpenRPA вместе с репозиторием, то будет использоваться встроенный браузер Google Chrome. Если установка pyOpenRPA производилась другим способом, то требуется указать расположение браузера Google Chrome и соответствующего WebDriver.

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
UIWeb.BrowserChromeStart()
UIWeb.BrowserClose()
```


* **Параметры**

    **inDriverExePathStr** (*str**, **опционально*) – Путь до компонента webdriver.exe, по умолчанию None (путь до webdriver.exe, который расположен в репозитории pyOpenRPA)


:param inChromeExePathStr:Путь до компонента chrome.exe, по умолчанию None (путь до chrome.exe, который расположен в репозитории pyOpenRPA)
:type inChromeExePathStr: str, опционально
:param inExtensionPathList: Список путей, по которым располагаются расширения Chrome, по умолчанию None
:type inExtensionPathList: list, опционально
:param inProfilePathStr: Путь, по которому выполнить сохранения профиля Chrome (история, куки и т.д.), по умолчанию None (профиль не сохраняется)
:type inProfilePathStr: str, опционально
:return: Объект браузера Google Chrome
:rtype: webdriver.Chrome


### pyOpenRPA.Robot.UIWeb.BrowserClose()
L+,W+: Закрыть браузер

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
UIWeb.BrowserClose()
```


### pyOpenRPA.Robot.UIWeb.PageJSExecute(inJSStr, \*inArgList)
L+,W+: Отправить на выполнение на сторону браузера код JavaScript.

!ВНИМАНИЕ! Данная функция поддерживает передачу переменных в область кода JavaScript (

```
*
```

inArgList). Обратиться к переданным переменным из JavaScript можно с помощью ключевого слова: arguments[i], где i - это порядковый номер переданной переменной

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
UIWeb.PageJSExecute(alert('arguments[0]);", "hello world!")
UIWeb.BrowserClose()
```


* **Параметры**

    
    * **inJSStr** (*str*) – Код JavaScript, отправляемый на сторону браузера


    * **\*inArgList** – Перечисление аргументов, отправляемых на сторону браузера




* **Результат**

    Результат отработки кода JavaScript, если он заканчивался оператором «return»



* **Тип результата**

    str | int | bool | float



### pyOpenRPA.Robot.UIWeb.PageOpen(inURLStr: str)
L+,W+: Открыть страницу inURLStr в браузере и дождаться ее загрузки.

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
UIWeb.BrowserClose()
```


* **Параметры**

    **inURLStr** (*str*) – URL адрес страницы



### pyOpenRPA.Robot.UIWeb.PageScrollTo(inVerticalPxInt=0, inHorizontalPxInt=0)
L+,W+: Выполнить прокрутку страницы (по вертикали или по горизонтали)

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
UIWeb.PageScrollTo(inVerticalPxInt=100)
UIWeb.BrowserClose()
```


* **Параметры**

    
    * **inVerticalPxInt** (*int**, **опционально*) – Величина вертикальной прокрутки страницы в пикселях, по умолчанию 0


    * **inHorizontalPxInt** (*int**, **опционально*) – Величина горизонтальной прокрутки страницы в пикселях, по умолчанию 0



### pyOpenRPA.Robot.UIWeb.UIOAttributeGet(inUIO, inAttributeStr)
L+,W+: Получить обычный (нестилевой) атрибут у UI элемента.

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
lUIOSelectorStr = "//*[@id="grid"]/div[2]/div[2]/div[3]/div[1]/ul/li[5]/div/a"
lUIO = UIWeb.UIOSelectorList(inUIOSelectorStr = lUIOSelectorStr)[0]
UIWeb.UIOAttributeGet(inUIO=lUIO, inAttributeStr = "href")
UIWeb.BrowserClose()
```


* **Параметры**

    
    * **inUIO** (*WebElement*) – UIO элемент. Получить его можно с помощью функций UIOSelectorList или UIOSelectorFirst


    * **inAttributeStr** (*str*) – Наименование обычного (нестилевого) атрибута



* **Результат**

    Значение обычного (нестилевого) атрибута



* **Тип результата**

    str



### pyOpenRPA.Robot.UIWeb.UIOAttributeRemove(inUIO, inAttributeStr)
L+,W+: Удалить обычный (нестилевой) атрибут у UI элемента.

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
lUIOSelectorStr = "//*[@id="grid"]/div[2]/div[2]/div[3]/div[1]/ul/li[5]/div/a"
lUIO = UIWeb.UIOSelectorList(inUIOSelectorStr = lUIOSelectorStr)[0]
UIWeb.UIOAttributeRemove(lUIO, "href")
UIWeb.BrowserClose()
```


* **Параметры**

    
    * **inUIO** (*WebElement*) – UIO элемент. Получить его можно с помощью функций UIOSelectorList или UIOSelectorFirst


    * **inAttributeStr** (*str*) – Наименование обычного (нестилевого) атрибута



### pyOpenRPA.Robot.UIWeb.UIOAttributeSet(inUIO, inAttributeStr, inValue)
L+,W+: Установить обычный (нестилевой) атрибут у UI элемента.

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
lUIOSelectorStr = "//*[@id="grid"]/div[2]/div[2]/div[3]/div[1]/ul/li[5]/div/a"
lUIO = UIWeb.UIOSelectorList(inUIOSelectorStr = lUIOSelectorStr)[0]
UIWeb.UIOAttributeSet(inUIO=lUIO, inAttributeStr = "href", inValue = "https://mail.ru")
UIWeb.BrowserClose()
```


* **Параметры**

    
    * **inUIO** (*WebElement*) – UIO элемент. Получить его можно с помощью функций UIOSelectorList или UIOSelectorFirst


    * **inAttributeStr** (*str*) – Наименование обычного (нестилевого) атрибута


    * **inValue** (*str*) – Устанавливаемое значение обычного (нестилевого) атрибута



### pyOpenRPA.Robot.UIWeb.UIOAttributeStyleGet(inUIO, inAttributeStr)
L+,W+: Получить стилевой атрибут у UI элемента.

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
lUIOSelectorStr = "//*[@id="grid"]/div[2]/div[2]/div[3]/div[1]/ul/li[5]/div/a"
lUIO = UIWeb.UIOSelectorList(inUIOSelectorStr = lUIOSelectorStr)[0]
UIWeb.UIOAttributeStyleGet(inUIO=lUIO, inAttributeStr = "href")
UIWeb.BrowserClose()
```


* **Параметры**

    
    * **inUIO** (*WebElement*) – UIO элемент. Получить его можно с помощью функций UIOSelectorList или UIOSelectorFirst


    * **inAttributeStr** (*str*) – Наименование стилевого атрибута



* **Результат**

    Значение стилевого атрибута



* **Тип результата**

    str



### pyOpenRPA.Robot.UIWeb.UIOAttributeStyleRemove(inUIO, inAttributeStr: str)
L+,W+: Удалить стилевой атрибут у UI элемента.

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
lUIOSelectorStr = "//*[@id="grid"]/div[2]/div[2]/div[3]/div[1]/ul/li[5]/div/a"
lUIO = UIWeb.UIOSelectorList(inUIOSelectorStr = lUIOSelectorStr)[0]
UIWeb.UIOAttributeStyleRemove(lUIO, "color")
UIWeb.BrowserClose()
```


* **Параметры**

    
    * **inUIO** (*WebElement*) – UIO элемент. Получить его можно с помощью функций UIOSelectorList или UIOSelectorFirst


    * **inAttributeStr** (*str*) – Наименование стилевого атрибута



### pyOpenRPA.Robot.UIWeb.UIOAttributeStyleSet(inUIO, inAttributeStr, inValue)
L+,W+: Установить стилевой атрибут у UI элемента.

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
lUIOSelectorStr = "//*[@id="grid"]/div[2]/div[2]/div[3]/div[1]/ul/li[5]/div/a"
lUIO = UIWeb.UIOSelectorList(inUIOSelectorStr = lUIOSelectorStr)[0]
UIWeb.UIOAttributeStyleSet(inUIO=lUIO, inAttributeStr = "color", inValue = "grey")
UIWeb.BrowserClose()
```


* **Параметры**

    
    * **inUIO** (*WebElement*) – UIO элемент. Получить его можно с помощью функций UIOSelectorList или UIOSelectorFirst


    * **inAttributeStr** (*str*) – Наименование стилевого атрибута


    * **inValue** (*str*) – Устанавливаемое значение стилевого атрибута



### pyOpenRPA.Robot.UIWeb.UIOClick(inUIO)
L+,W+: Выполнить нажатие по элементу inUIO.

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
lUIOSelectorStr = "//*[@id="grid"]/div[2]/div[2]/div[3]/div[1]/ul/li[5]/div/a"
lUIO = UIWeb.UIOSelectorList(inUIOSelectorStr = lUIOSelectorStr)[0]
UIOClick(inUIO = lUIO)
UIWeb.BrowserClose()
```


* **Параметры**

    **inUIO** (*WebElement*) – UIO элемент. Получить его можно с помощью функций UIOSelectorList или UIOSelectorFirst



### pyOpenRPA.Robot.UIWeb.UIOMouseSearchInit()
L+,W+: Инициализирует процесс поиска UI элемента с помощью мыши. Для прекращения поиска необходимо использовать функцию: UIOMouseSearchReturn

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
import time
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
UIWeb.UIOMouseSearchInit()
time.sleep(3)
UIWeb.UIOMouseSearchReturn()
UIWeb.BrowserClose()
```


### pyOpenRPA.Robot.UIWeb.UIOMouseSearchReturn()
L+,W+: Возвращает UIO объект, над которым находится указатель мыши. Предварительно должна быть вызвана функция UIWeb.UIOMouseSearchInit

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
import time
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
UIWeb.UIOMouseSearchInit()
time.sleep(3)
UIWeb.UIOMouseSearchReturn()
UIWeb.BrowserClose()
```


* **Результат**

    UIO объект



* **Тип результата**

    webelement



### pyOpenRPA.Robot.UIWeb.UIOSelectorClick(inUIOSelectorStr: str)
L+,W+: Выполнить нажатие по элементу с селектором inUIOSelectorStr.

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
lUIOSelectorStr = "//*[@id="grid"]/div[2]/div[2]/div[3]/div[1]/ul/li[5]/div/a"
UIWeb.UIOSelectorClick(inUIOSelectorStr = lUIOSelectorStr)
UIWeb.BrowserClose()
```


* **Параметры**

    **inUIOSelectorStr** (*str*) – XPATH или CSS селектор UI элемента на web странице. Подсказки по CSS: [https://devhints.io/css](https://devhints.io/css) Подсказки по XPath: [https://devhints.io/xpath](https://devhints.io/xpath)



### pyOpenRPA.Robot.UIWeb.UIOSelectorDetect(inUIOSelectorStr: str)
L+,W+: Идентифицировать стиль селектора (CSS или XPATH)

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
lUIOSelectorStr = "#grid > div.grid-middle > div.grid__main-col.svelte-2y66pa > div.grid_newscol.grid_newscol__more-pulse.svelte-1yvqfic > div.grid__ccol.svelte-1yvqfic > ul > li:nth-child(5) > div > a"
lUIOSelectorStr = "//*[@id="grid"]/div[2]/div[2]/div[3]/div[1]/ul/li[5]/div/a"
lResultStr = UIWeb.UIOSelectorDetect(inUIOSelectorStr = lUIOSelectorStr)
```


* **Параметры**

    **inUIOSelectorStr** (*str*) – XPATH или CSS селектор UI объекта на web странице. Подсказки по CSS: [https://devhints.io/css](https://devhints.io/css) Подсказки по XPath: [https://devhints.io/xpath](https://devhints.io/xpath)



* **Результат**

    «CSS» или «XPATH»



* **Тип результата**

    str



### pyOpenRPA.Robot.UIWeb.UIOSelectorFirst(inUIOSelectorStr, inUIO=None)
L+,W+: Получить UIO объект по UIO селектору.

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
lUIOSelectorStr = "//*[@id="grid"]/div[2]/div[2]/div[3]/div[1]/ul/li[5]/div/a"
lUIO = UIWeb.UIOSelectorFirst(inUIOSelectorStr = lUIOSelectorStr)
UIWeb.BrowserClose()
```


* **Параметры**

    
    * **inUIOSelectorStr** (*str*) – XPATH или CSS селектор UI объекта на web странице. Подсказки по CSS: [https://devhints.io/css](https://devhints.io/css) Подсказки по XPath: [https://devhints.io/xpath](https://devhints.io/xpath)


    * **inUIO** (*WebElement**, **опционально*) – Объект UIO, от которого выполнить поиск UIO объектов по селектору, по умолчанию None



* **Результат**

    Первый подходящий UIO объект



* **Тип результата**

    UIO объект



### pyOpenRPA.Robot.UIWeb.UIOSelectorHighlight(inUIOSelectorStr: str, inIsFirst: bool = False, inDurationSecFloat: float = 3.0, inColorStr: str = 'green')
L+,W+: Выполнить подсвечивание UI элемента с селектором inUIOSelectorStr.

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
lUIOSelectorStr = "//*[@id="grid"]/div[2]/div[2]/div[3]/div[1]/ul/li[5]/div/a"
UIWeb.UIOSelectorHighlight(inUIOSelectorStr = lUIOSelectorStr)
UIWeb.BrowserClose()
```


* **Параметры**

    
    * **inUIOSelectorStr** (*str*) – XPATH или CSS селектор UI элемента на web странице. Подсказки по CSS: [https://devhints.io/css](https://devhints.io/css) Подсказки по XPath: [https://devhints.io/xpath](https://devhints.io/xpath)


    * **inIsFirst** (*bool**, **опционально*) – True - подсветить только первый элемент, который удовлетворяет селектору. По умолчанию False


    * **inDurationSecFloat** (*float**, **опционально*) – Длительность подсвечивания. По умолчанию 3.0 сек.


    * **inColorStr** (*str**, **опционально*) – Цвет подсвечания Варианты: «red», «blue», «grey», «yellow». По умолчанию «green» (зеленый)



### pyOpenRPA.Robot.UIWeb.UIOSelectorList(inUIOSelectorStr, inUIO=None)
L+,W+: Получить список UIO объектов по UIO селектору.

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
lUIOSelectorStr = "//*[@id="grid"]/div[2]/div[2]/div[3]/div[1]/ul/li[5]/div/a"
lUIOList = UIWeb.UIOSelectorList(inUIOSelectorStr = lUIOSelectorStr)
UIWeb.BrowserClose()
```


* **Параметры**

    
    * **inUIOSelectorStr** (*str*) – XPATH или CSS селектор UI объекта на web странице. Подсказки по CSS: [https://devhints.io/css](https://devhints.io/css) Подсказки по XPath: [https://devhints.io/xpath](https://devhints.io/xpath)


    * **inUIO** (*WebElement**, **опционально*) – Объект UIO, от которого выполнить поиск UIO объектов по селектору, по умолчанию None



* **Результат**

    Список UIO объектов



* **Тип результата**

    list



### pyOpenRPA.Robot.UIWeb.UIOSelectorWaitAppear(inUIOSelectorStr: str, inWaitSecFloat: float = 60, inWaitIntervalSecFloat: float = 1.0)
L+,W+: Ожидать появление UI элемента на веб странице (блокирует выполнение потока), заданного по UIO селектору inUIOSelectorStr. Выполнять ожидание на протяжении inWaitSecFloat (по умолчанию 60 сек.). Проверка производится с интервалом inWaitIntervalSecFloat (по умолчанию 1 сек.)

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
lUIOSelectorStr = "//*[@id="grid"]/div[2]/div[2]/div[3]/div[1]/ul/li[5]/div/a"
lAppearUIOList = UIWeb.UIOSelectorWaitAppear(inUIOSelectorStr = lUIOSelectorStr)
UIWeb.BrowserClose()
```


* **Параметры**

    
    * **inUIOSelectorStr** (*str*) – XPATH или CSS селектор UI элемента на web странице. Подсказки по CSS: [https://devhints.io/css](https://devhints.io/css) Подсказки по XPath: [https://devhints.io/xpath](https://devhints.io/xpath)


    * **inWaitSecFloat** (*float**, **опциональный*) – Время ожидания на исчезновение UI элемента, по умолчанию UIO_WAIT_SEC_FLOAT (60 сек)


    * **inWaitIntervalSecFloat** (*float**, **опциональный*) – Интервал проверки исчезновения, по умолчанию UIO_WAIT_INTERVAL_SEC_FLOAT (1 сек)



* **Исключение**

    **Exception** – Время ожидания превышено



* **Результат**

    Список UI элементов, которые удовлетворяют селектору и появились на странице



* **Тип результата**

    list



### pyOpenRPA.Robot.UIWeb.UIOSelectorWaitDisappear(inUIOSelectorStr: str, inWaitSecFloat: float = 60, inWaitIntervalSecFloat: float = 1.0)
L+,W+: Ожидать исчезновение UI элемента с веб страницы (блокирует выполнение потока), заданного по UIO селектору inUIOSelectorStr. Выполнять ожидание на протяжении inWaitSecFloat (по умолчанию 60 сек.). Проверка производится с интервалом inWaitIntervalSecFloat (по умолчанию 1 сек.)

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
lUIOSelectorStr = "//*[@id="grid"]/div[2]/div[2]/div[3]/div[1]/ul/li[5]/div/a"
UIWeb.UIOSelectorWaitDisappear(inUIOSelectorStr = lUIOSelectorStr)
UIWeb.BrowserClose()
```


* **Параметры**

    
    * **inUIOSelectorStr** (*str*) – XPATH или CSS селектор UI элемента на web странице. Подсказки по CSS: [https://devhints.io/css](https://devhints.io/css) Подсказки по XPath: [https://devhints.io/xpath](https://devhints.io/xpath)


    * **inWaitSecFloat** (*float**, **опциональный*) – Время ожидания на исчезновение UI элемента, по умолчанию UIO_WAIT_SEC_FLOAT (60 сек)


    * **inWaitIntervalSecFloat** (*float**, **опциональный*) – Интервал проверки исчезновения, по умолчанию UIO_WAIT_INTERVAL_SEC_FLOAT (1 сек)



* **Исключение**

    **Exception** – Время ожидания превышено



### pyOpenRPA.Robot.UIWeb.UIOTextGet(inUIO)
L+,W+: Получить текст UI элемента.

```
# UIWeb: Взаимодействие с ui web
from pyOpenRPA.Robot import UIWeb
UIWeb.BrowserChromeStart()
UIWeb.PageOpen("https://mail.ru")
lUIOSelectorStr = "//*[@id="grid"]/div[2]/div[2]/div[3]/div[1]/ul/li[5]/div/a"
lUIO = UIWeb.UIOSelectorList(inUIOSelectorStr = lUIOSelectorStr)[0]
lTextStr = UIWeb.UIOTextGet(inUIO=lUIO)
UIWeb.BrowserClose()
```


* **Параметры**

    **inUIO** (*WebElement*) – UIO элемент. Получить его можно с помощью функций UIOSelectorList или UIOSelectorFirst



* **Результат**

    Текст UI элемента



* **Тип результата**

    str


## Быстрая навигация


* [Сообщество pyOpenRPA (telegram)](https://t.me/pyOpenRPA)


* [Сообщество pyOpenRPA (tenchat)](https://tenchat.ru/iMaslov?utm_source=19f2a84f-3268-437f-950c-d987ae42af24)


* [Сообщество pyOpenRPA (вконтакте)](https://vk.com/pyopenrpa)


* [Презентация pyOpenRPA](https://pyopenrpa.ru/Index/pyOpenRPA_product_service.pdf)


* [Портал pyOpenRPA](https://pyopenrpa.ru)


* [Репозиторий pyOpenRPA](https://gitlab.com/UnicodeLabs/OpenRPA)

		.. v1.3.1 replace:: v1.3.1
