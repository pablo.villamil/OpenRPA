#########Get settings############
gSettings= None #Init  variable
import Settings #Tool settings
gSettings = Settings.SettingsGet()
##################################
import requests #Lib for HTTP requests

#Create HTTP session
lSession = requests.Session()
lResponse = None
#If GET
if gSettings["Method"].upper()=="GET":
    lResponse = lSession.get(gSettings["URL"], headers=gSettings["Headers"], cookies=gSettings["Cookies"], verify=False, json = gSettings["JSON"])
#If POST
elif gSettings["Method"].upper()=="POST":
    lResponse = lSession.post(gSettings["URL"], headers=gSettings["Headers"], cookies=gSettings["Cookies"], verify=False, json = gSettings["JSON"])

print(f'SessionObject: {lResponse}, Body: {lResponse.text}')

#Debug section
while True:
    lCommand = input("Write Python cmd:") #Wait for python command
    exec(lCommand) #Execute the command
    