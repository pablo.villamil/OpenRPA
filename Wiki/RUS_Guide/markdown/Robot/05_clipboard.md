# 5. Функции Clipboard

Модуль для взаимодействия с буфером обмена (получить значение из буфера обмена, установить значение в буфер обмена)

!ВНИМАНИЕ! ПРИ ИСПОЛЬЗОВАНИИ НА LINUX НЕОБХОДИМО ИМЕТЬ ПАКЕТ XCLIP (sudo apt-get install xclip)

## Описание функций

Описание каждой функции начинается с обозначения L+,W+, что означает, что функция поддерживается в ОС Linux (L) и Windows (W)

**Functions:**

| `Get`()

 | L+,W+: Получить текстовое содержимое буфера обмена.

 |
| `Set`(inTextStr)

                                    | L+,W+: Установить текстовое содержимое в буфер обмена.

                                                                                                                                                         |

### pyOpenRPA.Robot.Clipboard.Get()
L+,W+: Получить текстовое содержимое буфера обмена.

```
# Clipboard: Взаимодействие с буфером
from pyOpenRPA.Robot import Clipboard
lClipStr = Clipboard.Get()
```


* **Результат**

    Текстовое содержимое буфера обмена



* **Тип результата**

    str



### pyOpenRPA.Robot.Clipboard.Set(inTextStr: str)
L+,W+: Установить текстовое содержимое в буфер обмена.

```
# Clipboard: Взаимодействие с буфером
from pyOpenRPA.Robot import Clipboard
lClipStr = Clipboard.Set(inTextStr="HELLO WORLD")
```


* **Параметры**

    **inTextStr** (*str*) – Текстовое содержимое для установки в буфера обмена


## Быстрая навигация


* [Сообщество pyOpenRPA (telegram)](https://t.me/pyOpenRPA)


* [Сообщество pyOpenRPA (tenchat)](https://tenchat.ru/iMaslov?utm_source=19f2a84f-3268-437f-950c-d987ae42af24)


* [Сообщество pyOpenRPA (вконтакте)](https://vk.com/pyopenrpa)


* [Презентация pyOpenRPA](https://pyopenrpa.ru/Index/pyOpenRPA_product_service.pdf)


* [Портал pyOpenRPA](https://pyopenrpa.ru)


* [Репозиторий pyOpenRPA](https://gitlab.com/UnicodeLabs/OpenRPA)

		.. v1.3.1 replace:: v1.3.1
