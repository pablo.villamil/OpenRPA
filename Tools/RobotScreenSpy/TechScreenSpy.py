import json
import os
import time
import datetime
#ControlPanelDict
from desktopmagic.screengrab_win32 import (
	getDisplayRects, saveScreenToBmp, saveRectToBmp, getScreenAsImage,
	getRectAsImage, getDisplaysAsImages)
############################################
inScreenshotFolder="Screenshots"
def SaveScreenshot(inFilePath):
    # grab fullscreen
    # Save the entire virtual screen as a PNG
    lScreenshot = getScreenAsImage()
    lScreenshot.save(inFilePath, format='png')
    # lScreenshot = ScreenshotSecondScreen.grab_screen()
    # save image file
    # lScreenshot.save('screenshot.png')
# Сохранить файл на диск
while True:
    try:
        SaveScreenshot(os.path.abspath(os.path.join(inScreenshotFolder,f'Scr_{datetime.datetime.now().strftime("%Y_%m_%d__%H_%M_%S")}.png')))
    except Exception as e:
        pass
    time.sleep(3)