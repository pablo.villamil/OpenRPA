import keyboard
import subprocess
import time
import Settings
import sys
sys.path.insert(0,"C:\\Abs\\Archive\\scopeSrcUL\\OpenRPA\\Sources")
from pyOpenRPA.Robot import UIDesktop #OpenRPA UIDesktop

global mSettingsDict
mSettingsDict = Settings.Settings()
#UIDesktop initialization
UIDesktop.Utils.ProcessBitness.SettingsInit(mSettingsDict.get("pyOpenRPA",{}).get("Robot",{}).get("UIDesktop",{}).get("Utils",{}).get("ProcessBitness",None))

#Robot script
#UIDesktop.UIOSelector_Get_UIO([{"title_re":".*Configu.*", "backend":"uia"}]).draw_outline()
UIDesktop.UIOSelectorUIOActivity_Run_Dict([{"title_re":".*Configu.*", "backend":"uia"}],"draw_outline")